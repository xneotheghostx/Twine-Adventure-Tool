var beautify = require('js-beautify').js_beautify;
var fs = require('fs');
var deDE = JSON.parse(fs.readFileSync("../../models/de-DE.json", 'utf8'));
var async = require('async');

var skillName = "";
process.argv.slice(2).forEach(function (val, index, array) {
	skillName += val + " ";
});
skillName = skillName.trim();

fs.writeFile('skillName', skillName, function (err) {
  if (err) return console.log(err);
  console.log('skillName written.');
});

fs.createReadStream(skillName + '.config').pipe(fs.createWriteStream('../../.ask/config'));
fs.createReadStream(skillName + '.skill').pipe(fs.createWriteStream('../../skill.json'));

var lang = JSON.parse(fs.readFileSync(skillName + ".json", 'utf8'));

deDE.interactionModel.languageModel.invocationName = lang["invocationName"];
deDE.interactionModel.languageModel.types = [];
deDE.interactionModel.languageModel.types.push({"name": "LIST_OF_DIRECTIONS","values": []});

async.eachLimit(lang["twineFiles"], 1, function(file, callback) {
	var story = skillName + file
	
	console.log("+++++++++++++++++++++++++++++++++++ " + story + " START ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
	
	// read the Twine 2 (Harlowe) story into JSON
	var fs = require('fs');
	var contents = fs.readFileSync(story + ".html", 'utf8');
	var m = contents.match(/<tw-storydata [\s\S]*<\/tw-storydata>/g);
	var xml = m[0];
	// because Twine xml has an attribute with no value
	xml = xml.replace('hidden>', 'hidden="true">');
	var parseString = require('xml2js').parseString;
	parseString(xml, function(err, result) {
		$twine = result['tw-storydata']['tw-passagedata'];
	});

	for (var i = 0; i < $twine.length; i++) {
		var skip = false;
		for(var j = 0; j < deDE.interactionModel.languageModel.types[0].values.length; j++) {
			if(deDE.interactionModel.languageModel.types[0].values[j].name.value == $twine[i]['$']['name'].toLowerCase())
				skip = true;
		}
		if(!skip)
			deDE.interactionModel.languageModel.types[0].values.push({"name": {"value": $twine[i]['$']['name'].toLowerCase()}});
	}

	deDEWriteStream = fs.createWriteStream('../../models/de-DE.json')
	deDEWriteStream.on('open', function () {
		var content = JSON.stringify( deDE, null, 2 )
		deDEWriteStream.write( content )
		deDEWriteStream.end();
		
		var session = {};
		session.attributes = {};
		session.attributes.scene = {};
		session.attributes.visited = [];

		outputWriteStream = fs.createWriteStream(story + '.js');
		outputWriteStream.on('open', function () {
			var content = "module.exports = {"

			for (var i = 0; i < $twine.length; i++) { // Loop Entries
				console.log("+++++++++++++++++++++++++++++++++++ Original ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
				console.log($twine[i]['_']);
				console.log("+++++++++++++++++++++++++++++++++++ Original END ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
				
				var speechOutput = "";
				var lines = $twine[i]['_'].split(/\n/);
				speechOutput = processLines(lines); // Process Lines of Entry
				
				content += $twine[i]['$']['name'].replace(/ /g,"_") + ": function(attributes) {";
				content += "var output = '';";
				content += speechOutput;
				content += "return output;";
				content += "},";
				
				console.log("----------------------------------- Final -----------------------------------------------------------------");
				console.log(speechOutput);
				console.log("----------------------------------- Final END -------------------------------------------------------------");
			}
			content += "}"

			outputWriteStream.write( beautify(content), function(err) {
				var m = require('./' + story + '.js');
				console.log("||||||||||||||||||||||||||||||||||| Testing |||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||");
				for(var name in m) {
					console.log(name + ": " + m[name](session.attributes));
				}
				console.log("___________________________________ Session _______________________________________________________________");
				console.log(session.attributes);
				console.log("___________________________________ Session END ___________________________________________________________\n\n\n");
				
				console.log("Building and Testing successful!");
			});
			outputWriteStream.end();
			console.log("+++++++++++++++++++++++++++++++++++ " + story + " END ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
			callback();
		});
	});
}, function(err) {
    // if any of the file processing produced an error, err would equal that error
    if( err ) {
      // One of the iterations produced an error.
      // All processing will now stop.
      console.log('Failed to process');
    } else {
      console.log('Success');
    }
});

function processLines(lines) {

	var speechArr = [];
	
	for(var i = 0; i < lines.length; i++) {
		var output = processLine(lines[i]);
		if(output != "")
			speechArr.push(output);
	}
	return speechArr.join("\n");
}

function processLine(line) {
	line = line.trim();
	
	if(line.indexOf("<<print") > -1) { // Replace <<print
		var name = line.split("<<print $")[1].split(">>")[0];
		line = line.replace('<<print $' + name + '>>', '\' + attributes.scene["' + name + '"] + \'');
	}

	if(line.startsWith("<<if")) {
		var visitedStarted = false;
		var visitedNames = [];
		var words = line.replace("<<if ", "").replace(">>", "").split(" ");
		line = "if(";
		
		for(var i = 0; i < words.length; i++) {
			if(words[i].indexOf("!") > -1) {
				line += '!attributes.scene["' + words[i].replace('!$', "") + '"] ';
			} else if(words[i].indexOf("$") > -1) {
				line += 'attributes.scene["' + words[i].replace('$', "") + '"] ';
			} else if(words[i].indexOf("&&") > -1) {
				line += '&&';
			} else if(words[i].indexOf("||") > -1) {
				line += '||';
			} else if(words[i].indexOf("==") > -1) {
				line += '==';
			} else if(words[i].indexOf("visited") > -1) {
				visitedStarted = true;
				var name = words[i].replace('visited("', "");
				if(words[i].indexOf('")') > -1) {
					visitedStarted = false;
					name = name.replace('")', "");
					visitedNames.push(name);
					line += 'attributes.visited.includes("' + visitedNames.join(" ") + '") ';
					visitedNames = [];
				} else {
					visitedNames.push(name);
				}
				
			} else if(words[i].indexOf('")') > -1) {
				visitedStarted = false;
				visitedNames.push(words[i].replace('")', ""));
				line += 'attributes.visited.includes("' + visitedNames.join(" ") + '") ';
				visitedNames = [];
			} else {
				if(visitedStarted)
					visitedNames.push(words[i]);
				else
					line += words[i];
			}
		}
		return line + ") {";
	} else if(line.startsWith("<</if")) { // Replace <</if
		return "}";
	} else if(line.startsWith("<<else")) { // Replace <<else
		return "} else {";
	} else if(line.startsWith("<<set")) { // Replace <<set
		
		var words = line.replace("<<set $", "").replace(">>", "").split(" ");
		
		return 'attributes.scene["' + words[0]  + '"] ' + words[1] + ' ' + words[2] + ';';		
	} else { // Output regular text
		return "output += '" + line + " ';";
	}
	return "";
}