module.exports = {
    Start: function(attributes) {
        var output = '';
        output += 'Hallo und Willkommen bei deinem Spiel "Mein Auftrag". Du bist Steven Brown, ein Privatdetektiv und ich bin "Alexa". Ich werde dich bei dem Spiel begleiten, stelle dir fragen, gebe dir Hinweise und wenn du Hilfe brauchst sage "Hilfe". <break time="200ms"/><say-as interpret-as="interjection">oh nein</say-as> <break time="150ms"/> ich bin nicht nur deine Sekretärin, sondern deine Assistentin. <break time="200ms"/> Ich passe auf dich auf und helfe dir, wenn du es zulässt. <break time="250ms"/>Weil du immer den richtigen Riecher beim lösen deiner Fälle hast, nennt man dich auch "Die Nase". <break time="250ms"/> Du <say-as interpret-as="interjection">hasst</say-as> es wenn man dich so nennt, daher sage ich Steve zu dir. <break time="350ms"/> Gut Steve, in unseren Auftragsbuch stehen derzeit 3 Aufträge für dich zur Auswahl. <break time="150ms"/> Der 1. Auftrag heißt der <phoneme alphabet="ipa" ph="ˈaːdl̩">Adel</phoneme>, er ist relativ einfach. <break time="250ms"/> Der 2. Auftrag <break time="150ms"/> "Professor Schwarz" <break time="150ms"/> (Mittel) und der 3. Auftrag, das verschwundene Gemälde (Schwer). <break time="150ms"/> Such dir einen Auftrag aus, dafür sage [[Der Adel]], [[Professor Schwarz]] oder [[Das verschwundene Gemälde]]. <break time="250ms"/> Kleiner Tip von mir: Starte mit dem 1. Auftrag. ';
        return output;
    },
    Der_Adel: function(attributes) {
        var output = '';
        attributes.scene["twineSuffix"] = "_DerAdel";
        attributes.scene["twineSwitch"] = true;
        return output;
    },
    Professor_Schwarz: function(attributes) {
        var output = '';
        attributes.scene["twineSuffix"] = "_ProfSchw";
        attributes.scene["twineSwitch"] = true;
        return output;
    },
    Das_verschwundene_Gemälde: function(attributes) {
        var output = '';
        attributes.scene["twineSuffix"] = "_DasGemälde";
        attributes.scene["twineSwitch"] = true;
        return output;
    },
}