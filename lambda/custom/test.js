const story = 'Twine.html';

var fs = require('fs');
var deDE = JSON.parse(fs.readFileSync("../../models/de-DE.json", 'utf8'));

// read the Twine 2 (Harlowe) story into JSON
var fs = require('fs');
var contents = fs.readFileSync(story, 'utf8');
var m = contents.match(/<tw-storydata [\s\S]*<\/tw-storydata>/g);
var xml = m[0];
// because Twine xml has an attribute with no value
xml = xml.replace('hidden>', 'hidden="true">');
var parseString = require('xml2js').parseString;
parseString(xml, function(err, result) {
	$twine = result['tw-storydata']['tw-passagedata'];
});

deDE.interactionModel.languageModel.types = [];
deDE.interactionModel.languageModel.types.push({"name": "LIST_OF_DIRECTIONS","values": []});

var session = {};
session.attributes = {};
session.attributes.scene = {};

var ifStarted = false;
var elseStarted = false;
var conditions = false;

for (var i = 0; i < $twine.length; i++) { // Loop Entries
	console.log("+++++++++++++++++++++++++++++++++++ Original ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
	console.log($twine[i]['_']);
	console.log("+++++++++++++++++++++++++++++++++++ Original END ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
	
	var speechOutput = "";
	var lines = $twine[i]['_'].split(/\n/);
	
	speechOutput = processLines(lines); // Process Lines of Entry
	console.log("----------------------------------- Final -----------------------------------------------------------------");
	console.log(speechOutput);
	console.log("----------------------------------- Final END -------------------------------------------------------------");
	
	console.log("___________________________________ Session _______________________________________________________________");
	console.log(session.attributes.scene);
	console.log("___________________________________ Session END ___________________________________________________________\n\n\n");
}

function processLines(lines) {

	ifStarted = false;
	elseStarted = false;
	conditions = false;
	
	var speechArr = [];
	
	for(var i = 0; i < lines.length; i++) {
		var output = processLine(lines[i]);
		if(output != "")
			speechArr.push(output);
	}
	return speechArr.join(" ");
}

function processLine(line) {
	line = line.trim();
	
	if(line.startsWith("<<if")) {
		elseStarted = false;
		ifStarted = true;
		var words = line.replace("<<if", "").replace(">>", "").split(" ");
		
		var variable;
		var operator;
		for(var i = 0; i < words.length; i++) {
			if(words[i].startsWith("$")) {
				if(words[i + 1]) {
					if(words[i + 1] == "==" || words[i + 1] == "<" || words[i + 1] == ">")
						variable = words[i].substring(1);
					else if(words[i + 1] == "&&" || words[i + 1] == "||") {
						if(session.attributes.scene[words[i].substring(1)])
							conditions = true;
						else
							conditions = false;
					}						
				} else {
					if(session.attributes.scene[words[i].substring(1)])
						conditions = true;
					else
						conditions = false;
				}
			} else if(words[i].startsWith("!")) {
				if(words[i].substring(1).startsWith("$")) {
					if(session.attributes.scene[words[i].substring(2)]) {
						conditions = false;
					} else {
						conditions = true;
					}
				}
			} else if(words[i].startsWith("visited")) {		
				if (session.attributes['visited'] === undefined) {
					session.attributes['visited'] = [];
				}
				if (session.attributes['visited'].includes(words[i].replace('visited("', '').replace('")>>', '')))
					conditions = true;
				else
					conditions = false;
			} else if(words[i] == "==" || words[i] == "<" || words[i] == ">" ) {
				operator = words[i];
			} else if(!isNaN(parseInt(words[i]))) {
				switch(operator) {
					case "==":
						if(session.attributes.scene[variable] && session.attributes.scene[variable] == parseInt(words[i]))
							conditions = true;
						else
							conditions = false;
						break;
					case "<":
						if(session.attributes.scene[variable] && session.attributes.scene[variable] < parseInt(words[i]))
							conditions = true;
						else
							conditions = false;
						break;
					case ">":
						if(session.attributes.scene[variable] && session.attributes.scene[variable] > parseInt(words[i]))
							conditions = true;
						else
							conditions = false;
						break;					
				}
			} else if(typeof words[i] == "string") {
				if(operator == "==") {
					if(session.attributes.scene[variable] && session.attributes.scene[variable] == words[i])
						conditions = true;
					else
						conditions = false;
				}
			}
		}
	} else if(line.startsWith("<</if")) {
		ifStarted = false;
		elseStarted = false;
		conditions = false;
	} else if(line.startsWith("<<else")) {
		elseStarted = true;
	} else if(line.startsWith("<<set")) {
		// Setting Variable
		if((!ifStarted && !elseStarted) || (ifStarted && !elseStarted && conditions) || (ifStarted && elseStarted && !conditions)) {
			var setName = "";
			var setValue = "";
			
			if(line.indexOf(" = ") > -1) {
				setName = line.substring(line.indexOf("<<set") + 7, line.indexOf("=") - 1);
				setValue = line.substring(line.indexOf("=") + 2, line.indexOf(">>"));	
				if(!isNaN(parseInt(setValue))) {
					session.attributes.scene[setName] = parseInt(setValue);
				} else {
					session.attributes.scene[setName] = setValue;
				}
			} else if(line.indexOf("+=") > -1) {
				setName = line.substring(line.indexOf("<<set") + 7, line.indexOf("+=") - 1);
				setValue = line.substring(line.indexOf("+=") + 3, line.indexOf(">>"));
				session.attributes.scene[setName] = session.attributes.scene[setName] + parseInt(setValue);
			} else if(line.indexOf("-=") > -1) {
				setName = line.substring(line.indexOf("<<set") + 7, line.indexOf("-=") - 1);
				setValue = line.substring(line.indexOf("-=") + 3, line.indexOf(">>"));
				session.attributes.scene[setName] = session.attributes.scene[setName] - parseInt(setValue);
			}
		}
	} else {
		if((!ifStarted && !elseStarted) || (ifStarted && !elseStarted && conditions) || (ifStarted && elseStarted && !conditions)) {
			return line;
		}
	}
	return "";
}