module.exports = {
    Start: function(attributes) {
        var output = '';
        output += 'Hallo Steve, hier ist dein zweiter Auftrag. <break time="150ms"/> "Professor Schwarz". <break time="100ms"/><audio src="https://nordicskills.de/audio/ProfSchwarz/tastatur.mp3" /> Die suche im Internet, brachte dich bei deinen Recherchen nicht weiter. Da bekommst du eine Email.<break time="150ms"/> <audio src="https://nordicskills.de/audio/ProfSchwarz/siehabenpost.mp3" /> <break time="100ms"/> Du rufst die Mail ab und startest die Voicemail. <audio src="https://nordicskills.de/audio/ProfSchwarz/maillesen.mp3" /><break time="150ms"/> OK Steve, wenn du fertig bist, sage [[Anfangen]]. ';
        return output;
    },
    Anfangen: function(attributes) {
        var output = '';
        output += 'Steve, bevor wir uns auf unseren zweiten Auftrag stürzen, noch ein Paar Infos zu unserem letzten Auftrag. <break time="250ms"/> Frau Thomson und Ihrer Familie geht es gut. Ihr Scheck ist eingelöst. Ich habe alle offenen Rechnungen bezahlt. <say-as interpret-as="interjection">ähm</say-as> dein Konto ist jetzt schon wieder ins Minus gerutscht. <break time="200ms"/> Umso besser, das der neue Auftrag gerade reintrudelt.<break time="250ms"/> Also. Lass uns loslegen. Wie du ja aus der Voicemail heraus hören konntest, müssen wir nach Oxford. Dort treffen wir uns zu erst, mit <lang xml:lang="en-US">John Adams.</lang> und vergiss nicht, Ihn mit <say-as interpret-as="interjection">Sir</say-as><break time="100ms"/> anzusprechen. Der steht drauf.<break time="200ms"/> Mit <say-as interpret-as="interjection">Sir.</say-as> ich glaub der spinnt. Wie wäre das Steve? <say-as interpret-as="interjection">Sir</say-as>Steve? <say-as interpret-as="interjection">hihi</say-as>.<break time="200ms"/> OK. Ich hör schon auf. Nun lass uns los. Sage [[Flur betreten]]. ';
        return output;
    },
    Flur_betreten: function(attributes) {
        var output = '';
        output += '<audio src="https://nordicskills.de/audio/ProfSchwarz/fuss_schritte.mp3" /> Hier im Flur kannst du die <phoneme alphabet="ipa" ph="kɔˈmoːdə">kommode</phoneme> durchsuchen, etwas aus dem Abstellraum nehmen oder das Büro verlassen. Und? was möchtest du? <break time="100ms"/> zur [[Komode]], zum [[Ausgang]] oder zum [[Abstellraum]]. ';
        return output;
    },
    Komode: function(attributes) {
        var output = '';
        if (attributes.scene["Item_Taschenlampe"] == 1) {
            output += '<audio src="https://nordicskills.de/audio/ProfSchwarz/schubladeauf.mp3" /> Was für eine Leere aufgeräumte Schublade. Hier ist nichts mehr. <audio src="https://nordicskills.de/audio/ProfSchwarz/schubladezu.mp3" /> ';
        } else {
            attributes.scene["Item_Taschenlampe"] = 1;
            attributes.scene["Item_Handschuhe"] = 1;
            output += 'Die Schublade der Kommode öffnet sich <audio src="https://nordicskills.de/audio/ProfSchwarz/schubladeauf.mp3" /> du nimmst dir die Taschenlampe und die Handschuhe packst du auch gleich mit ein. Steve, ich glaube hier ist nichts mehr. <audio src="https://nordicskills.de/audio/ProfSchwarz/schubladezu.mp3" /> ';
        }
        output += 'Lass uns weiter, dafür sage [[Abstellraum]] oder [[Ausgang]]. ';
        return output;
    },
    Ausgang: function(attributes) {
        var output = '';
        if (attributes.scene["Item_Regenschirm"] == 1) {
            output += '<audio src="https://nordicskills.de/audio/ProfSchwarz/tuerzu.mp3" /> Steve, schließ die Tür ab. <break time="100ms"/> Sonnst kommen wir wieder und alle Rechnungen sind bezahlt. <audio src="https://nordicskills.de/audio/ProfSchwarz/abschliessen.mp3" /> Steve? weist du was? manchmal glaube ich du machst das mit Absicht. Ich meine das mit der Tür, um mich auf die Palme zu bringen. Aber egal, lass uns die Treppe runter und uns auf den Weg machen. Sage [[Treppe runter]]. ';
        } else {
            output += 'Hast du nicht was vergessen? Du weist das wir in England sind. Oder? <break time="200ms"/> Wie, du weist nicht was ich meine. <break time="300ms"/> Wie wäre es mit einem Regenschirm. Ich glaube im Abstellraum hängt einer. <break time="100ms"/> Sage [[Abstellraum]]. ';
        }
        return output;
    },
    Abstellraum: function(attributes) {
        var output = '';
        if (attributes.scene["Item_Regenschirm"] == 1) {
            output += '<say-as interpret-as="interjection">ähm</say-as>. Reicht dir ein Regenschirm nicht? ';
        } else {
            attributes.scene["Item_Regenschirm"] = 1;
            output += 'Du öffnest die Tür <audio src="https://nordicskills.de/audio/ProfSchwarz/abstellraumauf.mp3" /> nimmst den Regenschirm der in dem Abstellraum hängt. Steve. Du bist so ein Gentleman, du denkst an mich und meine tolle Frisur. Echt toll von dir. <break time="150ms"/> Jetzt schließt du die Tür <audio src="https://nordicskills.de/audio/ProfSchwarz/abstellraumzu.mp3" /> und <phoneme alphabet="ipa" ph="ɡeːst">gehst</phoneme> zurück zum Flur. ';
        }
        output += 'Sage [[Komode]] oder [[Ausgang]] ';
        return output;
    },
    Treppe_runter: function(attributes) {
        var output = '';
        output += '<audio src="https://nordicskills.de/audio/ProfSchwarz/trepperunter.mp3" />Ich frage mich die ganze Zeit um was es in dem neuen Auftrag geht. <break time="100ms"/> Hast du eine Ahnung? <break time="350ms"/> Wie? <break time="150ms"/> du meinst das bekommen wir schon noch zu hören. <break time="200ms"/> <say-as interpret-as="interjection">aha</say-as> du bist also gar nicht Neugierig? <break time="200ms"/><audio src="https://nordicskills.de/audio/ProfSchwarz/trepperunter.mp3" /> ';
        output += '<say-as interpret-as="interjection">Alles klar</say-as>, schweigen ist auch eine Antwort. <break time="200ms"/> Wir sind unten angekommen, nun sage [[auf den Gehweg]] ';
        return output;
    },
    auf_den_Gehweg: function(attributes) {
        var output = '';
        if (attributes.visited.includes("aufheben")) {
            output += 'So nach rechts waren wir schon <break time="150ms"/> und wohin jetzt Chef? <break time="150ms"/> Willst du über die Strasse gehen oder die Strasse nach links? Sage [[über die Strasse]] oder [[Strasse nach links]] ';
        } else {
            output += '<audio src="https://nordicskills.de/audio/ProfSchwarz/strassenverkehr.mp3" /> Steve. Hier ist ordentlich Verkehr, pass bitte auf. <break time="200ms"/> Wenn du weist wo wir hin wollen, sage [[Strasse nach rechts]], [[über die Strasse]] oder [[Strasse nach links]] ';
        }
        return output;
    },
    Strasse_nach_rechts: function(attributes) {
        var output = '';
        if (attributes.visited.includes("aufheben")) {
            output += 'Dahinten waren wir doch schon. Lass uns woanders hingehen. dafüs sage [[Strasse nach links]] oder [[über die Strasse]] ';
        } else {
            output += '<audio src="https://nordicskills.de/audio/ProfSchwarz/fussschritte_strasse.mp3" /> Was willst du denn hier? <break time="1s"/> gut das ich eine Antwort bekomme <audio src="https://nordicskills.de/audio/ProfSchwarz/fussschritte_strasse.mp3" /> Hallo, Erde an Steve. <break time="1s"/> Achso du weist es auch nicht. Na dann. Gut das wir drüber geredet haben. <break time="1s"/> Was? Du bist nur hier lang gelaufen, weil ich nichts gesagt habe? <audio src="https://nordicskills.de/audio/ProfSchwarz/fussschritte_strasse.mp3" /> <say-as interpret-as="interjection">alles klar</say-as>. <break time="200ms"/> Und hätte ich gesagt nach links? Was dann? <break time="200ms"/> Und nein ich bin nicht zickig. <break time="200ms"/> Schau mal da. Da liegt etwas. Da meine Fingernägel frisch lackiert sind, darfst du das gerne auf heben. Dafür sage [[aufheben]]. ';
        }
        return output;
    },
    über_die_Strasse: function(attributes) {
        var output = '';
        output += '<audio src="https://nordicskills.de/audio/ProfSchwarz/autohupe_vorbeifahrt.mp3" />Sei bloß vorsichtig und pass auf den Verkehr auf. <audio src="https://nordicskills.de/audio/ProfSchwarz/auto_vorbeifahrt.mp3" /> Bist du dir sicher, das du weiter gehen willst? sage [[ja weiter]], [[Strasse nach links]] oder [[Strasse nach rechts]]. ';
        return output;
    },
    Strasse_nach_links: function(attributes) {
        var output = '';
        output += 'Da vorn steht ein Taxi, das sollten wir nehmen und damit zum Bahnhof fahren.<break time="200ms"/> Ich hoffe, wir bekommen noch einen Zug nach Oxford. <break time="100ms"/> Denn auf dem Bahnhof, ist es immer so kalt und zugig.<break time="250ms"/> Wie? Was meinst du? Ich bin heute mal wieder Witzig. <break time="250ms"/> Ach so. Das Wort Zugig. <break time="150ms"/> Nee nee, das war schon ernst gemeint. Da zieht es fürchterlich und du weist genau, wenn mir kalt wird bekomme ich schlechte Laune.<break time="250ms"/> Nur so am Rande. Wenn du soweit bist, lass uns einsteigen und zum Bahnhof fahren. Sage [[Taxi benutzen]] ';
        return output;
    },
    aufheben: function(attributes) {
        var output = '';
        if (attributes.visited.includes("Nach sehen")) {
            output += 'Hier ist nichts außer Müll. Lass uns zurück zu Strasse [[auf den Gehweg]] ';
        } else {
            attributes.scene["Item_Haarspange"] = 1;
            output += 'Schau mal, da liegt etwas. <break time="100ms"/> Was ist das ? <break time="250ms"/> Ach so eine Alte Haarspange, egal <break time="200ms"/> ich steck die mal ein. Lass uns zurück zu Strasse [[auf den Gehweg]]. ';
        }
        return output;
    },
    ja_weiter: function(attributes) {
        var output = '';
        output += '<audio src="https://nordicskills.de/audio/ProfSchwarz/autohupe_vorbeifahrt.mp3" /> <say-as interpret-as="interjection">Achtung</say-as> <audio src="https://nordicskills.de/audio/ProfSchwarz/autounfall.mp3" /> Steve? <break time="350ms"/> <prosody pitch="+30%">Steve?</prosody> <break time="200ms"/> Hörst du mich? <break time="100ms"/> lebst du noch? <break time="200ms"/> Mann. Du hast mir vielleicht einen Schrecken eingejagt. Du spinnst wohl. Rennst einfach über die Strasse. Tja. Das mit dem Auftrag, können wir jawohl knicken. Mit einem gebrochenem Bein wird das nichts. Klasse gemacht. <audio src="https://nordicskills.de/audio/ProfSchwarz/rettungswagen.mp3" /> Du hast leider verloren. Wenn du noch einmal spielen möchtest sage nochmal spielen. Oder zum beenden sage Beenden. ';
        return output;
    },
    Taxi_benutzen: function(attributes) {
        var output = '';
        output += 'Der Taxifahrer hatte gefragt wohin wir wollen. <break time="150ms"/> Ich habe ihm gesagt, das wir zum Bahnhof müssen.<audio src="https://nordicskills.de/audio/ProfSchwarz/autostarten.mp3" /> <audio src="https://nordicskills.de/audio/ProfSchwarz/siehabenpost.mp3" /> <break time="150ms"/> Steve. Ich glaube du hast gerade eine Mail bekommen. Vielleicht mit mehr Informationen zum Auftrag. Spiel die Sprachnachricht bitte ab. <audio src="https://nordicskills.de/audio/ProfSchwarz/sprachnachricht.mp3" /> <break time="200ms"/> Das klingt ja mal nach einem merkwürdigen Auftrag. <break time="250ms"/> Steve, da vorn ist der Bahnhof, lass uns später weiter reden. Lass uns erst einmal Aussteigen. Dafür sage [[Aussteigen]]. ';
        return output;
    },
    Aussteigen: function(attributes) {
        var output = '';
        if (attributes.scene["Item_Zeitung"] == 1) {
            output += 'Danke Steve für die Zeitung. Du bist ein toller und fürsorglicher Chef, aber jetzt lass uns weiter. Sage [[zum Bahnhof]]. ';
        } else {
            output += 'Steve schau mal, da liegt eine Zeitung auf der Rückbank. Ich glaub die ist von heute. Nimm die bitte mit. Dann habe ich im Zug was zum lesen. Du schläfst ja immer und mir wird dann langweilig. Entscheide dich und sage [[zum Bahnhof]] oder [[Zeitung mitnehmen]]. ';
        }
        return output;
    },
    zum_Bahnhof: function(attributes) {
        var output = '';
        output += 'Steve, nur mal so am rande. Wenn du wissen willst, was in meiner <phoneme alphabet="ipa" ph="ˈtaʃə">Tasche</phoneme> ist, brauchst du nur sagen, "Alexa <phoneme alphabet="ipa" ph="ˈtaʃə">Tasche</phoneme>" oder "Alexa Inventar". Dann sage ich dir, was bis jetzt alles drin ist. Wenn sie mir aber zu schwer wird, trägst du sie. So. Nun lass uns in den Bahnhof hinein gehen. Sage [[Bahnhof betreten]]. ';
        return output;
    },
    Zeitung_mitnehmen: function(attributes) {
        var output = '';
        if (attributes.visited.includes("Zeitung mitnehmen")) {
            output += 'Bist du vergesslich? Die Zeitung habe ich doch schon von dir bekommen. Oder suchtst du nach Keckskrümel. Komm jetzt, lass uns zurück. Dafüs sage [[zum Bahnhof]]. ';
        } else {
            attributes.scene["Item_Zeitung"] = 1;
            output += 'Gibst du sie mir? <break time="250ms"/> Danke. <break time="100ms"/> Komm jetzt, lass uns zurück, sage [[zum Bahnhof]]. ';
        }
        return output;
    },
    Bahnhof_betreten: function(attributes) {
        var output = '';
        output += '<audio src="https://nordicskills.de/audio/ProfSchwarz/bahnhof.mp3" /> Steve. Schau dich um <break time="100ms"/> und sag wohin wir sollen. [[zum Zug]], [[Kiosk besuchen]] oder [[zum Fahrkartenautomat]]. ';
        return output;
    },
    zum_Zug: function(attributes) {
        var output = '';
        if (attributes.scene["Item_Fahrkarte"] == 1) {
            output += 'Tja. Ich habe schon einen tollen Chef. Der denkt sogar an die Fahrkarten. Klasse Steve. Lass uns einsteigen. Dafür sage [[einsteigen]]. ';
        } else {
            output += '<say-as interpret-as="interjection">ey</say-as><break time="150ms"/> du weist schon, das wir Fahrkarten brauchen um mit dem Zug zu fahren. <break time="250ms"/> Da vorne war ein Fahrkartenautomat, lass uns dort hingehen. Sage [[zum Fahrkartenautomat]]. ';
        }
        output += ' ';
        return output;
    },
    Kiosk_besuchen: function(attributes) {
        var output = '';
        if (attributes.scene["Item_Zeitung"] == 1 && attributes.scene["Item_Fahrkarte"] == 1) {
            output += 'Du hast doch schon eine Zeitung. Die aus dem Taxi und Fahrkarten haben wir auch. Steve, kann es sein das du noch nicht ganz wach bist? Komm lass uns zum Zug gehen. Sage [[zum Zug]] ';
        } else {
            if (attributes.scene["Item_Zeitung"] == 0 && attributes.scene["Item_Fahrkarte"] == 1) {
                attributes.scene["Item_Zeitung"] = 1;
                output += '<audio src="https://nordicskills.de/audio/ProfSchwarz/bahnhofskiosk.mp3" /> Gut die Fahrkarten haben wir auch dann las uns weiter. Sage [[zum Zug]] ';
            } else {
                if (attributes.scene["Item_Zeitung"] == 1 && attributes.scene["Item_Fahrkarte"] == 0) {
                    output += 'Du hast doch schon eine Zeitung. Die aus dem Taxi. Steve, kann es sein das du noch nicht ganz wach bist? Komm wir brauchen noch eine Fahrkarten lass uns welche holen. Davorne ist ein Automat, dort kannst du welche kaufen. Sage [[zum Fahrkartenautomat]] ';
                } else {
                    attributes.scene["Item_Zeitung"] = 1;
                    output += '<audio src="https://nordicskills.de/audio/ProfSchwarz/bahnhofskiosk.mp3" /> Gut jetzt brauchen wir noch fahrkarten. Da vorne ist ein Automat, dort kannst du welche kaufen. Sage [[zum Fahrkartenautomat]] ';
                }
            }
        }
        return output;
    },
    zum_Fahrkartenautomat: function(attributes) {
        var output = '';
        if (attributes.visited.includes("zum Fahrkartenautomat")) {
            output += 'Wie viele Fahrkarten brauchst du denn noch ? Also mir reicht eine. Lass uns endlich losfahren. Sage [[zum Zug]] oder [[Kiosk besuchen]]. ';
        } else {
            attributes.scene["Item_Fahrkarte"] = 1;
            output += '<audio src="https://nordicskills.de/audio/ProfSchwarz/fahrkartendrucker.mp3" /> OK. du hast uns gerade zwei Fahrkarten für Oxford gekauft. Dann lass uns zum Zug gehen. Sage [[zum Zug]] oder [[Kiosk besuchen]]. ';
        }
        output += ' ';
        return output;
    },
    einsteigen: function(attributes) {
        var output = '';
        output += '<audio src="https://nordicskills.de/audio/ProfSchwarz/zugtuerzu.mp3" />Steve pass auf. Die Tür schließt. Nicht das ich ohne dich nach Oxford fahren muss. <break time="250ms"/> Lass uns in das Abteil gehen, dort ist es doch gemütlicher als hier auf dem Gang. <audio src="https://nordicskills.de/audio/ProfSchwarz/abteiltuer.mp3" /> Sage [[Abteil betreten]] ';
        return output;
    },
    Abteil_betreten: function(attributes) {
        var output = '';
        if (attributes.scene["Item_Zeitung"] == 1) {
            output += 'Klasse das du die Zeitung besorgt hast. Gib sie mir bitte einmal. Ich werde mal sehen was dort alles steht.<break time="200ms"/><say-as interpret-as="interjection">aha</say-as><break time="250ms"/> <say-as interpret-as="interjection">ach so</say-as> <break time="250ms"/> <say-as interpret-as="interjection">als ob</say-as> dass glaub ich jetzt nicht. Steve, hör mal zu. Hier steht das die Wissenschaftler an einer Formel arbeiten, die angeblich das Altern aufhalten soll. Glaubst du das hat was mit unserem Fall zutun? <break time="200ms"/> Wie, du glaubst nicht an so einen Quatsch.<break time="150ms"/> Das mit der Wissenschaft oder das es mit unserem Auftrag zu tun hat. <break time="250ms"/> Ach du meinst das mit dem Jungbrunnen. Naja wer weis. <audio src="https://nordicskills.de/audio/ProfSchwarz/zugfahrt.mp3" /> Steve, wir fahren gerade in Oxford ein <break time="200ms"/> und der Zug hält gleich. Dann müssen wir den Zug verlassen. Sage [[Zug verlassen]] ';
        } else {
            output += 'Toll. <break time="100ms"/> hätte ich jetzt eine Zeitung, könnte ich dir vorlesen was heute alles passiert ist. <break time="150ms"/> Aber nein, <break time="100ms"/> der Herr Detektiv, hat es ja nicht nötig auf seine Assistentin zu hören. Naja dann leg dich hin. Ich wecke dich wenn es soweit ist. <audio src="https://nordicskills.de/audio/ProfSchwarz/zugfahrt.mp3" /><prosody rate="75%">Eine Stunde später.</prosody> Steve, aufwachen. Wir sind in Oxford. Lass uns aussteigen. Sage [[Zug verlassen]] ';
        }
        return output;
    },
    Zug_verlassen: function(attributes) {
        var output = '';
        output += '<audio src="https://nordicskills.de/audio/ProfSchwarz/zugverlassen.mp3" /> Ich habe gerade eine Nachricht von der Uni bekommen, das vor dem Bahnhof ein Shuttle auf uns wartet. Der bringt uns zum Campus und dort treffen wir dann auf unseren Auftraggeber. <break time="150ms"/> Bin ja mal gespannt, was das für einer ist. <break time="100ms"/> Unser <lang xml:lang="en-US">Sir Adams</lang>. Ok dann lass uns weiter. Sage [[Bahnhof verlassen]] ';
        return output;
    },
    Bahnhof_verlassen: function(attributes) {
        var output = '';
        output += '<audio src="https://nordicskills.de/audio/ProfSchwarz/regenschauer.mp3" /> Was für ein Schmuddelwetter, war ja klar das es jetzt regnet. <break time="150ms"/> ';
        if (attributes.scene["Item_Regenschirm"] == 1) {
            output += 'Steve, du bist ein Held. Was für ein Glück das du den Schirm mit genommen hast. <break time="200ms"/> Schau mal. Das ist doch mal ein Service. Der Shuttle steht bereit und wartet auf uns. <break time="200ms"/> Dann lass uns zum Campus fahren. Sage [[zum Campuseingang]] ';
        } else {
            output += 'Tja, ein Regenschirm wäre jetzt nicht schlecht, aber was solls, dann werde ich eben nass. <break time="200ms"/> Lass uns schnell einsteigen und zumCampus fahren. Sage [[zum Campuseingang]] ';
        }
        output += ' ';
        return output;
    },
    zum_Campuseingang: function(attributes) {
        var output = '';
        if (attributes.visited.includes("zum Pförtner")) {
            output += 'Mit dem Ausweis vom Pförtner können wir auch in das Hauptgebäude kommen. Wohin willst du jetzt? [[zum Garten]] [[zum Verwaltungsgebäude]] oder [[zum Labor]] ';
        } else {
            if (attributes.visited.includes("zum Garten")) {
                output += 'Nicht vergessen, wir sollten noch zum Pförtner und verräts du mir vohin du willst? [[zum Pförtner]], [[zum Verwaltungsgebäude]] oder [[zum Labor]] ';
            } else {
                output += '<audio src="https://nordicskills.de/audio/ProfSchwarz/autostarten.mp3" /> Da vorn ist der Campus. Der ist ja ganz schön groß.<break time="150ms"/><audio src="https://nordicskills.de/audio/ProfSchwarz/autotueraufundzu.mp3" /> Da sind wir. Auf dem Campus gibt es neben Hörsälen einer Bibliothek einen Garten und Mensa auch ein Labor. Das Labor sollten wir umbedingt aufsuchen. Aber zuerst sollten wir zum Pförtner, um uns die Ausweise geben zu lassen. <break time="150ms"/> Und wohin wollen wir? [[zum Garten]], [[zum Labor]], [[zum Verwaltungsgebäude]] oder [[zum Pförtner]] ';
            }
        }
        return output;
    },
    zum_Garten: function(attributes) {
        var output = '';
        output += '<audio src="https://nordicskills.de/audio/ProfSchwarz/voegelimwald.mp3" /> Hier im Garten sieht es sehr gepflegt aus. Aber ich bin mir nicht sicher, dass es uns weiter bringt. <break time="200ms"/> Steve, lass uns zurück gehen und wo anders suchen. Dafür sage [[zum Campuseingang]]. ';
        return output;
    },
    zum_Pförtner: function(attributes) {
        var output = '';
        if (attributes.scene["Item_Ausweis"] == 1) {
            output += '<voice name="Vicki"><lang xml:lang="de-DE">Hallo Herr Brown, Sie haben die Ausweise doch schon erhalten. Weiterhin einen angenehmen Aufendhalt.</lang></voice> Steve? Und jetzt. WIe geht es weiter? Wohin willst du? [[zum Garten]], [[zum Labor]], [[zum Campuseingang]] oder [[zum Verwaltungsgebäude]]. ';
        } else {
            attributes.scene["Item_Ausweis"] = 1;
            attributes.scene["Ausweis2"] = 0;
            output += '<voice name="Vicki"><lang xml:lang="de-DE">Hallo Herr Brown, Hallo Alexa. Hier sind ihre Ausweise für den Campus die Sir Adams für sie hinterlegt hat. Ich wünsche Ihnen einen angenehmen Aufendhalt.</lang></voice> <break time="200ms"/> ';
            output += 'Mit dem Ausweis haben wir endlich Zugang zu den Gebäuden auf dem Campus. <break time="200ms"/> Wohin willst du? [[zum Garten]], [[zum Campuseingang]], [[zum Labor]], oder [[zum Verwaltungsgebäude]]. ';
        }
        return output;
    },
    zum_Labor: function(attributes) {
        var output = '';
        if (attributes.scene["Item_Ausweis"] == 1 && attributes.scene["Ausweis2"] == 1) {
            output += 'Durch die Freigabe kommen wir hier endlich rein. Steve, <break time="150ms"/> hör mal. <break time="150ms"/> Ich fand das Gespräch mit <lang xml:lang="en-US">Adams</lang> echt komisch. Und dazu kommt, ich traue dem überhaupt nicht. Ok <break time="100ms"/> er ist unser Kunde, aber das bedeutet ja nicht das ich ihn mögen muss. <break time="200ms"/> Was meinst du? <break time="250ms"/> Du bist meiner Meinung? Das ist ja mal was ganz neues. <break time="200ms"/> So nun lass uns mal weiter. Damit die Tür aufgeht sage [[Labor betreten]]. ';
        } else {
            if (attributes.scene["Item_Ausweis"] == 1 && attributes.scene["Ausweis2"] == 0) {
                output += '<audio src="https://nordicskills.de/audio/ProfSchwarz/zugangverboten.mp3" /> Um in das Labor zu kommen, brauchen wir die Freigabe durch <lang xml:lang="en-US">Adams.</lang>. Oh sorry, ich meine <lang xml:lang="en-US">Sir Adams.</lang> Dann lass uns zurück. Sage [[zum Verwaltungsgebäude]]. ';
            } else {
                output += '<audio src="https://nordicskills.de/audio/ProfSchwarz/zugangverboten.mp3" /> In das Gebäude kommen wir nur mit dem Ausweis vom Pförtner. Das hatte ich dir aber auch gesagt, dass wir zuerst zum Pfördner sollten. Dann lass uns die Ausweise abholen. Sage [[zum Pförtner]]. ';
            }
        }
        return output;
    },
    zum_Verwaltungsgebäude: function(attributes) {
        var output = '';
        if (attributes.scene["Item_Ausweis"] == 1) {
            output += 'Hier im Verwaltungsgebäude soll das Büro sein. <break time="250ms"/> Schau mal, <break time="250ms"/> da vorne ist das Büro. Dann lass uns mal reingehen. Sage [[Büro betreten]]. ';
        } else {
            output += '<audio src="https://nordicskills.de/audio/ProfSchwarz/zugangverboten.mp3" /> Tja, ich sags ja immer wieder, hör auf mich. Aber der Meisterdetektiv weis es ja besser. In das Hauptgebäude kommen wir nur mit dem Ausweis vom Pförtner. Also lass uns weiter. Sage [[zum Pförtner]]. ';
        }
        output += ' ';
        return output;
    },
    Labor_betreten: function(attributes) {
        var output = '';
        attributes.scene["Item_Notizbuch"] = 0;
        output += '<say-as interpret-as="interjection">was zur hölle</say-as> <break time="200ms"/> wie sieht es denn hier aus? <break time="350ms"/> Hier im Labor ist ja das reine Chaos. <break time="250ms"/> Gibt es eigentlich einen Schrank der nicht durch wühlt ist? <break time="300ms"/><say-as interpret-as="interjection">oh mein gott</say-as>, hier räume ich nicht auf. <break time="300ms"/> Schau mal auf dem Boden.<break time="250ms"/> Hunderte von Seiten Papier.<break time="200ms"/> Steve, dort in der Ecke liegt eine Computertastatur <break time="200ms"/> und unter der Tastatur klebt etwas, sieht aus wie ein Zettel. <break time="250ms"/> <say-as interpret-as="interjection">schau an.</say-as> Hier steht ein Code drauf <break time="200ms"/>vier <break time="100ms"/>eins <break time="100ms"/>acht <break time="100ms"/>acht <break time="100ms"/>fünf. <break time="200ms"/> Ich würde sagen, den solltest du dir merken oder aufschreiben.<break time="300ms"/> Was meinst du? <break time="350ms"/> Die Schranktür. Was soll damit sein? <break time="250ms"/> Ach so. Ja gute Idee <break time="100ms"/> schau mal nach was du da findest. ';
        attributes.scene["Item_Zettel_mit_Passwort_4_1_8_8_5"] = 1;
        output += 'Sage [[Schranktür öffnen]]. ';
        return output;
    },
    Schranktür_öffnen: function(attributes) {
        var output = '';
        if (attributes.scene["Item_Notizbuch"] == 1) {
            output += 'Komm schon. Das Notizbuch haben wir. Lass uns hier abhauen. Dafür sage [[Labor verlassen]]. ';
        } else {
            output += '<audio src="https://nordicskills.de/audio/ProfSchwarz/schranktueroeffnen.mp3" /> Steve. Du hattest recht. Der Schrank, ist kein richtiger Schrank. Schau mal, da ist ein Safe drin. Wir haben doch gerade einen Zettel mit einem Code darauf gefunden. <break time="100ms"/> Den hast du ja noch oder? <break time="250ms"/> Wenn nicht, dann schau mal in die <phoneme alphabet="ipa" ph="ˈtaʃə">Tasche</phoneme> oder in die App. Sage, Alexa <phoneme alphabet="ipa" ph="ˈtaʃə">Tasche</phoneme> oder wenn du den Code hast, dann sage die fünf Zahlen. ';
            if (2 == 1) {
                output += '[[Vier eins acht acht fünf]] ';
            }
        }
        return output;
    },
    Büro_betreten: function(attributes) {
        var output = '';
        attributes.scene["Ausweis2"] = 1;
        output += 'Da vorne ist John Adams. <audio src="https://nordicskills.de/audio/ProfSchwarz/notizblockblaettern.mp3" /> Was blättert der denn wie verrückt in den Unterlagen. Sieht aus als wenn er was sucht. Hallo Sir Adams, wir sind angekommen. <voice name="Hans"><lang xml:lang="de-DE">schön. Sehr schön.</lang></voice>. Herr Brown und ich würden gerne etwas mehr über den Auftrag wissen. <voice name="Hans"><lang xml:lang="de-DE">jetzt nicht. <break time="150ms"/> Später.<break time="150ms"/>Hier ist Ihre Freigabe. Gehn Sie jetzt bitte. <break time="250ms"/></lang></voice> wow. Der Typ spinnt ja wohl. <break time="250ms"/> Du brauchst doch Informationen um diesen Auftrag zu erledigen. Was hat der denn für ein Problem. <break time="150ms"/> Mir gefällt der Typ überhaupt nicht. Egal. <break time="350ms"/> Du meinst wir sollen zum Labor gehen und uns dort umsehen. OK. Das machen wir. Sage [[zum Labor]]. ';
        return output;
    },
    Labor_verlassen: function(attributes) {
        var output = '';
        if (attributes.scene["Item_Autoschluessel"] == 1) {
            output += 'Wohin willst du? Ich will dich ja nicht hetzen, aber jetzt wo du einen Autoschlüssel hast, müssen wir zum Parkplatz. Wenn du soweit bist sage [[zum Parkplatz]] ';
        } else {
            attributes.scene["Item_Autoschluessel"] = 0;
            output += '<audio src="https://nordicskills.de/audio/ProfSchwarz/labortuerzu.mp3" /> So, aus dem Gebäude sind wir schon einmal raus.<break time="200ms"/> Schau mal Steve, da vorne ist ein Schlüsselkasten und dahinten ist ein Parkplatz mit vielen Autos. Wir müssen uns beeilen. <break time="200ms"/> Daher schlage ich vor, wir schnappen uns ein Auto und machen uns vom Hof. Wir bringen das Auto später wieder zurück. Was machen wir jetzt? [[zum Schlüsselkasten]] oder [[zum Parkplatz]]. ';
        }
        return output;
    },
    zum_Parkplatz: function(attributes) {
        var output = '';
        if (attributes.scene["Item_Autoschluessel"] == 0) {
            output += 'Na klasse. Hier stehen vier Autos ein BMW, Audi, eine Ente und ein Golf und du hast keinen Schlüssel. Schnell lauf zum Schlüsselkasten und hole dir einen. Lauf Forrest <break time="100ms"/> lauf. [[zum Schlüsselkasten]] ';
        } else {
            output += 'Hier stehen vier Autos. Ein BMW, ein Audi, eine Ente und ein Golf <break time="250ms"/> und zu welchem passt der Schlüssel? Du hast die Wahl. Mir gefällt der B.M.W am besten, aber du hast den Schlüssel. [[zum BMW]], [[zum Audi]], [[zur Ente]] oder [[zum Golf]] ';
        }
        return output;
    },
    zum_Schlüsselkasten: function(attributes) {
        var output = '';
        attributes.scene["Item_Autoschluessel"] = 1;
        output += 'Steve, schnell nimm dir einfach einen Schlüssel und dann weg hier. Beeile dich wir müssen zum Parkplatz. <audio src="https://nordicskills.de/audio/ProfSchwarz/schluesselbund.mp3" /> Jetzt mach schnell. Sage [[zum Parkplatz]]. ';
        return output;
    },
    zum_BMW: function(attributes) {
        var output = '';
        output += '<audio src="https://nordicskills.de/audio/ProfSchwarz/autoschluesselNo.mp3" /> Nein. Das war das falsche Auto. <say-as interpret-as="interjection">verdammt.</say-as>Ich wollte BMW fahren. Schnell ein anderes. [[zum Golf]], [[zum Audi]] oder [[zur Ente]]. ';
        return output;
    },
    zum_Audi: function(attributes) {
        var output = '';
        output += '<audio src="https://nordicskills.de/audio/ProfSchwarz/autoschluesselNo.mp3" /> Nö, passt nicht. Das ist das falsche Auto. Welchen nimmst du jetzt? [[zum BMW]], [[zum Golf]] oder [[zur Ente]] ';
        return output;
    },
    zur_Ente: function(attributes) {
        var output = '';
        output += '<audio src="https://nordicskills.de/audio/ProfSchwarz/autoschluesselYes.mp3" /> <say-as interpret-as="interjection">Bingo.</say-as> Na <break time="150ms"/> du hast ja <prosody rate="65%">richtig</prosody> glück. Vier Autos <break time="100ms"/> und du nimmst dir den Schlüssel von einer Ente. <break time="200ms"/> Aber egal, jetzt lass uns einsteigen und von hier abhauen. <audio src="https://nordicskills.de/audio/ProfSchwarz/autotueraufundzu.mp3" /> Steve. Der schlüssel gehört ins Schloss. <break time="100ms"/> Und ras nicht so. <say-as interpret-as="interjection">hihi</say-as> <break time="250ms"/> Mit einer Ente rasen.<break time="150ms"/><say-as interpret-as="interjection">brumm</say-as>. Sage [[Abfahren]]. ';
        return output;
    },
    zum_Golf: function(attributes) {
        var output = '';
        output += '<audio src="https://nordicskills.de/audio/ProfSchwarz/autoschluesselNo.mp3" /> Nee nee, der Golf ist es nicht. Das ist das falsche Auto. Und jetzt? Welchen nimmst du jetzt? [[zum BMW]], [[zum Audi]] oder [[zur Ente]] ';
        return output;
    },
    Abfahren: function(attributes) {
        var output = '';
        output += '<audio src="https://nordicskills.de/audio/ProfSchwarz/autostartenlosfahren.mp3" /> Du glaubst das wir im Haus vom Professor was finden werden? <break time="100ms"/> Ich hoffe nicht Ihn. Ich mein ja nur, kann doch sein, das er Tot ist.<break time="350ms"/> Stimmt, da hast du recht. Dann wären wir unseren Auftrag ja gleich wieder los. <break time="1s"/> Ich glaube das da vorn ist doch die Adresse oder? Dann lass uns anhalten und aussteigen. Dafür sage [[Auto verlassen]]. ';
        return output;
    },
    Auto_verlassen: function(attributes) {
        var output = '';
        attributes.scene["Klingel"] = 0;
        output += '<audio src="https://nordicskills.de/audio/ProfSchwarz/autotuerzu.mp3" /> ';
        output += 'Das Haus und das Grundstück sehen aber sehr gepflegt aus. <break time="100ms"/> Es macht nicht den Eindruck, das er nicht wieder kommen wollte. Ich frage mich immer wieder was wohl mit Ihm los ist. Und warum er verschwunden ist.<break time="200ms"/> Ich hoffe nur, das wir Ihn finden werden.<say-as interpret-as="interjection">mein gott</say-as>, ich muss schnell auf andere Gedanken kommen. Sage [[zum Eingang]]. ';
        return output;
    },
    zum_Eingang: function(attributes) {
        var output = '';
        if (attributes.scene["Klingel"] == 0) {
            output += 'Geh mal an die Tür und <phoneme alphabet="ipa" ph="ˈklɪŋl̩">Klingel</phoneme>. Sage [[zur Klingel]] ';
        } else {
            output += 'Du kannst den Schlüssel ja auch suchen. <break time="200ms"/> Da ist eine Regenrinne, ein Blumentopf und eine Fußmatte. Such dir was aus. Sage [[zur Regenrinne]], [[zum Blumentopf]] oder [[zur Fußmatte]]. ';
        }
        return output;
    },
    zur_Klingel: function(attributes) {
        var output = '';
        output += '<audio src="https://nordicskills.de/audio/ProfSchwarz/tuerklingel.mp3" /> ';
        if (attributes.visited.includes("Klingel")) {
            output += 'Du kannst so oft Klingeln wie du willst. Deshalb ist trotzdem keiner da. ';
            output += 'Lass uns lieber den Schlüssel suchen. Sage [[zum Eingang]]. ';
        } else {
            attributes.scene["Klingel"] = 1;
            output += '<audio src="https://nordicskills.de/audio/ProfSchwarz/tuerklingel.mp3" /> Und? <break time="200ms"/> <audio src="https://nordicskills.de/audio/ProfSchwarz/tuerklingel.mp3" /> Nee. Da macht keiner auf. Lass uns den Schlüssel suchen. Sage [[zum Eingang]]. ';
        }
        return output;
    },
    zur_Regenrinne: function(attributes) {
        var output = '';
        if (attributes.visited.includes("zur Klingel") && attributes.visited.includes("zur Fußmatte")) {
            output += '<say-as interpret-as="interjection">pfui</say-as>, das war nichts. Aber dreckige Hände kann man ja waschen. Unter der Fußmatte war auch nichts. Jetzt bleibt noch der <phoneme alphabet="ipa" ph="ˈbluːmənˌtɔp͡f">Blumentopf</phoneme>. Sage [[zum Blumentopf]]. ';
        } else {
            if (attributes.visited.includes("zur Klingel")) {
                output += '<say-as interpret-as="interjection">pfui</say-as>, das war nichts. Aber dreckige Hände kann man ja waschen. Auf ein Neues und weiter gehts. Wohin jetzt? Sage [[zum Blumentopf]] oder [[zur Fußmatte]]. ';
            } else {
                output += 'Bevor du gleich erwischt wirst das du Ihm die Regenrinne saubermachst, habe ich eine Frage, hast du schon geklingelt? Sage [[zur Klingel]]. ';
            }
        }
        return output;
    },
    zum_Blumentopf: function(attributes) {
        var output = '';
        output += '<audio src="https://nordicskills.de/audio/ProfSchwarz/schluesselbund.mp3" /> Bingo. Tja Steve. Dein Riecher hat mal wieder zugeschlagen.<break time="150ms"/> Dann kommen wir ja endlich rein. <break time="100ms"/> Ich hoffe nicht, das er auf dem Boden liegt und <break time="200ms"/> naja du weist schon.<break time="350ms"/> Jaja ich weis. Hast du ja schon gesagt. Ich bin ja schon ruhig. <break time="200ms"/> Los schließ die Tür auf und las uns endlich einen hinweis auf den verbleib vom Professor finden. Sage [[Haustür aufschließen]]. ';
        return output;
    },
    zur_Fußmatte: function(attributes) {
        var output = '';
        if (attributes.visited.includes("zur Klingel") && attributes.visited.includes("zur Regenrinne")) {
            output += 'Hatte ich gerade den Herrn Professor gelobt? Auf die Fußmatte, hat der Postbote aber seinen Dreck gleich mit abgelegt. <say-as interpret-as="interjection">Schade</say-as>, das Du unter der Matte und in der Regenrinne nichts gefunden hast. Bleibt nur noch der <phoneme alphabet="ipa" ph="ˈbluːmənˌtɔp͡f">Blumentopf</phoneme> übrig. Sage [[zum Blumentopf]]. ';
        } else {
            if (attributes.visited.includes("zur Klingel")) {
                output += 'Hatte ich gerade den Herrn Professor gelobt? Auf die Fußmatte, hat der Postbote aber seinen Dreck gleich mit abgelegt. <say-as interpret-as="interjection">Schade</say-as>, das Du unter der Matte keinen Schlüssel gefunden hast. Und jetzt? sage [[zum Blumentopf]] oder [[zur Regenrinne]]. ';
            } else {
                output += 'Bevor du gleich erwischt wirst, wie du Ihm die Fussmatte klaust, habe ich eine Frage, hast du schon geklingelt? Sage [[zur Klingel]]. ';
            }
        }
        return output;
    },
    Haustür_aufschließen: function(attributes) {
        var output = '';
        output += '<audio src="https://nordicskills.de/audio/ProfSchwarz/tuerauf.mp3" /> Gut. Der Schlüssel passt. <say-as interpret-as="interjection">puh</say-as><break time="100ms"/> Glück gehabt. Es richt nicht nach Altem Professor.<break time="250ms"/><say-as interpret-as="interjection">geh nur</say-as><break time="150ms"/> und lass uns ins Haus gehen. Dafür sage [[Haus betreten]] ';
        return output;
    },
    Vier_eins_acht_acht_fünf: function(attributes) {
        var output = '';
        if (attributes.scene["Item_Notizbuch"] == 1) {
            output += 'Was willst du denn noch? Wir haben doch das Buch. Lass uns von hier verschwinden. Dafür sage [[Labor verlassen]]. ';
        } else {
            attributes.scene["Item_Notizbuch"] = 1;
            output += '<audio src="https://nordicskills.de/audio/ProfSchwarz/safe_tuer_auf.mp3" /> Was meinst du?<break time="1s"/> OK. Du hast recht Steve. Hier, nimm das Notizbuch und steck es ein. Wir können uns später anschauen was da drinnen steht. <break time="350ms"/> Und Ja, es ist eine gute Idee von hier zu verschwinden. Dafür sage [[Labor verlassen]]. ';
        }
        return output;
    },
    Haus_betreten: function(attributes) {
        var output = '';
        output += 'Steve, schau mal <break time="100ms"/> Links von dir ist eine Toilette, rechts steht ein Tisch und da vorn ist ein Flur. Wo willst du als erstes suchen? Sage [[zur Toilette]], [[zum Tisch]] oder [[in den Flur]]. ';
        return output;
    },
    zur_Toilette: function(attributes) {
        var output = '';
        if (attributes.scene["Autoschluessel2"] == 1) {
            output += '<audio src="https://nordicskills.de/audio/ProfSchwarz/toilette.mp3" /> ';
            output += 'Sag mal Steve, ist das dein ernst. Hast du hier gerade gepullert? Ich fasse es nicht. Los komm jetzt. Den Schlüssel hast du ja jedenfalls mitgenommen. Lass uns weiter. Sage [[in den Flur]]. ';
        } else {
            output += '<audio src="https://nordicskills.de/audio/ProfSchwarz/toilette.mp3" /> ';
            output += 'Sag mal Steve, ist das dein ernst. Hast du hier gerade gepullert? Ich fasse es nicht. Los komm jetzt. Lass uns weiter. Sage [[in den Flur]] oder [[zum Tisch]]. ';
        }
        output += ' ';
        return output;
    },
    zum_Tisch: function(attributes) {
        var output = '';
        if (attributes.scene["Autoschluessel2"] == 1) {
            output += 'Die Schale auf dem Tisch ist leer <break time="100ms"/> und den Schlüssel darin, hast du schon eingesteckt. Von hieraus gehts in den Flur oder zur Toilette. Such dir was aus und sage [[in den Flur]] oder [[zur Toilette]]. ';
        } else {
            attributes.scene["Autoschluessel2"] = 1;
            output += 'Schau mal dort <break time="200ms"/> in der Schale liegt ein <phoneme alphabet="ipa" ph="ˈʃlʏsl̩">Schlüssel</phoneme>. Nimm den mal mit. <audio src="https://nordicskills.de/audio/ProfSchwarz/schluesselbund.mp3" /> ';
            output += 'Von hieraus gehts in den Flur oder zur Toilette. Such dir was aus und sage [[in den Flur]] oder [[zur Toilette]]. ';
        }
        return output;
    },
    in_den_Flur: function(attributes) {
        var output = '';
        if (attributes.scene["Autoschluessel2"] == 1) {
            output += 'Da vorne ist ein Zimmer. Das sieht aus als wenn es das Wohnzimmer ist. Lass uns rein gehen und alles absuchen. Irgend etwas lässt sich da sicher finden. Irgend etwas was uns weiter hilft. Sage [[ins Wohnzimmer]]. ';
        } else {
            output += 'Hast du nicht was vergessen? Da war doch noch ein Tisch, den du auch durchsuchen wolltest. Oder? Sage [[zum Tisch]] ';
        }
        return output;
    },
    ins_Wohnzimmer: function(attributes) {
        var output = '';
        output += 'Steve, was meinst du? <break time="100ms"/> Du willst den Schreibtisch durchsuchen und ich soll mir mal die Bücherwand vornehmen? <break time="150ms"/> OK mach ich.<break time="500ms"/> Hier ist nichts. <break time="350ms"/> Überhaupt nichts. Ich hatte gehofft wir finden etwas, wenigstens einen kleinen hinweis. <break time="1s"/> Was meinst du? <break time="250ms"/> Warte, ich komme zu dir. <break time="300ms"/> Was hast du denn gefunden? <break time="250ms"/> OK ein Tablet. Und hast du schon rein geschaut? <break time="300ms"/> Achso, das soll ich machen.<break time="300ms"/> Achja, weil du keine Ahnung von diesen Dingen hast. Naja gib mal her. <audio src="https://nordicskills.de/audio/ProfSchwarz/polizei.mp3" /> So was blödes. Jetzt stehen die Bullen vor der Tür. Bestimmt hat einer der Nachbarn uns gesehen wie wir hier ins Haus gegangen sind.<break time="300ms"/> Ja Steve du hast recht. Lass uns hier schnell raus. <break time="350ms"/> Ich habe auch keine lust denen zu erklären was wir hier im Haus vom Professor machen. <break time="200ms"/> Komm. Lass uns verschwinden, dafür sage [[nach hinten verschwinden]]. ';
        return output;
    },
    nach_hinten_verschwinden: function(attributes) {
        var output = '';
        output += 'Steve was machst <prosody pitch="+10%">du denn.</prosody> <break time="250ms"/> <say-as interpret-as="interjection">Ist nicht dein ernst</say-as>. Du willst dich hier verstecken und abwarten bis die weck sind?  <break time="200ms"/> Ich finde es besser nach draußen zugehen und zu verschwinden. <break time="200ms"/> Und was ist? Was willst du machen?  [[nach draußen]] oder [[abwarten]]. ';
        return output;
    },
    nach_draußen: function(attributes) {
        var output = '';
        output += 'Schnell in die Garage <break time="350ms"/> <say-as interpret-as="interjection">hach ja</say-as>, ich habe schon einen Intelligenten und aufmerksamen Chef. <break time="200ms"/> Gut das du den Schlüssel aus der Schale mitgenommen hast. <break time="250ms"/>Cool. Der BMW hier ist mir schon lieber als die Ente. Komm lass uns einsteigen und abhauen. <audio src="https://nordicskills.de/audio/ProfSchwarz/autotueraufundzu.mp3" /> <audio src="https://nordicskills.de/audio/ProfSchwarz/autostartenBMW.mp3" /> Sage [[losfahren]]. ';
        return output;
    },
    abwarten: function(attributes) {
        var output = '';
        output += 'Das ist ein Fehler gewesen. Denn die Polizei kommt jetzt hier rein und findet uns. Ich finde wir stellen uns lieber der Polizei. <say-as interpret-as="interjection">machs gut</say-as> <break time="1s"/> Da du nach der vorläufigen Festnahme deinen Auftrag los bist, hast du leider verloren. <break time="200ms"/> <audio src="https://nordicskills.de/audio/ProfSchwarz/verloren.mp3" /> Wenn du es nochmal versuchen möchtest sage [[in den Flur]] oder beenden um es zu beenden. ';
        return output;
    },
    losfahren: function(attributes) {
        var output = '';
        if (attributes.visited.includes("Ziel A")) {
            output += 'Such dir ein anderes Ziel aus. Ziel A ist sein zuhause. ';
        } else {
            output += '<audio src="https://nordicskills.de/audio/ProfSchwarz/autofahrt.mp3" /> Und, wo sollen wir hinfahren? <break time="200ms"/> Wie das weist du nicht. Ich schau mal in das Navi, wo er zu letzt gewesen ist. <break time="250ms"/> <say-as interpret-as="interjection">na sieh mal einer an</say-as> der Herr Professor war aber viel unterwegs. Hier sind zwei Ziele die er immer wieder angefahren hat. Welches willst du zuerst anfahren? [[Ziel A]] oder [[Ziel B]]. ';
        }
        return output;
    },
    Ziel_A: function(attributes) {
        var output = '';
        if (attributes.visited.includes("Ziel A")) {
            output += 'Da sind wir doch schon. Sehr witzig ';
        } else {
            output += '<audio src="https://nordicskills.de/audio/ProfSchwarz/navi_ziel_erreicht.mp3" /> Ups, sorry das war mein Fehler. Nimm das andere Ziel. Dafür sage [[Ziel B]] ';
        }
        return output;
    },
    Ziel_B: function(attributes) {
        var output = '';
        if (attributes.visited.includes("anhalten")) {
            output += 'Komm <break time="100ms"/> und lass uns das Haus mal näher ansehen. Sage [[Haus ansehen]]. ';
        } else {
            output += '<audio src="https://nordicskills.de/audio/ProfSchwarz/navi_ziel_erreichen.mp3" /> Na dann gib Gas. Ich bin gespannt wo wir landen werden. Ich werde Während der Autofahrt mir mal den Laptop aus dem Haus vom Professor vor nehmen. Mal schaun was ich dort finde. <audio src="https://nordicskills.de/audio/ProfSchwarz/tastatur.mp3" /> <break time="200ms"/> <say-as interpret-as="interjection">mazel tov</say-as> muss man haben. Ich bin drin. <break time="350ms"/>Was meinst du? <break time="1s"/> Wie <break time="100ms"/> was steht da drin? <break time="250ms"/> Steve, gibst du mir mal ein paar Minuten? <break time="150ms"/> Ich mach ja schon. <break time="1s"/> Also <break time="150ms"/> Aha <break time="200ms"/><say-as interpret-as="interjection">mamma mia</say-as><break time="500ms"/><say-as interpret-as="interjection">oh mein gott</say-as> oder besser gesagt, die wollen Gott spielen.<audio src="https://nordicskills.de/audio/ProfSchwarz/navi_ziel_erreichen_ende.mp3" /> OK Steve. Da vorne ist ein Parkplatz. Halte da an, dann erkläre ich dir was ich gefunden habe. <break time="100ms"/> Sage [[anhalten]]. ';
        }
        return output;
    },
    anhalten: function(attributes) {
        var output = '';
        output += 'Lass uns aussteigen denn ich brauch frische Luft.<audio src="https://nordicskills.de/audio/ProfSchwarz/autotueraufundzu.mp3" />Hörzu. Der Professor ist bei seiner Arbeit nach irgendeinem Impfstoff auf eine Formel gestoßen. Die soll angeblich das Altern aufhalten. <break time="150ms"/> Vorab, ich finde sowas total schräg. Aber auch egal. Nachdem der Professor mit <lang xml:lang="en-US">Adams</lang> darüber gesprochen hat, gab es einen riesen Streit. Wegen der rechte und so.<break time="200ms"/> Was meinst du? <break time="600ms"/> Du meinst das er deswegen entführt wurde? <break time="200ms"/> Mag sein, aber hör erst einmal weiter zu. Der Prof. hat hier in seinen Laptop einen versteckten Ordner. Und dort habe ich noch weitere Informationen gefunden. Also er hatte natürlich bedenken und angst, das jemand ihm diese Formel wegnehmen wollte. Daher ist er untergetaucht. <break time="500ms"/> Steve <break time="100ms"/> wenn ich mich hier so umschaue, finde ich, das Haus dahinten sieht doch ziemlich verlassen aus oder? <break time="750ms"/> Genau, du hast recht. <break time="100ms"/> Wir sollten uns das Haus einmal genauer ansehen. Sage [[Haus ansehen]]. ';
        return output;
    },
    Haus_ansehen: function(attributes) {
        var output = '';
        if (attributes.scene["Item_Brechstange"] == 1) {
            output += '<say-as interpret-as="interjection">wow</say-as> Steve. Mit der Brechstange in der Hand, siehst du aus wie ein Schwerverbrecher. Wo willst du jetzt damit hin? Sage [[Hinter das Haus]]. ';
        } else {
            output += 'Und? Wo willst du hin? Sage [[Hinter das Haus]], [[Aufs Dach]], [[Durchs Fenster]] oder [[zum Kofferraum]] ';
        }
        return output;
    },
    Hinter_das_Haus: function(attributes) {
        var output = '';
        if (attributes.scene["Item_Brechstange"] == 1) {
            output += 'OK. Mit der Brechstange aus dem Auto, solltest du die Tür Aufbrechen können. Versuchs es mal. Sage [[Tür aufbrechen]]. ';
        } else {
            output += '<say-as interpret-as="interjection">oh mann</say-as>. Die Tür ist verschlossen und verriegelt. Das wird so nichts. Aber im Kofferraum vom Auto findest du bestimmt was. Schau mal nach und sage [[zum Kofferraum]]. ';
        }
        return output;
    },
    Aufs_Dach: function(attributes) {
        var output = '';
        if (attributes.scene["Item_Brechstange"] == 1) {
            output += 'Steve. Die Brechstange ist aber kein Eispickel. Um damit auf das Dach zu kommen. Versuch was anderes [[Hinter das Haus]] oder [[Durchs Fenster]]. ';
        } else {
            output += '<say-as interpret-as="interjection">nee</say-as> Steve. Das machst du nicht. Wenn du da rauf gehst, fällst du runter und brichst dir dein Genick. Und wer darf sich dann um deine Leiche kümmern? Das Dach ist viel zu nass. Versuch was anderes Sage [[Hinter das Haus]], [[zum Kofferraum]] oder [[Durchs Fenster]]. ';
        }
        return output;
    },
    Durchs_Fenster: function(attributes) {
        var output = '';
        if (attributes.scene["Item_Brechstange"] == 1) {
            output += 'Das wird nichts. Das Gitter ist so massiv, das bekommst du auch mit der Brechstange nicht auf. Versuch was anderes [[Hinter das Haus]] oder [[Aufs Dach]]. ';
        } else {
            output += 'Schau mal Steve. Vor dem Fenster ist ein Gitter, als wären wir in Fortnox. Da kommst du niemals so rein. Versuch was anderes Sage [[Hinter das Haus]], [[zum Kofferraum]] oder [[Aufs Dach]]. ';
        }
        return output;
    },
    zum_Kofferraum: function(attributes) {
        var output = '';
        if (attributes.scene["Item_Brechstange"] == 1) {
            output += 'Was willst du denn noch hier? Du hast doch schon eine Brechstange und im Kofferaum des Wagens liegt nichts mehr. Dann lass uns weitermachen und sage [[Hinter das Haus]] ';
        } else {
            attributes.scene["Item_Brechstange"] = 1;
            output += '<audio src="https://nordicskills.de/audio/ProfSchwarz/kofferraumauf.mp3" /> <say-as interpret-as="interjection">bingo</say-as>. Hier ist eine  Brechstange <break time="100ms"/> nimm die mit. Wer weis für was die gut ist. Sage [[Hinter das Haus]]. ';
        }
        return output;
    },
    Tür_aufbrechen: function(attributes) {
        var output = '';
        if (attributes.visited.includes("Nein wir gehen")) {
            output += 'Na, hast du dir es doch noch einmal überlegt? <break time="250ms"/> Das ist auch gut so, denn wir brauchen den Auftrag und das Geld sowieso. <break time="250ms"/> Dann versuche es noch einmal und sage [[an der Tür Horchen]]. ';
        } else {
            if (attributes.scene["Item_Brechstange"] == 1) {
                output += '<audio src="https://nordicskills.de/audio/ProfSchwarz/tueraufbrechen.mp3" /> <say-as interpret-as="interjection">donnerwetter.</say-as> Du bist so ein starker Mann. Wenn du nicht mein Chef wärst, Tja. <break time="350ms"/> So weiter gehts.<break time="250ms"/> Hier ist ja gar nichts. Außer ein Kühlschrank und ein Tisch mit Stuhl. <break time="300ms"/> Was meinst du? <break time="1s"/> Nein, ich habe die Tür noch nicht gesehen. Wir sind ja auch gerade mal eine Minute hier. <break time="250ms"/> Jaja ich komme schon.<audio src="https://nordicskills.de/audio/ProfSchwarz/fuss_schritte.mp3" /> Was meinst du? Ich soll horchen? Sage [[an der Tür Horchen]]. ';
            } else {
                output += 'Ohne eine Brechstange oder etwas anderes bekommst du die Tür nicht auf. Schau mal im Kofferaum des Autos nach. Dafür Sage [[zum Kofferraum]]. ';
            }
        }
        return output;
    },
    an_der_Tür_Horchen: function(attributes) {
        var output = '';
        output += '<audio src="https://nordicskills.de/audio/ProfSchwarz/schnarchen.mp3" /> Da liegt einer zum pennen <break time="100ms"/> und schnarcht den Holzfällerblues.<break time="350ms"/> Wie? Ich soll die Tür aufmachen und reingehen <break time="350ms"/><say-as interpret-as="interjection">ist nicht dein ernst.</say-as> <say-as interpret-as="interjection">geh nur</say-as>. Wenn der sich erschreckt <break time="150ms"/> und wild um sich ballert, darfst du gerne in der ersten reihe stehen.<break time="250ms"/> So <break time="100ms"/> und jetzt machst du leise die Tür auf. Dafür sage [[Tür leise öffnen]] ';
        return output;
    },
    Tür_leise_öffnen: function(attributes) {
        var output = '';
        output += '<audio src="https://nordicskills.de/audio/ProfSchwarz/fuss_schritte.mp3" /> <amazon:effect name="whispered">Ich glaube das ist der Professor.<break time="500ms"/> </amazon:effect><prosody volume="-3dB">Hallo, sind Sie wach?</prosody><break time="200ms"/> <prosody volume="+4dB"><say-as interpret-as="interjection">huhu.</say-as>Aufwachen.</prosody> <break time="200ms"/> Sind Sie Herr Professor Schwarz? <audio src="https://nordicskills.de/audio/ProfSchwarz/prof_husten.mp3" /> Steve geh mal bitte zum Kühlschrank und hole ein Glas Wasser. Der Professor hat sich verschluckt. <prosody rate="75%">Fünf Minuten später.</prosody> Steve lass uns mal nach vorn gehen. Der Professor will uns was erzählen bzw. erklären. Sage [[Raum verlassen]]. ';
        return output;
    },
    Raum_verlassen: function(attributes) {
        var output = '';
        output += '<audio src="https://nordicskills.de/audio/ProfSchwarz/fuss_schritte.mp3" /> <audio src="https://nordicskills.de/audio/ProfSchwarz/prof_schwarz_text2.mp3" /> Das ist Steven Brown ein Privatdetektiv <break time="100ms"/> und ich bin seine Kollegin. <break time="250ms"/> Herr Brown und ich, haben den Auftrag Sie zu suchen und zu finden. <break time="100ms"/> <audio src="https://nordicskills.de/audio/ProfSchwarz/prof_schwarz_text1.mp3" /> Steve? Was meinst du helfen wir Ihm? <break time="250ms"/> Dann sage [[Ja wir helfen]] oder [[Nein wir gehen]]. ';
        return output;
    },
    Ja_wir_helfen: function(attributes) {
        var output = '';
        output += '<audio src="https://nordicskills.de/audio/ProfSchwarz/prof_schwarz_text3.mp3" />Gute Idee, vom langen stehen bekommt man Krampfadern. <audio src="https://nordicskills.de/audio/ProfSchwarz/stuhl_ziehen.mp3" /> OK. Herr Professor. Dann mal Butter bei die Fische. Und Steve vergesse nicht dir Notizen zu machen <break time="250ms"/>. <audio src="https://nordicskills.de/audio/ProfSchwarz/prof_schwarz_text4.mp3" /> Und was ist jetzt mit der Entdeckung? Gibt es das Mittel schon zu kaufen? Steve <break time="100ms"/> du weist ich hasse meine Falten.<audio src="https://nordicskills.de/audio/ProfSchwarz/prof_schwarz_text5.mp3" /> <say-as interpret-as="interjection">alles klar</say-as>. Ich will das zeug doch nicht haben. Dann behalte ich lieber meine Falten. <break time="400ms"/> Ich helfe auf jeden Fall. <break time="150ms"/> Was ist mit dir Steve? Sage [[Nein keine Lust]] oder [[Ja natürlich]]. ';
        return output;
    },
    Nein_wir_gehen: function(attributes) {
        var output = '';
        output += '<audio src="https://nordicskills.de/audio/ProfSchwarz/prof_schwarz_geht.mp3" /><audio src="https://nordicskills.de/audio/ProfSchwarz/fuss_schritte.mp3" /><audio src="https://nordicskills.de/audio/ProfSchwarz/tuerzu.mp3" /><break time="150ms"/><say-as interpret-as="interjection"><break time="150ms"/> ich glaub mich laust der affe <break time="100ms"/></say-as> was ist denn mit dir los? Du weist schon, das wir nicht nur den Auftrag los sind, sondern auch pleite. Vielen Dank dafür.<break time="200ms"/><audio src="https://nordicskills.de/audio/ProfSchwarz/verloren.mp3" /> Leider hast du verloren. Wenn du es nochmal versuchen möchtest, sage [[Tür aufbrechen]] oder beenden. ';
        return output;
    },
    Nein_keine_Lust: function(attributes) {
        var output = '';
        output += '<audio src="https://nordicskills.de/audio/ProfSchwarz/prof_schwarz_geht.mp3" /><audio src="https://nordicskills.de/audio/ProfSchwarz/fuss_schritte.mp3" /><audio src="https://nordicskills.de/audio/ProfSchwarz/tuerzu.mp3" /><break time="150ms"/><say-as interpret-as="interjection"><break time="150ms"/> ich glaub mich laust der affe <break time="100ms"/></say-as> was ist denn mit dir los? Du weist schon, das wir nicht nur den Auftrag los sind, sondern auch pleite. Vielen Dank dafür.<break time="200ms"/><audio src="https://nordicskills.de/audio/ProfSchwarz/verloren.mp3" /> ';
        if (attributes.visited.includes("Nein wir gehen")) {
            output += 'Leider hast du schon wieder verloren. Das war einmal zuviel. Wenn du es nochmal versuchen möchtest, sage nochmal spielen oder beenden. ';
        } else {
            output += 'Leider hast du verloren. Wenn du es nochmal versuchen möchtest, sage [[Tür aufbrechen]] oder beenden. ';
        }
        return output;
    },
    Ja_natürlich: function(attributes) {
        var output = '';
        if (attributes.visited.includes("nein verprügeln") || attributes.visited.includes("Nein erpressen")) {
            output += 'Gut Steve, jeder gute Mensch hat eine zweite Chance verdient. Du auch. Aber überlege diesmal und entscheide dich richtig. Such dir etwas aus und sage [[Ich verprügel Ihn]], [[Ich Erpresse Ihn]] oder [[Eine Falle stellen]]. ';
        } else {
            output += 'Steve du bist der Boss. <break time="300ms"/> Na gut Herr Professor <break time="200ms"/> dann erzählen sie mal weiter.<audio src="https://nordicskills.de/audio/ProfSchwarz/prof_schwarz_text6.mp3" /><say-as interpret-as="interjection">oh mann</say-as><break time="200ms"/>Steve ich hatte dir gesagt, das dieser Sir Typ, mir nicht gefällt. Und was meinst du Steve? Was willst du machen. Du hast doch bestimmt eine Idee oder? <break time="150ms"/> Such dir etwas aus und sage [[Ich verprügel Ihn]], [[Ich Erpresse Ihn]] oder [[Eine Falle stellen]]. ';
        }
        return output;
    },
    Ich_verprügel_Ihn: function(attributes) {
        var output = '';
        output += '<say-as interpret-as="interjection">wie du meinst</say-as> <break time="100ms"/> aber ich glaube nicht, das es eine gute Idee ist. Auch wenn der Typ es sicher verdient hat. Willst du dir das nicht noch mal überlegen? <break time="150ms"/> Dann sage [[Nein verprügeln]], [[Ich Erpresse Ihn]] oder [[Eine Falle stellen]]. ';
        return output;
    },
    Ich_Erpresse_Ihn: function(attributes) {
        var output = '';
        output += '<say-as interpret-as="interjection">ohne scheiß</say-as> <break time="200ms"/> aber ich glaube nicht das es eine gute Idee ist, auch wenn der Typ es sicher verdient hat. Willst du dir das nicht noch mal überlegen? <break time="200ms"/> Dann sage [[Nein erpressen]], [[Ich verprügel Ihn]] oder [[Eine Falle stellen]]. ';
        return output;
    },
    Eine_Falle_stellen: function(attributes) {
        var output = '';
        output += '<say-as interpret-as="interjection">halleluja</say-as> <break time="100ms"/> das ist eine richtig gute Idee. Der Typ gehört hinter Gitter und der Professor wäre in Sicherheit. <break time="100ms"/> Gut. Und weist du uns in deinen Plan ein? <break time="500ms"/> Wie? Noch nicht.<break time="650ms"/> Ach so. Du musst dir das noch genau überlegen. Aber wir sollen schon einmal zurück zur Uni fahren. <break time="100ms"/> Gut dann lass uns zum Parkplatz gehen und ins Auto einsteigen. Dafür sage [[zum Auto gehen]] ';
        return output;
    },
    Nein_verprügeln: function(attributes) {
        var output = '';
        output += '<audio src="https://nordicskills.de/audio/ProfSchwarz/autostartenlosfahren.mp3" /> Da vorn auf dem Parkplatz <break time="100ms"/> da steht der Typ. <break time="350ms"/> <audio src="https://nordicskills.de/audio/ProfSchwarz/verpruegeln.mp3" /> Nach dem du Herrn Adams eine auf die Zwölf gegeben hast, kam die Polizei <audio src="https://nordicskills.de/audio/ProfSchwarz/polizei.mp3" /> Festnahme. Urteil. Gefängnis. Ich sagte doch, ganz schlechte Idee.<break time="200ms"/><audio src="https://nordicskills.de/audio/ProfSchwarz/verloren.mp3" /> Leider hast du verloren. Wenn du es nochmal versuchen möchtest, sage [[Ja natürlich]]. <break time="100ms"/> Wenn nicht, sage beenden. ';
        return output;
    },
    Nein_erpressen: function(attributes) {
        var output = '';
        output += '<audio src="https://nordicskills.de/audio/ProfSchwarz/autostartenlosfahren.mp3" /> Da vorn auf dem Parkplatz steht der Typ. Nach dem du versucht hast Ihn zu erpressen, hat er dich angezeigt. <break time="150ms"/> Den Rest übernimmt die Polizei.<audio src="https://nordicskills.de/audio/ProfSchwarz/polizei.mp3" /> Festnahme. Urteil. Gefängnis. Ich sagte doch, ganz schlechte Idee.<break time="200ms"/><audio src="https://nordicskills.de/audio/ProfSchwarz/verloren.mp3" /> Leider hast du verloren. Wenn du es nochmal versuchen möchtest, sage [[Ja natürlich]] <break time="100ms"/> Wenn nicht, sage beenden. ';
        return output;
    },
    zum_Auto_gehen: function(attributes) {
        var output = '';
        output += '<audio src="https://nordicskills.de/audio/ProfSchwarz/autotueraufundzu.mp3" /> <audio src="https://nordicskills.de/audio/ProfSchwarz/autostartenBMW.mp3" /> Gut anschnallen <break time="250ms"/> und Steve? wohin gehts jetzt? Oder bleibt das auch noch ein Geheimnis? <break time="300ms"/> Achso. Du willst erst einmal zur Uni. Dann sage [[zur Uni]]. ';
        return output;
    },
    zur_Uni: function(attributes) {
        var output = '';
        output += 'Was soll ich mit deinem Notizblock? <break time="250ms"/> Lesen? Ok mach ich. <audio src="https://nordicskills.de/audio/ProfSchwarz/notizblockblaettern.mp3" /> Achso <break time="500ms"/> Aha <break time="500ms"/> Gut das verstehe ich. <break time="400ms"/> Ok Steve. Klingt nach einem guten Plan. So ich fass das noch mal zusammen. <break time="150ms"/> Du willst das ich <lang xml:lang="en-US">Sir Adams</lang> ablenke. Damit du die Video Kamera verstecken kannst mit der du die Aufnahmen machst, wie <lang xml:lang="en-US">Adams</lang> den Professor bedroht. <break time="150ms"/> Ich hoffe, das alles so klappt wie du dir das vorgestellt hast. <audio src="https://nordicskills.de/audio/ProfSchwarz/autofahrt.mp3" /> Da vorne ist der Parkplatz von der Uni. Steve und jetzt? Sage [[zum Uni Parkplatz]]. ';
        return output;
    },
    zum_Uni_Parkplatz: function(attributes) {
        var output = '';
        if (attributes.visited.includes("Wir gehen rechts")) {
            output += '<audio src="https://nordicskills.de/audio/ProfSchwarz/voegelimwald_kurz.mp3" /> Hast du dir es anders überlegt. Wohin denn jetzt? [[Wir nehmen links]] oder [[Die Hintertür benutzen]] ';
        } else {
            if (attributes.visited.includes("Wir nehmen links")) {
                output += '<audio src="https://nordicskills.de/audio/ProfSchwarz/voegelimwald_kurz.mp3" /> Hast du dir es anders überlegt. Wohin denn jetzt? [[Wir gehen rechts]] oder [[Die Hintertür benutzen]] ';
            } else {
                output += '<audio src="https://nordicskills.de/audio/ProfSchwarz/autotueraufundzu.mp3" /> Professor Schwarz meint, du solltest links gehen. <break time="200ms"/> Ich glaube, rechts ist besser. Aber, Du bist der Chef du kannst auch die Hintertür benutzen, <break time="200ms"/> denn es ist dein Plan. <break time="300ms"/> Wohin gehen wir? Sage [[Wir nehmen links]], [[Wir gehen rechts]] oder [[Die Hintertür benutzen]]. ';
            }
        }
        return output;
    },
    Wir_nehmen_links: function(attributes) {
        var output = '';
        output += '<say-as interpret-as="interjection">achtung</say-as>. Wenn du weiter gehst kann es sein, das <lang xml:lang="en-US">Adams</lang> uns sieht. Willst du es trotzdem versuchen? Dann sage [[Wir gehen weiter]]. ';
        return output;
    },
    Die_Hintertür_benutzen: function(attributes) {
        var output = '';
        output += '<audio src="https://nordicskills.de/audio/ProfSchwarz/zugangverboten.mp3" /> Steve. <lang xml:lang="en-US">Adams</lang> hat den Code geändert. <break time="100ms"/> Versuch mal den Code Umgekehrt einzugeben. Also die Letzte Zahl zu erst usw. <break time="100ms"/>. Vielleicht klappt das. <break time="100ms"/> Nur gut, das du das Passwort aus dem Labor noch hast. Der Zettel ist in der <phoneme alphabet="ipa" ph="ˈtaʃə">Tasche</phoneme>. <break time="200ms"/> Du hast ihn doch noch. Oder? <break time="150ms"/> Sonnst kuck in die App. Da steht er auch drin. <break time="100ms"/> Ich hatte Ihn dir da rein geschrieben. <break time="250ms"/> Wie gesagt, versuche es umgekehrt. ';
        if (2 == 1) {
            output += '[[fünf acht acht eins vier]] ';
        }
        return output;
    },
    Wir_gehen_rechts: function(attributes) {
        var output = '';
        output += '<say-as interpret-as="interjection">verdammt</say-as>. Da vorn ist die Security. Immer musst du recht haben. <break time="100ms"/> Grumel. <break time="250ms"/>Was meinst du? <break time="250ms"/> Ich soll versuchen Sie abzulenken. <say-as interpret-as="interjection">ohne scheiß</say-as>. Und das ist dein ernst. Und wenn es schief geht, bin ich die Dumme.<break time="150ms"/> Willst du nicht lieber zurückgehen und einen anderen <phoneme alphabet="ipa" ph="veːk">weg</phoneme> versuchen? ';
        output += 'Sage [[zum Uni Parkplatz]] oder [[sicherheitspersonal ablenken]]. ';
        return output;
    },
    sicherheitspersonal_ablenken: function(attributes) {
        var output = '';
        output += '<say-as interpret-as="interjection">machs gut.</say-as> Denn die Security, hat uns auf Anweisung von <lang xml:lang="en-US">Sir Adams</lang> festgenommen. Er selber konnte fliehen und ist bis heute untergetaucht. Der Professor wird ab heute in Angst und schrecken leben.<break time="200ms"/><audio src="https://nordicskills.de/audio/ProfSchwarz/verloren.mp3" /> Leider hast du verloren. Wenn du es nochmal versuchen möchtest, sage nochmal spielen oder beenden. ';
        return output;
    },
    Wir_gehen_weiter: function(attributes) {
        var output = '';
        output += 'Gut. Dann lass uns aber <emphasis level="moderate">ganz langsam am Fenster vorbei gehen.</emphasis>. Ich hoffe das er nichts bemerkt hat. <break time="350ms"/> <say-as interpret-as="interjection">puh</say-as><break time="150ms"/> Das war aber knapp. So schnell <break time="100ms"/> lass uns weiter. <break time="150ms"/> Sage [[zum Vordereingang]]. ';
        return output;
    },
    zum_Vordereingang: function(attributes) {
        var output = '';
        output += 'Steve. Was machen wir wenn wir im Gebäude sind? <break time="1s"/> OK. Ich soll mit dem Professor ins Büro gehen und Du willst dann vom Nebenraum mit der Kamera die Aufnahme machen. <say-as interpret-as="interjection">alles klar</say-as>. OK Chef. So machen wir das.<break time="100ms"/> Dann ab zum Büro <break time="100ms"/> Herr Professor. <break time="150ms"/> Lassen wir die Falle zuschnappen. <break time="250ms"/><audio src="https://nordicskills.de/audio/ProfSchwarz/festnahme_adams.mp3" /> <say-as interpret-as="interjection">was zur hölle</say-as>ist denn da los? <prosody volume="+4dB"><emphasis level="reduced">Steve. Komm schnell her.</emphasis></prosody> Schau dir das an. Die Polizei nimmt gerade <lang xml:lang="en-US">Sir Adams</lang> fest.<break time="150ms"/>So weit ich das verstanden habe, geht es um Steuerbetrug und Urkundenfälschung. Dafür soll er wohl mindestens fünf Jahre bekommen. Tja. Da sage ich doch gerne. <say-as interpret-as="interjection">arrivederci</say-as>. <break time="150ms"/> Warte Steve, ich komm mit. [[zum Büro vom Professor]] ';
        return output;
    },
    zum_Büro_vom_Professor: function(attributes) {
        var output = '';
        output += 'Tja Herr Professor. Herr Brown und ich sind froh, das Sie in Sicherheit sind und es jetzt zu Ende ist. Steve. Ich hoffe du hast den Scheck von <lang xml:lang="en-US">Adams</lang> schon vorher eingelöst. Denn jetzt wird das sonnst nichts mehr. <break time="350ms"/> Gut dann lass uns nach Hause. <break time="350ms"/> Ihnen Herr Professor, wünschen wir noch einen schönen Tag. <break time="2s"/> <say-as interpret-as="interjection">oh nein</say-as> dir gefällt dieser Schluss nicht? Mir auch nicht. <say-as interpret-as="interjection">na und?</say-as> dann sage [[zum Uni Parkplatz]] und Spiele von dort weiter und nimm einen anderen Schluss. Es gibt mindestens 3 Enden. Viel Spaß dabei wünsche ich dir und das Team von Nordic Skills. ';
        return output;
    },
    fünf_acht_acht_eins_vier: function(attributes) {
        var output = '';
        output += '<audio src="https://nordicskills.de/audio/ProfSchwarz/tastenton_labor.mp3" /> Sehr gut. <break time="300ms"/> So im Vorraum sind wir schon mal. Steve <break time="100ms"/> und Du willst jetzt hier im Flur bleiben. Während der Professor und ich in das Büro von <lang xml:lang="en-US">Adams</lang> gehen. Richtig? <break time="350ms"/> Is Klar. Und wenn du mir ein Zeichen gibst, komme ich wieder raus zu dir. Dann nimmst du alles mit der Videocam auf. <break time="350ms"/> Ja. Genauso wie besprochen. <break time="300ms"/> Ich weis.  Gut dann versteck dich und wir gehen rein. Mach uns aber vorher noch die Bürotür auf. Dafür sage [[Bürotür öffnen]]. ';
        return output;
    },
    Bürotür_öffnen: function(attributes) {
        var output = '';
        output += 'Hallo Herr <lang xml:lang="en-US">Adams</lang><break time="100ms"/> <say-as interpret-as="interjection">oje</say-as> <break time="200ms"/> Ich meine <lang xml:lang="en-US">Sir Adams</lang>. <break time="250ms"/> Herr Brown ist schon wieder in unser Büro gefahren. Wie Sie sehen, haben wir den Professor gefunden. Ich soll Ihnen noch Grüße und Vielen Dank für den Auftrag und das Vertrauen in die Detektei Brown bestellen. <break time="200ms"/> <audio src="https://nordicskills.de/audio/ProfSchwarz/sir_adams_das_ende.mp3" /> <say-as interpret-as="interjection">ähm</say-as><break time="150ms"/> So, ich gehe dann jetzt. Die Rechnung schicken wir Ihnen zu. Auf wiedersehen. Sage [[ein Zeichen geben]]. ';
        return output;
    },
    ein_Zeichen_geben: function(attributes) {
        var output = '';
        output += 'Ok Steve. Da bin ich wieder. <break time="150ms"/> Wenn du alles bereit hast, dann mach mal die Kamera an und lass uns zuhören. Ich bin mal gespannt was da abgeht <break time="150ms"/> und ob der Trottel auf unsere Falle reinfällt. <break time="250ms"/><audio src="https://nordicskills.de/audio/ProfSchwarz/sir_adams_das_ende1.mp3" /> <break time="100ms"/> Steve das reicht. Der Typ spinnt ja wohl. Ich kriege gleich die Kriese. <break time="250ms"/> Ich bin sicher, das die Aufnahme reicht um den Idioten <lang xml:lang="en-US">Adams</lang> zu überführen. <break time="200ms"/> Komm. Lass uns das beenden und in sein Büro gehen, um Ihn damit zu konfrontieren. Dafür sage [[zu Adams gehen]]. ';
        return output;
    },
    zu_Adams_gehen: function(attributes) {
        var output = '';
        output += 'Hallo Herr <lang xml:lang="en-US">Adams</lang>. <break time="250ms"/> Herr Professor, kommen Sie bitte zu Herrn Brown und mir. <audio src="https://nordicskills.de/audio/ProfSchwarz/fuss_schritte.mp3" /><audio src="https://nordicskills.de/audio/ProfSchwarz/sir_adams_das_ende2.mp3" /> <say-as interpret-as="interjection">Tatsächlich</say-as><break time="150ms"/><say-as interpret-as="interjection">ach</say-as><break time="200ms"/> Herr Brown hatte das verlangen ihren Erpressungsversuch, die Drohungen und alles andere auf Video festzuhalten. <break time="200ms"/> Denn Herr Brown und ich natürlich auch, sind der festen Überzeugung, das die Polizei sowie die Staatsanwaltschaft, begeistert davon sind dieses Videomaterial, sowie die anderen Unterlagen zu bekommen. <audio src="https://nordicskills.de/audio/ProfSchwarz/sir_adams_das_ende3.mp3" /><lang xml:lang="en-US">Adams</lang> warten Sie. <break time="200ms"/> Steve. Zeige dem Herrn <lang xml:lang="en-US">Adams</lang>, das Sir <break time="200ms"/> lassen wir jetzt mal weg, das Notizbuch und die Bänder aus der Überwachungskamera. Dafür sage [[das Notizbuch zeigen]]. ';
        return output;
    },
    das_Notizbuch_zeigen: function(attributes) {
        var output = '';
        output += '<audio src="https://nordicskills.de/audio/ProfSchwarz/sir_adams_das_ende4.mp3" /> Tja, das kommt wenn man das beste Ermittlungsteam, die Detektei Brown beauftragt. Nicht war Steve? <break time="300ms"/> So. Rufst du bitte die Polizei an und teile den Beamten bitte mit, das Sie eines Ihrer älteren Fahrzeuge schicken sollen. <break time="300ms"/> Denn das <prosody rate="75%">pieps</prosody> <break time="100ms"/> Entschuldigung <break time="150ms"/>grunns, hat es nicht besser verdient. <audio src="https://nordicskills.de/audio/ProfSchwarz/handy_toene.mp3" /><audio src="https://nordicskills.de/audio/ProfSchwarz/pausenmusic.mp3" /><prosody rate="75%">Eine Stunde später.</prosody> So Steve das wars. ';
        output += '<lang xml:lang="en-US">Adams</lang> ist hinter Gittern, Professor Schwarz bei seiner Familie und wir beide haben es mal wieder geschafft. ';
        if (attributes.scene["Item_Haarspange"] == 1) {
            output += '<break time="200ms"/> <say-as interpret-as="interjection">ach ja</say-as>, wir hatten doch in der Seitengasse diese Haarspange gefunden. Ich habe vorhin gelesen, dass der Finder dafür eine Belohnung von 5.000 Euro bekommt. <break time="200ms"/> Klasse Steve, dann kannst du mich später zum Essen einladen. <break time="200ms"/> Hier, ';
        } else {
            output += 'Übrigens hatte ich vorhin in der Zeitung gelesen, dass jemand 5.000 Euro für eine verlorene Haarspange bezahlt. Schade, die hätte ich gerne gefunden. <break time="200ms"/> aber egal, ';
        }
        output += 'diese Flasche habe ich im Kühlschrank gefunden und die beiden Gläser, waren im Schrank. Mach die Flasche mal auf. Sage dafür [[Flasche öffnen]]. ';
        return output;
    },
    Flasche_öffnen: function(attributes) {
        var output = '';
        output += '<audio src="https://nordicskills.de/audio/ProfSchwarz/champagner_auf.mp3" /> Danke. <break time="100ms"/> Das haben wir uns verdient. <break time="100ms"/> Prost Steve <audio src="https://nordicskills.de/audio/ProfSchwarz/champagner_anstossen.mp3" /> Tja. Mit der Abschlagzahlung von <lang xml:lang="en-US">Adams</lang> <break time="100ms"/> und dem großzügigen Bonus vom Professor, kann ich nachher deine restlichen Rechnungen bezahlen. <break time="200ms"/> Und dann brauche ich mindestens eine Woche Sonderurlaub von Dir. <break time="700ms"/> Was sagst du? <break time="200ms"/> Ja schon gut. Ich bin jetzt still. <audio src="https://nordicskills.de/audio/ProfSchwarz/gewonnen.mp3" /> Hallo Du <break time="500ms"/> Ja du vor dem Alexagerät. <break time="100ms"/> Wir wünschen dir Herzlichen Glückwunsch. Denn Du hast das Spiel gewonnen. <break time="100ms"/> Bis bald denn wir hören uns sicher wieder, bei deinem 3. Auftrag. <break time="200ms"/> Sage Beenden um das Spiel zu beenden. ';
        return output;
    },
}