module.exports = {
    Elfin: function(attributes) {
        var output = '';
        output += '<audio src="https://nordicskills.de/audio/DerZauberwaldKendell/teleport_magier.mp3" /> <break time="100ms"/> Hallo meine Elfin. Schön, dass du hier angekommen bist. <break time="100ms"/>Vorab <break time="100ms"/> bevor wir weiter machen, brauchst du einen Namen. Da du zu den Lichtalben gehörst, bekommst du von mir den Namen Lyria. <audio src="https://nordicskills.de/audio/DerZauberwaldKendell/zauber_spruch.mp3" /> Dieser Name kommt aus dem Nordischen und bedeutet Licht. <break time="150ms"/> Bevor du dich gleich auf den Weg machst, erkläre ich dir noch einiges. Außerdem brauchst du noch einen Beutel sowie ein Paar nützliche Dinge, da komme ich aber später drauf zurück. <break time="100ms"/> Nimm dir jetzt die Schriftrollen vom Tisch und dann treffen wir uns draußen beim Lagerfeuer. Um sie zu nehmen sage, [[Schriftrollen nehmen]]. ';
        return output;
    },
    Schriftrollen_nehmen: function(attributes) {
        var output = '';
        output += '<audio src="https://nordicskills.de/audio/DerZauberwaldKendell/lyria_zauberbuch.mp3" /><audio src="https://nordicskills.de/audio/DerZauberwaldKendell/holztuerauf.mp3" /> um die Holzhütte zu verlassen, sage [[Zum Lagerfeuer]]. ';
        return output;
    },
    Zum_Lagerfeuer: function(attributes) {
        var output = '';
        output += '<audio src="https://nordicskills.de/audio/DerZauberwaldKendell/zum_lagerfeuer.mp3" /> Hallo Lyria. Schön, dass du die Schriftrollen gefunden und den weg zu mir gefunden hast. <break time="150ms"/> Komm zu mir. Du darfst dich ans Feuer setzen, dich aufwärmen und dort die erste Schriftrolle lesen. <break time="250ms"/> So lernst du deinen ersten Zauber. Pass aber bitte auf, falsch gelernte Sprüche können im Chaos enden. <break time="250ms"/> <audio src="https://nordicskills.de/audio/DerZauberwaldKendell/schriftrolle.mp3" /> <audio src="https://nordicskills.de/audio/DerZauberwaldKendell/zauber_spruch.mp3" /> Gut gemacht Lyria. Ab sofort beherrschst du den Zauber Momentum. Mit diesem kannst du dich an verschiedene Orte Transportieren. Leider funktioniert dieser Zauber nur auf kurze Entfernungen. <break time="250ms"/> Damit du siehst wie der neue Zauber funktioniert, probiere ihn doch gleich einmal aus. <break time="200ms"/> Dafür machst du folgendes. Du nennst deinen Zauberspruch und den Ort. In diesem Fall sagst du [[Momentum Baumhaus]]. ';
        return output;
    },
    Momentum_Baumhaus: function(attributes) {
        var output = '';
        output += '<audio src="https://nordicskills.de/audio/DerZauberwaldKendell/teleport_magier.mp3" /> Hallo Lyria, da bist du ja. Das hat doch klasse funktioniert. <break time="150ms"/> Gut gut. Hier steht eine Truhe in der, dass hatte ich dir ja schon gesagt, einige Dinge sind, die du für die Reise brauchst. <break time="150ms"/> Bevor du die Truhe öffnen kannst, musst du noch einen weiteren Zauber lernen. <break time="150ms"/> Dafür nimm deine nächste Schriftrolle und lese diese. Dann lernst du den Zauber Patentibus. <break time="150ms"/> Dieser Zauber ist wie alle Zauber wichtig. Du brauchst ihn, um als Elfin verschlossene Truhen und Türen zu öffnen. Um die Schriftrolle zu öffnen und den richtigen Zauber zu lernen, sage [[Schriftrolle Zwei lesen]]. ';
        return output;
    },
    Schriftrolle_Zwei_lesen: function(attributes) {
        var output = '';
        output += '<audio src="https://nordicskills.de/audio/DerZauberwaldKendell/schriftrolle.mp3" /> <audio src="https://nordicskills.de/audio/DerZauberwaldKendell/zauber_spruch.mp3" /><say-as interpret-as="interjection">huch</say-as>. Du lernst aber schnell. <break time="200ms"/> Und auch deinen zweiten Zauber kannst du gleich einmal ausprobieren. Mach die Truhe auf, dafür sage deinen neuen Zauberspruch und das was du öffnen möchtest. <break time="150ms"/> In diesem Fall, sage [[Patentibus Truhe]]. ';
        return output;
    },
    Patentibus_Truhe: function(attributes) {
        var output = '';
        if (attributes.visited.includes("patentibus")) {} else {
            attributes.scene["Item_BeutelKlein"] = 1;
            attributes.scene["Item_Lampe"] = 1;
            attributes.scene["Item_Feuerstein"] = 1;
            attributes.scene["Item_Magietrank"] = 3;
            attributes.scene["Item_Lebenstrank"] = 3;
            attributes.scene["Item_Gesundheitstrank"] = 3;
            attributes.scene["Item_Brot"] = 3;
            attributes.scene["Item_Wasserflasche"] = 1;
        }
        output += '<audio src="https://nordicskills.de/audio/DerZauberwaldKendell/zauber_spruch.mp3" /><audio src="https://nordicskills.de/audio/DerZauberwaldKendell/truhe_schloss_auf.mp3" /> So machst du es richtig. <break time="150ms"/> Dann wollen wir doch einmal schauen was du gefunden hast. <audio src="https://nordicskills.de/audio/DerZauberwaldKendell/baldur_beutel_mit_zauber.mp3" /> Lyria jetzt pass auf. Ich erkläre dir ein paar Dinge. <break time="150ms"/> Mit der Magie ist das so, jedes Mal, wenn du sie einsätzt, kostet es dich Energie. <break time="150ms"/> Wenn du drei Zauber ausgesprochen hast, brauchst du einen Magietrank. <break time="100ms"/> Mit diesem Trank kannst du deine Energie wieder auffüllen und du kannst weiter Zaubern. Aber wieder nur drei Mal, also sei sparsam mit dem Zaubern. Denn du weißt ja nicht was noch auf dich zukommt. <break time="200ms"/> Die Lampe und den Feuerstein brauchst du sicherlich später noch. Damit du bei Kräften bleibst, musst du was Essen. Eine Trinkflasche mit Wasser und 3 Stücke Brot habe ich dir auch in den Beutel gelegt. Nimm den Beutel mit und mach dich dann auf den Weg. Dafür musst du einen Momentum Zauber sprechen. Sage [[Momentum Waldweg]]. ';
        return output;
    },
    Momentum_Waldweg: function(attributes) {
        var output = '';
        if (attributes.visited.includes("Momentum Waldweg")) {} else {
            attributes.scene["Item_Magietrank"] -= 1;
        }
        output += '<audio src="https://nordicskills.de/audio/DerZauberwaldKendell/teleport_magier.mp3" /> Lyria das war dein dritter Zauber. Jetzt musst du einen Magietrank nehmen. Nimm diese Flasche und trink. <break time="150ms"/> <audio src="https://nordicskills.de/audio/DerZauberwaldKendell/zaubertrank_trinken.mp3" /> Ok. <break time="150ms"/> Da du jetzt alles für die lange Reise hast, geht es jetzt los. <break time="200ms"/> Während wir dem Weg folgen, erzähle ich dir etwas über Kendell den Zauberwald <break time="100ms"/> und warum wir uns auf die Suche machen. <audio src="https://nordicskills.de/audio/DerZauberwaldKendell/waldweg_steine_laub_voegel.mp3" /> Kendell ist aus einem Streit zwischen zwei Riesen entstanden. Die Riesen waren Brüder und ihr Streit dauerte über 350 Jahre. <break time="200ms"/> Bis heute weiß keiner worum es bei diesem Streit ging. Aber eines kann ich dir sagen, in diesem Streit ging es hart her. <break time="200ms"/> Die Riesen bewarfen sich mit Felsbrocken und Bäumen. <break time="250ms"/> <say-as interpret-as="interjection">ähm</say-as> Lyria? sollen wir uns nicht eine kleine Pause gönnen? Dann kann ich dir den Rest der Geschichte in Ruhe erzählen. <break time="200ms"/> Dafür sage [[Pause einlegen]] <break time="150ms"/> oder wenn dich die Geschichte nicht interessiert, sage [[Reise fortsetzen]]. ';
        output += ' ';
        return output;
    },
    Pause_einlegen: function(attributes) {
        var output = '';
        output += '<audio src="https://nordicskills.de/audio/DerZauberwaldKendell/waldweg_steine_laub_voegel.mp3" /> Das hier ist ein guter Platz. Lass uns hier eine Pause machen und ich erzähle dir wie es weiter geht. <break time="300ms"/> Hörzu und pass gut auf. <break time="200ms"/> Die Riesen beschimpften sich, sie bewarfen sich, sie stürzten und fielen hin. <break time="100ms"/> So formten Sie die Täler <break time="100ms"/> und Hügel <break time="250ms"/> und so entstand Kendell der Zauberwald. <break time="250ms"/> Jetzt fragst du dich sicher warum der Wald verzaubert ist? <break time="350ms"/> Tja, das verrate ich dir. <break time="100ms"/> Zu Beginn trauten sich wegen der Riesen niemand in den Wald <break time="150ms"/> und so haben sich die Trolle und die Unholde hier niedergelassen. <break time="200ms"/> Seither gibt es in Kendell, viele mystische Wesen <break time="150ms"/> und man muss aufpassen, dass man nicht ausgeraubt, verzaubert, verhext <break time="150ms"/> oder sogar schlimmeres wird. <break time="250ms"/> Aber was sage ich, du wirst es ja selber sehen und erleben. <break time="250ms"/> Nun gut. <break time="150ms"/> Jetzt weist du wie der Ort Kendell entstanden ist. Aber du weist noch nicht was deine Aufgabe ist. <break time="250ms"/> Ich brauche dich um mir etwas aus dem Wald zu holen. <break time="150ms"/> Hörzu. In der Mitte des Waldes ist eine Höhle, in dieser Höhle ist eine Truhe versteckt und in ihr liegt ein mächtiger Zauberstab. <break time="200ms"/> Diesen sollst du mit mir holen. <break time="150ms"/> Ich weiß, das ist viel verlangt, aber machst du es für mich? <break time="150ms"/> [[Ja]] oder [[Nein]]. ';
        return output;
    },
    Reise_fortsetzen: function(attributes) {
        var output = '';
        if (attributes.scene["GeschichteTeil1"] == 1) {
            if (attributes.scene["MagietrankBekommenReiseFortsetzen"] == 1) {
                output += 'Weißt du was? Ich glaube das wird ein tolles und spannendes Abenteuer mit dir. <break time="100ms"/> Nun gut, Sage [[Weiter gehen]]. ';
            } else {
                attributes.scene["MagietrankBekommenReiseFortsetzen"] = 1;
                attributes.scene["Item_Magietrank"] += 1;
                output += 'Ach ja, das habe ich fast vergessen. Weil du mir gerade beim Erzählen der Geschichte so aufmerksam zugehört hast, schenke ich dir noch einen weiteren Magietrank. <audio src="https://nordicskills.de/audio/DerZauberwaldKendell/zauber_geschenkt.mp3" /> Weißt du was? Ich glaube das wird ein großartiges und spannendes Abenteuer mit dir. <break time="100ms"/> Sage [[Weiter gehen]]. ';
            }
        } else {
            output += '<say-as interpret-as="interjection">schade</say-as><break time="250ms"/> Ich hätte dir so gerne die Geschichte erzählt. <break time="200ms"/> Ok dann lass uns weiter gehen. Oder willst du sie doch hören? <break time="150ms"/> Dann sage [[Pause einlegen]] oder [[Nein]]. ';
        }
        output += ' ';
        return output;
    },
    Ja: function(attributes) {
        var output = '';
        attributes.scene["GeschichteTeil1"] = 1;
        output += '<say-as interpret-as="interjection">halleluja</say-as>. Danke Lyria. Da fällt mir aber ein Stein von meinem Feenherz. <break time="200ms"/> Der Elfenbogen heißt Aurora <break time="200ms"/> und gehörte einer mächtigen Elfin. Diese Elfin wurde von der Schwarzen Magie in die irregeführt. <break time="100ms"/> Man sagt sie habe sich selbst von dieser Erde verbannt. <break time="200ms"/> Frag mich nicht ob es stimmt, aber ich weiß das sie seit über 150 Jahren verschwunden ist <break time="150ms"/> und ihr Bogen Aurora auch. <break time="250ms"/> Sie hatte ihn bevor sie verschwand in dieser Höhle versteckt. <break time="200ms"/> Darum hat man mich aufgesucht <break time="100ms"/> und gebeten jemanden zu finden, der Mutig genug ist, diese Aufgabe zu Meistern. <break time="300ms"/> Und da kommst du ins Spiel. <break time="350ms"/> Ich werde dich während unserer Reise zur Höhle noch mit dem nötigen Wissen ausstatten. <break time="200ms"/> Du wirst, sofern du fleißig übst, alle Zaubersprüche, die du für diese Aufgabe brauchst lernen. <break time="150ms"/> Deinen Beutel mit dem nötigstem hast du <break time="100ms"/> der Rest liegt bei dir. Mach dich jetzt auf den Weg und besorge mir den Elfenbogen Aurora. <audio src="https://nordicskills.de/audio/DerZauberwaldKendell/feen_fluegelschlag.mp3" /> Lyria ich setze mich auf deine Schulter. Denn das rum geflattere strengt mich an. Ich bleibe in deiner Nähe, aber eingreifen kann ich nicht. <break time="200ms"/> Dafür bin ich doch viel zu klein. <break time="100ms"/> sage [[Reise fortsetzen]]. ';
        return output;
    },
    Nein: function(attributes) {
        var output = '';
        if (attributes.visited.includes("Reise fortsetzen")) {
            output += 'Tja, das ist aber schade. <audio src="https://nordicskills.de/audio/DerZauberwaldKendell/verzaubern.mp3" /> <audio src="https://nordicskills.de/audio/DerZauberwaldKendell/lyria_als_kroete.mp3" /> Hast du etwa geglaubt ich lasse deine Entscheidung unbestraft? ';
            output += 'Du lässt mich dir Geschenke machen. Und dann erkennst du nicht einmal, wenn etwas wichtig ist. <break time="150ms"/> So kannst du Kendell nicht retten. <break time="250ms"/> Jetzt darfst du die nächsten Jahre als Kröte verbringen. Deine Stimme wird verstummen, so dass du nicht Zaubern kannst. <audio src="https://nordicskills.de/audio/DerZauberwaldKendell/kroete.mp3" /> <break time="150ms"/> Lebwohl Lyria und pass auf die Störche auf. <break time="150ms"/> <audio src="https://nordicskills.de/audio/DerZauberwaldKendell/negatives_spiel_ende.mp3" /> Um noch einmal zu spielen, Sage Neustarten. <break time="50ms"/> Um zu beenden sage beenden oder Stop. ';
        } else {
            output += 'Tja, das ist aber schade. <audio src="https://nordicskills.de/audio/DerZauberwaldKendell/verzaubern.mp3" /> <audio src="https://nordicskills.de/audio/DerZauberwaldKendell/lyria_als_kroete.mp3" /> Hast du etwa geglaubt ich lasse deine Entscheidung unbestraft? Du darfst jetzt die nächsten Jahre als Kröte verbringen. Deine Stimme wird verstummen, so dass du nichts von dem Geheimnis des Elfenbogens verraten kannst. <audio src="https://nordicskills.de/audio/DerZauberwaldKendell/kroete.mp3" /> <break time="150ms"/> Lebwohl Lyria und pass auf die Störche auf. <break time="150ms"/> <audio src="https://nordicskills.de/audio/DerZauberwaldKendell/negatives_spiel_ende.mp3" /> Um noch einmal zu spielen, Sage Neustarten. <break time="50ms"/> Um zu beenden sage beenden oder Stop. ';
        }
        return output;
    },
    Weiter_gehen: function(attributes) {
        var output = '';
        output += '<audio src="https://nordicskills.de/audio/DerZauberwaldKendell/feen_fluegelschlag.mp3" />Da vorne kommt eine Kreuzung. Tja Lyria <break time="200ms"/> und schon geht es los. <break time="200ms"/> Ab jetzt musst du entscheiden. Du wirst viele Entscheidungen treffen müssen. <break time="200ms"/> Ob sie alle richtig sind? Das musst du entscheiden. Dabei solltest du auf dein Herz hören und deinem Verstand vertrauen. <break time="300ms"/> Schau dir die Situation an, lausche in den Wald und entscheide dann. <break time="250ms"/> Also, ich? <break time="250ms"/> ';
        output += '<say-as interpret-as="interjection">nee</say-as><say-as interpret-as="interjection">nee</say-as><break time="150ms"/> ich sag nichts. <break time="150ms"/> Nachher bin ich schuld, wenn du den falschen Weg nimmst. Und nun sage [[Zur Kreuzung]]. ';
        return output;
    },
    Zur_Kreuzung: function(attributes) {
        var output = '';
        if (attributes.scene["Moorschlam"] == 1 && attributes.scene["Geroellsteine"] == 1) {
            output += '<say-as interpret-as="interjection">ähm</say-as> Das war wohl auch nichts. <break time="200ms"/> Tja. <break time="150ms"/> Jetzt bleibt nur noch ein <phoneme alphabet="ipa" ph="veːk">Weg</phoneme> übrig. Sage [[Nach links]]. ';
        } else {
            if (attributes.scene["Moorschlam"] == 1) {
                output += '<say-as interpret-as="interjection">nee</say-as> ';
                output += 'geradeaus war wohl nichts. <break time="150ms"/> Wohin willst du jetzt gehen? [[Nach links]] oder [[Nach rechts]]. ';
            } else {
                if (attributes.scene["Geroellsteine"] == 1) {
                    output += '<say-as interpret-as="interjection">na?</say-as> Rechts war wohl nichts. <break time="150ms"/> Wohin willst du jetzt gehen? [[Nach links]] oder [[Geradeaus]]. ';
                } else {
                    if (attributes.scene["Verwundet"] == 1) {
                        output += '<audio src="https://nordicskills.de/audio/DerZauberwaldKendell/teleport_magier.mp3" /><say-as interpret-as="interjection">heiliger strohsack</say-as> Das war aber knap. <break time="150ms"/> Nur gut das du einen Gesundheitstrank hattest. <break time="200ms"/> Die Kreuzung kennen wir doch noch. Erst nach Links und dann würde ich mir aber diesmal überlegen was du machst. Ist sicher gesünder. <break time="200ms"/> Wohin willst du jetzt gehen? Wieder [[Nach links]], [[Geradeaus]] oder [[Nach rechts]]. ';
                    } else {
                        output += '<audio src="https://nordicskills.de/audio/DerZauberwaldKendell/waldweg_steine_laub_voegel.mp3" /> Ok. Schau dir die Kreuzung genau an und dann entscheide dich. <break time="200ms"/> <say-as interpret-as="interjection">na?</say-as> wohin willst du? [[Nach links]], [[Geradeaus]] oder [[Nach rechts]]. ';
                    }
                }
            }
        }
        return output;
    },
    Nach_links: function(attributes) {
        var output = '';
        output += '<audio src="https://nordicskills.de/audio/DerZauberwaldKendell/waldweg_steine_laub_voegel.mp3" /> Lyria, der Weg gefällt mir. Es ist so friedlich hier. Ich werde einmal voraus fliegen und schauen wie der Weg weiter vorn aussieht. <audio src="https://nordicskills.de/audio/DerZauberwaldKendell/feen_fluegelschlag.mp3" /> <audio src="https://nordicskills.de/audio/DerZauberwaldKendell/waldweg_steine_laub_voegel.mp3" /> <audio src="https://nordicskills.de/audio/DerZauberwaldKendell/feen_fluegelschlag.mp3" /> <say-as interpret-as="interjection">oh oh</say-as>, da bin ich wieder. Ich glaube da vorn solltest du aufpassen. Ich bin mir nicht sicher, aber irgendwas stimmt da nicht. Hörst du das? <audio src="https://nordicskills.de/audio/DerZauberwaldKendell/wald_wind_ohne_voegel.mp3" /> Genau. Da ist nichts. Nur der Wind, aber wo sind die Vögel geblieben? <break time="100ms"/> Das finde ich komisch. <break time="150ms"/> Was willst du machen [[Einfach weiter gehen]], [[Vorbei schleichen]] oder [[Los rennen]]? ';
        return output;
    },
    Geradeaus: function(attributes) {
        var output = '';
        output += '<audio src="https://nordicskills.de/audio/DerZauberwaldKendell/waldweg_steine_laub_voegel.mp3" /> Lyria der Weg sieht aber feucht aus. Bin ich froh das ich fliegen kann. <audio src="https://nordicskills.de/audio/DerZauberwaldKendell/feen_fluegelschlag_schnell.mp3" /> So klein wie ich bin, würde ich in den Pfützen ertrinken. Hier können wir nur gerade aus gehen. Besser gesagt, du gehst und ich fliege. <break time="150ms"/> Sage [[Weiter geradeaus]]. ';
        return output;
    },
    Nach_rechts: function(attributes) {
        var output = '';
        output += '<audio src="https://nordicskills.de/audio/DerZauberwaldKendell/waldweg_steine_laub_voegel.mp3" /> Der Wald links und rechts von uns ist so dicht gewachsen, dass wir hier nur dem Weg folgen können. Aber Lyria sei vorsichtig, denn der Weg geht ziemlich steil nach unten. Ich möchte nicht, dass dir was passiert. Sage [[Dem Weg folgen]]. ';
        return output;
    },
    Weiter_geradeaus: function(attributes) {
        var output = '';
        attributes.scene["Moorschlam"] = 1;
        output += '<audio src="https://nordicskills.de/audio/DerZauberwaldKendell/durchswasser.mp3" /> So langsam werden die Pfützen aber immer größer und tiefer. Wenn du nicht aufpasst, bekommst du nasse Füße. <audio src="https://nordicskills.de/audio/DerZauberwaldKendell/durchswasser.mp3" /> Es wird nicht besser. <break time="150ms"/> <audio src="https://nordicskills.de/audio/DerZauberwaldKendell/sumpf_moor.mp3" /> <say-as interpret-as="interjection">halt</say-as> bleib stehen Lyria. <break time="150ms"/> Wenn du weiter gehst versackst du und bleibst im Sumpf stecken. Ich kann dich dann da nicht rausholen. <break time="200ms"/> Den Seilzauber kennst du noch nicht, den lernst du erst später. <break time="150ms"/> Tja, das bedeutet wir müssen umdrehen und zurück. Wenn du nicht versacken willst, sage [[Zur Kreuzung]]. ';
        return output;
    },
    Dem_Weg_folgen: function(attributes) {
        var output = '';
        attributes.scene["Geroellsteine"] = 1;
        output += '<say-as interpret-as="interjection">vorsicht</say-as>. Lyria pass auf <break time="150ms"/> da ist ein Abhang. <break time="350ms"/><say-as interpret-as="interjection">manometer</say-as><break time="250ms"/>Da hast du aber Glück gehabt. Wenn du daruntergefallen wärst? <break time="200ms"/> naja ich kann fliegen du aber nicht. <break time="200ms"/> Wobei <break time="200ms"/> runter fliegen kannst du auch. <say-as interpret-as="interjection">hihi</say-as><break time="150ms"/> tschuldigung. <break time="300ms"/> Auf jedem Fall geht es hier nicht weiter. <break time="150ms"/> Wir müßen zurück zur Kreuzung und dort dann einen anderen <phoneme alphabet="ipa" ph="veːk">Weg</phoneme> nehmen. Sage [[Zur Kreuzung]]. ';
        return output;
    },
    Einfach_weiter_gehen: function(attributes) {
        var output = '';
        output += '<audio src="https://nordicskills.de/audio/DerZauberwaldKendell/wald_wind_ohne_voegel.mp3" /> Lyria? Ich glaube das war eine blöde Idee. <audio src="https://nordicskills.de/audio/DerZauberwaldKendell/troll_laufen_und_bruellen.mp3" /> <say-as interpret-as="interjection">ach du Liebe Zeit</say-as> der kommt uns hinterher. Schnell, Du musst hier weg. Sage [[Weg laufen]]. ';
        return output;
    },
    Vorbei_schleichen: function(attributes) {
        var output = '';
        output += '<audio src="https://nordicskills.de/audio/DerZauberwaldKendell/im_wald_schleichen.mp3" /> Lyria schaumal, da vorn sitzt ein Troll an seinem Lagerfeuer. <audio src="https://nordicskills.de/audio/DerZauberwaldKendell/troll_grummeln.mp3" /> Ich glaub mit dem ist nicht zu spaßen. <break time="150ms"/> Wir sollten ihn nicht stören. <break time="100ms"/> Das Beste ist, wenn wir einen Umweg nehmen oder was meinst du? [[Den Umweg nehmen]], [[Angreifen]] oder [[Laut schreien]]. ';
        return output;
    },
    Los_rennen: function(attributes) {
        var output = '';
        output += '<audio src="https://nordicskills.de/audio/DerZauberwaldKendell/waldlaufen.mp3" /> Schneller Lyria. Der Troll kommt näher <audio src="https://nordicskills.de/audio/DerZauberwaldKendell/troll_laufen_und_bruellen.mp3" /> Noch schneller <audio src="https://nordicskills.de/audio/DerZauberwaldKendell/waldlaufen.mp3" /> gleich hast du es geschafft. <audio src="https://nordicskills.de/audio/DerZauberwaldKendell/troll_laeuft.mp3" /> <say-as interpret-as="interjection">puh</say-as><break time="10ms"/> das war knapp. <break time="200ms"/> Schau mal da vorn. Da scheint die Sonne. Das sieht aus wie eine Lichtung. <break time="150ms"/> Wenn du da hin willst, sage [[Zur Lichtung]]. ';
        return output;
    },
    Den_Umweg_nehmen: function(attributes) {
        var output = '';
        output += '<audio src="https://nordicskills.de/audio/DerZauberwaldKendell/waldweg_steine_laub_voegel.mp3" /> ';
        output += 'Lyria? <break time="100ms"/> Du bist eine echt schlaue Elfin. Statt mit dem Troll zu kämpfen, nimmst du lieber einen Umweg. <break time="200ms"/> Übrigens hattest du gesehen wie groß der war? <break time="150ms"/> Nee nee, deine Entscheidung war genau richtig. Schau mal, <break time="150ms"/> da vorn ist eine Lichtung, das sieht nach einem schönen Platz zum rasten aus. Aber bevor du weiter gehst, da vorn liegt schönes trockenes Feuerholz. Nimm das doch mit. <break time="150ms"/> Sage entweder, [[Feuerholz sammeln]] oder [[Zur Lichtung]]. ';
        output += ' ';
        return output;
    },
    Angreifen: function(attributes) {
        var output = '';
        output += '<audio src="https://nordicskills.de/audio/DerZauberwaldKendell/lyria_laufen_und_schrein.mp3" /><say-as interpret-as="interjection">was zur Hölle</say-as> sollte das denn? Hast du gesehen, der Troll ist mindestens 10 mal so groß und 20 mal so schwer wie du. <audio src="https://nordicskills.de/audio/DerZauberwaldKendell/troll_laufen_und_bruellen.mp3" /> <audio src="https://nordicskills.de/audio/DerZauberwaldKendell/kopfschlag.mp3" /><break time="250ms"/> Tja das wars. Du bist tot. ';
        if (attributes.scene["Item_Lebenstrank"] > 0) {
            output += 'Wenn du willst kannst du einen Lebenstrank nehmen. Dann sage [[Lebenstrank]]. ';
        } else {
            output += 'da du keinen Lebenstrank mehr hast, hast du verloren. <audio src="https://nordicskills.de/audio/DerZauberwaldKendell/verlorenundtot.mp3" /> Um noch einmal zu spielen, Sage Neustarten. <break time="50ms"/> Um zu beenden sage beenden oder Stop. ';
        }
        return output;
    },
    Laut_schreien: function(attributes) {
        var output = '';
        output += '<audio src="https://nordicskills.de/audio/DerZauberwaldKendell/lyria_laufen_und_schrein.mp3" /> <say-as interpret-as="interjection">ist nicht dein ernst</say-as>. Warum schreist du denn hier rum? <break time="150ms"/> meinst du der Troll läuft vor dir davon? <audio src="https://nordicskills.de/audio/DerZauberwaldKendell/troll_laufen_und_bruellen.mp3" /> <say-as interpret-as="interjection">vorsicht</say-as> <audio src="https://nordicskills.de/audio/DerZauberwaldKendell/kopfschlag.mp3" /> <audio src="https://nordicskills.de/audio/DerZauberwaldKendell/troll_laeuft.mp3" /> Lyria? <break time="150ms"/> Lyria, lebst du noch? <break time="250ms"/> <say-as interpret-as="interjection">oje</say-as>. Der hat gesessen. <break time="150ms"/> Der Troll hat dir mit seiner Keule eine Kopfnuss verpasst. <break time="100ms"/> Hier, entweder nimmst du einen Gesundheitstrank <break time="100ms"/> oder du gibst auf. <break time="150ms"/> Sage [[Gesundheitstrank]] oder, um noch einmal zu spielen sage Neustarten. <break time="350ms"/> Du kannst auch beenden oder Stop sagen. ';
        return output;
    },
    Gesundheitstrank: function(attributes) {
        var output = '';
        if (attributes.visited.includes("Gesundheitstrank")) {} else {
            attributes.scene["Item_Gesundheitstrank"] -= 1;
        }
        output += '<audio src="https://nordicskills.de/audio/DerZauberwaldKendell/zaubertrank_trinken.mp3" /> ';
        output += 'Hallo Lyria, durch den Gesundheitstrank, den du gerade genommen hast, kannst du nach dem du dich erholt hast weiter gehen. Aber vergesse nicht, du hast nur noch ' + attributes.scene["Item_Gesundheitstrank"] + ' im Beutel. <break time="250ms"/> Wenn du soweit bist, sage [[Zur Lichtung]]. ';
        return output;
    },
    Weg_laufen: function(attributes) {
        var output = '';
        output += '<audio src="https://nordicskills.de/audio/DerZauberwaldKendell/baldur_und_fee_laufen.mp3" /> <say-as interpret-as="interjection">puh</say-as><break time="10ms"/> das war knapp. Du bist lebensmüde oder einfach nur leichtsinnig. Mach sowas nie wieder. <break time="250ms"/> Ich glaube der wollte dich zum Mittag und mich als Nachspeise. <break time="300ms"/> Nun gut lass uns weiter gehen. <audio src="https://nordicskills.de/audio/DerZauberwaldKendell/waldweg_steine_laub_voegel.mp3" /> Da vorn ist eine Weggabelung, welchen <phoneme alphabet="ipa" ph="veːk">Weg</phoneme> nimmst du? Sage, [[Ich nehme den Linken Weg]] oder [[Ich nehme den Rechten Weg]]. ';
        return output;
    },
    Ich_nehme_den_Linken_Weg: function(attributes) {
        var output = '';
        output += '<audio src="https://nordicskills.de/audio/DerZauberwaldKendell/waldweg_steine_laub_voegel.mp3" /> Schau da vorn ist eine Lichtung. Das sieht nach einem schönen Platz zum rasten aus. <break time="250ms"/> Aber bevor du weiter gehst, schau mal da liegt trockenes Brennholz. Nimm das doch mit, wir brauchen sowieso etwas für unser Lagerfeuer heute Nacht. <break time="150ms"/> Sage entweder, [[Feuerholz sammeln]] oder [[Zur Lichtung]]. ';
        return output;
    },
    Ich_nehme_den_Rechten_Weg: function(attributes) {
        var output = '';
        output += '<audio src="https://nordicskills.de/audio/DerZauberwaldKendell/waldweg_steine_laub_voegel.mp3" /> Schau da vorn ist eine Lichtung. Das sieht nach einem schönen Platz zum rasten aus. <break time="250ms"/> Aber bevor du weiter gehst, schau mal da liegt trockenes Brennholz. Nimm das doch mit, wir brauchen sowieso etwas für unser Lagerfeuer heute Nacht. <break time="150ms"/> Sage entweder, [[Feuerholz sammeln]] oder [[Zur Lichtung]]. ';
        return output;
    },
    Zur_Lichtung: function(attributes) {
        var output = '';
        if (attributes.visited.includes("Nach dem Geräusch schauen")) {
            output += 'Brennholz hast du gesammelt, den Feuerstein hast du im Beutel. Dann mach uns doch bitte ein Lagerfeuer an. Mir ist kalt und außerdem hält das Feuer die bösen Tiere davon ab uns zu fressen. <break time="150ms"/> Und was machst du jetzt? [[Feuermachen]] oder [[Schlafen legen]]. ';
        } else {
            if (!attributes.scene["Brennholz"] && !attributes.scene["Feuerholz"]) {
                output += 'Lyria. Die Sonne wird bald untergehen. Und wir sollten nicht im dunkeln im Wald herumirren. <break time="200ms"/> Schau mal <break time="300ms"/> die Lichtung hier sieht doch gut aus. <break time="150ms"/> Hier kannst du unser Nachtlager aufbauen. Ich finde es wirkt wie ein ruhiger Ort zum schlafen. <break time="200ms"/> Wenn du Brennholz sammelst komme ich mit. Was möchtest du jetzt machen? [[Brennholz sammeln]] oder [[Schlafen legen]]. ';
            } else {
                if (attributes.scene["Brennholz"] == 1) {
                    output += 'Das Brennholz hast du gesammelt und den Feuerstein hast du im Beutel. <break time="150ms"/> Dann mach uns doch bitte ein Lagerfeuer an. Mir ist kalt und außerdem hält das Feuer die bösen Tiere davon ab uns zu fressen. <break time="250ms"/> Was machst du als nächstes? [[Feuermachen]] oder [[Schlafen legen]]. ';
                } else {
                    if (attributes.scene["Feuerholz"] == 1) {
                        output += 'Lyria. Die Sonne wird bald untergehen und wir sollten nicht im dunkel im Wald herumirren. Schau mal, die Lichtung hier, sieht doch gut aus. <break time="100ms"/> Hier kannst du unser Nachtlager aufbauen. Ich finde es wirkt wie ein ruhiger Ort zum schlafen. <break time="150ms"/> Brennholz hast du gesammelt, den Feuerstein hast du im Beutel. <break time="150ms"/> Dann mach uns doch bitte ein Lagerfeuer an. Mir ist kalt und außerdem hält das Feuer die bösen Tiere davon ab uns zu fressen. <break time="150ms"/> Und was machst du jetzt? [[Feuermachen]] oder [[Schlafen legen]]. ';
                    }
                }
            }
        }
        return output;
    },
    Nach_dem_Geräusch_schauen: function(attributes) {
        var output = '';
        if (attributes.visited.includes("Nach dem Geräusch schauen")) {} else {
            attributes.scene["Item_Drachenfeder"] = 1;
        }
        output += '<audio src="https://nordicskills.de/audio/DerZauberwaldKendell/monster_klein_wald.mp3" /> Schau mal da vorne bei der großen Eiche. <break time="350ms"/> Geh mal dichter herran. <audio src="https://nordicskills.de/audio/DerZauberwaldKendell/wesen_an_der_eiche.mp3" /> Was ist das? <audio src="https://nordicskills.de/audio/DerZauberwaldKendell/babydrache_fliegt_weg.mp3" /> <say-as interpret-as="interjection">wow</say-as>. Hast du das gesehen? <audio src="https://nordicskills.de/audio/DerZauberwaldKendell/lyria_sagt_ja_das_habe_ich.mp3" /> Schau mal da liegt eine Feder. Eine Drachenfeder. <break time="150ms"/> Das ist was ganz Besonderes. <break time="150ms"/> Nimm sie mit <audio src="https://nordicskills.de/audio/DerZauberwaldKendell/zauber_geschenkt.mp3" /> wenn du soweit bist, sage [[Zur Lichtung]]. ';
        return output;
    },
    Brennholz_sammeln: function(attributes) {
        var output = '';
        attributes.scene["Brennholz"] = 1;
        output += '<audio src="https://nordicskills.de/audio/DerZauberwaldKendell/holzsammeln_wald.mp3" /> Ich glaube das reicht. <break time="150ms"/> Wir wollen ja nur ein Lagerfeuer für eine Nacht sammeln und nicht für eine Woche. <break time="200ms"/> Aber bevor wir zurück zur Lichtung gehen <break time="150ms"/> hörst du das? <audio src="https://nordicskills.de/audio/DerZauberwaldKendell/monster_klein_wald.mp3" /> wo kommt das her? <break time="250ms"/> sage [[Nach dem Geräusch schauen]] oder [[Zur Lichtung]]. ';
        return output;
    },
    Schlafen_legen: function(attributes) {
        var output = '';
        if (attributes.visited.includes("Feuermachen") && attributes.scene["Ignis"] == 1) {
            output += '<audio src="https://nordicskills.de/audio/DerZauberwaldKendell/lagerfeuer_nachts.mp3" /> Lyria, das Feuer ist an und du hast deinen dritten Zauber gelernt. <break time="200ms"/> Dann lass uns jetzt schlafen. <break time="200ms"/> Du weißt, Morgen ist ein neuer Tag, mit neuen Aufgaben und neuen Abenteuern. <audio src="https://nordicskills.de/audio/DerZauberwaldKendell/baldur_schlaeft.mp3" /> Lyria? <break time="150ms"/> Lyria du musst aufstehen. Wir müssen weiter. <break time="250ms"/> Wenn du soweit bist, sage [[Aufstehen]]. ';
        } else {
            if (attributes.visited.includes("Feuermachen") && !attributes.scene["Ignis"]) {
                output += '<audio src="https://nordicskills.de/audio/DerZauberwaldKendell/lagerfeuer_nachts.mp3" /> Lyria das Feuer brennt. Dann setz dich hin, nimm dir deine Schriftrolle. Du musst noch lernen, dafür sage [[Schriftrolle drei lesen]]. ';
            } else {
                output += 'Du willst also lieber frieren <break time="200ms"/> und lernen willst du auch nicht. <break time="1s"/> Na dann gute Nacht. <break time="450ms"/> <audio src="https://nordicskills.de/audio/DerZauberwaldKendell/vom_baeren_gefressen.mp3" /> In der Nacht wurdest du von einem Bären angegriffen und getötet. <break time="150ms"/> Ich hatte dich ja gewarnt, aber du wolltest ja kein Feuer machen. <break time="200ms"/> Tja, das wars. Du bist Tot. <break time="150ms"/> ';
                if (attributes.scene["Item_Lebenstrank"] > 0) {
                    output += 'Wenn du willst kannst du einen Lebenstrank nehmen. Dann sage [[Lebenstrank]]. ';
                } else {
                    output += 'Da du keinen Lebenstrank mehr hast, hast du verloren. <audio src="https://nordicskills.de/audio/DerZauberwaldKendell/verlorenundtot.mp3" /> Um noch einmal zu spielen, Sage Neustarten. <break time="50ms"/> Um zu beenden sage beenden oder Stop. ';
                }
            }
        }
        output += ' ';
        return output;
    },
    Lebenstrank: function(attributes) {
        var output = '';
        attributes.scene["Item_Lebenstrank"] -= 1;
        output += '<audio src="https://nordicskills.de/audio/DerZauberwaldKendell/zaubertrank_trinken.mp3" /> ';
        output += 'Hallo Lyria, durch den Lebenstrank, den du gerade genommen hast, kannst du nach dem du dich erholt hast, weiter gehen. Aber vergesse nicht, du hast nur noch ' + attributes.scene["Item_Lebenstrank"] + ' im Beutel. <break time="250ms"/> Wenn du soweit bist, sage [[Zur Lichtung]]. ';
        output += ' ';
        return output;
    },
    Feuermachen: function(attributes) {
        var output = '';
        output += '<audio src="https://nordicskills.de/audio/DerZauberwaldKendell/lyria_feuermachen.mp3" /> <say-as interpret-as="interjection">schau an</say-as>, das ist ein schönes Feuer. Du bist wirklich talentiert. <break time="150ms"/> Nun gut. <break time="200ms"/> Das Feuer brennt. Der Schlafplatz ist gemacht. Jetzt solltest du deine nächste Schriftrolle nehmen und lesen. <break time="250ms"/> Denn als Elfin musst du lernen, lernen und lernen. <break time="250ms"/> Wenn du soweit bist sage [[Schriftrolle drei lesen]] oder [[Schlafen legen]]. ';
        return output;
    },
    Feuerholz_sammeln: function(attributes) {
        var output = '';
        attributes.scene["Feuerholz"] = 1;
        output += '<audio src="https://nordicskills.de/audio/DerZauberwaldKendell/holzsammeln_wald.mp3" /> Ich glaube das reicht jetzt. Wir wollen ja nur ein Lagerfeuer für eine Nacht sammeln und nicht für eine Woche. <break time="300ms"/> Nimm das Holz zusammen mit etwas Reisig <break time="100ms"/> und dann lass uns zu Lichtung gehen. <break time="250ms"/> Dafür sage [[Zur Lichtung]]. ';
        return output;
    },
    Schriftrolle_drei_lesen: function(attributes) {
        var output = '';
        attributes.scene["Ignis"] = 1;
        if (attributes.visited.includes("schlafen legen")) {
            output += 'Na Lyria, hast du dir es überlegt? <break time="150ms"/> Das ist auch besser so, denn nur wer fleißig übt kann auch ein mächtige Elfin werden. <break time="150ms"/> Dann fang an zu lesen und sei vorsichtig und pass genau auf beim Lernen. <audio src="https://nordicskills.de/audio/DerZauberwaldKendell/schriftrolle.mp3" /> <audio src="https://nordicskills.de/audio/DerZauberwaldKendell/zauber_spruch.mp3" />Gut gemacht Lyria, du hast deinen dritten Zauber gelernt. Das ist ein Feuerzauber er heißt <break time="100ms"/> Ignis. Mit ihm kannst du Gegenstände anzünden. Später wenn du die zweite Stufe erlernt hast, kannst du auch einen Feuerball auf Gegner schleudern. Aber soweit bist du noch nicht. Und ja, du wirst noch Möglichkeiten haben deinen neuen Zauber auszuprobieren. ';
        } else {
            output += 'Lyria, sei vorsichtig und pass genau auf beim Lernen. <audio src="https://nordicskills.de/audio/DerZauberwaldKendell/schriftrolle.mp3" /> <audio src="https://nordicskills.de/audio/DerZauberwaldKendell/zauber_spruch.mp3" />Gut gemacht Lyria, du hast deinen dritten Zauber gelernt. Das ist ein Feuerzauber er heißt <break time="100ms"/> Ignis. Mit ihm kannst du Gegenstände anzünden. Später wenn du die zweite Stufe erlernt hast, kannst du auch einen Feuerball auf Gegner schleudern. Aber soweit bist du noch nicht. Und ja, du wirst noch Möglichkeiten haben deinen neuen Zauber auszuprobieren. ';
        }
        output += '<break time="150ms"/> Gut, dann lass uns schlafen. Morgenfrüh haben wir einen anstrengenden Tag. <break time="250ms"/> Wenn du fertig bist, sage [[Schlafen legen]]. ';
        return output;
    },
    Aufstehen: function(attributes) {
        var output = '';
        if (attributes.visited.includes("Gesundheitstrank nehmen")) {
            attributes.scene["Verletzt_Kobobande"] = 1;
            output += '<audio src="https://nordicskills.de/audio/DerZauberwaldKendell/lyria_verletzt.mp3" /> Tja. So ein Goblin Fusstritt, kann schon ziemlich schmerzhaft sein. <break time="250ms"/> Da du einen Gesundheitstrank genommen hast und es dir langsam besser geht, können wir ja weiter. Sage [[Wir können weiter]]. ';
        } else {
            output += '<audio src="https://nordicskills.de/audio/DerZauberwaldKendell/lyria_morgens.mp3" /> ';
            if (attributes.visited.includes("Gute nacht")) {
                output += 'Na Lyria, sieht aus als hast du gut geschlafen. <break time="100ms"/> <say-as interpret-as="interjection">manometer</say-as> war das ein Schietwetter. <break time="100ms"/> Aber jetzt scheint ja gleich die Sonne. <break time="150ms"/> Lyria steh auf und schau dir mal das Tal an. <break time="150ms"/> Schau auf den Horizont <break time="100ms"/> und dann warte bis die Sonne aufgeht. Dieser Sonnenaufgang ist jedes Mal fantastisch. <break time="500ms"/> <audio src="https://nordicskills.de/audio/DerZauberwaldKendell/sonnenaufgang.mp3" /> Oh wie schön das ist. Aber immer nur so Kurz. <say-as interpret-as="interjection">seufz</say-as><break time="1s"/> Nagut. Wenn du alles zusammen gepackt hast <break time="100ms"/> und fertig mit dem Frühstück bist, dann lass uns los. <break time="250ms"/> Wenn du soweit bist, sage [[Wir können weiter]]. ';
            } else {
                output += 'Guten Morgen Lyria, du solltest dir dort am Bach deine Wasserflasche füllen, vergesse die Katzenwäsche nicht und auf dem Weg dorthin solltest du noch ein paar Beeren sammeln. Wenn du Hunger hast, solltest du auch noch etwas Essen. Wer weiß wann wir die nächste Pause machen können. Was willst du machen [[Wasser auffüllen]] oder [[Auf den Weg machen]]. ';
            }
        }
        output += ' ';
        return output;
    },
    Auf_den_Weg_machen: function(attributes) {
        var output = '';
        if (attributes.visited.includes("Wasser auffüllen")) {
            output += '<audio src="https://nordicskills.de/audio/DerZauberwaldKendell/waldweg_steine_laub_voegel.mp3" /> Ich habe dir doch von der Elfin und ihrem Bogen erzählt. <break time="200ms"/> Das weist du doch noch oder? <break time="150ms"/> Wenn ja, dann nenne mir den Namen. War es [[Fauna]], [[Aurora]] oder [[Aura]]? ';
        } else {
            output += '<audio src="https://nordicskills.de/audio/DerZauberwaldKendell/waldweg_steine_laub_voegel.mp3" /> <say-as interpret-as="interjection">puh</say-as><break time="10ms"/> Kann es sein das du weder Wasser geholt noch dich gewaschen hast? <break time="150ms"/> Es müffelt hier. <break time="150ms"/> Lyria, geh das Wasser holen und wasch dich. <break time="100ms"/> Sage, [[Wasser auffüllen]]. ';
        }
        return output;
    },
    Wasser_auffüllen: function(attributes) {
        var output = '';
        attributes.scene["Item_Beeren"] = 1;
        output += '<audio src="https://nordicskills.de/audio/DerZauberwaldKendell/feen_fluegelschlag.mp3" /> Warte, ich komm mit. <break time="150ms"/> Ich möchte mir am Bach eine Feenwäsche gönnen. <break time="200ms"/> Lyria <break time="150ms"/> etwas Wasser im Gesicht, würde dir auch nicht schaden. Du bist voller Ruß vom Feuermachen. <audio src="https://nordicskills.de/audio/DerZauberwaldKendell/bach_im_wald.mp3" /> Schau da ist der Bach. Füll deine Flasche mit Wasser. <audio src="https://nordicskills.de/audio/DerZauberwaldKendell/wasser_in_flasche_fuellen.mp3" /> Die Flasche ist voll. Jetzt noch die Beeren sammeln und dann können wir weiter. <audio src="https://nordicskills.de/audio/DerZauberwaldKendell/beeren_pfluecken.mp3" /><break time="250ms"/> Wenn du los willst, dann sage [[Auf den Weg machen]]. ';
        return output;
    },
    Fauna: function(attributes) {
        var output = '';
        output += '<say-as interpret-as="interjection">seufz</say-as>. Jetzt bin ich aber fast endtäuscht. <break time="200ms"/> Hast du nicht zugehört? <break time="150ms"/> oder hast du dich nur versprochen? <break time="150ms"/> Ich gebe dir noch eine Chance. Sage [[Nochmal versuchen]]. ';
        return output;
    },
    Aurora: function(attributes) {
        var output = '';
        if (attributes.visited.includes("Fauna") || attributes.visited.includes("Fauna Auraru")) {
            output += 'Gut. Du weist es ja doch. Aber ein Preis, hast du dafür nicht gewonnen. <break time="150ms"/> Ich hoffe nicht, dass du auch so vergesslich beim Lernen der Zauber bist. <break time="150ms"/> Da vorne geht der Weg Bergab zu einem See mit einem Wasserfall. Sage [[Zum See]]. ';
        } else {
            if (attributes.scene["GetränkBeimRaten"] == 1) {
                output += 'Gutgemacht Lyria. Dann lass uns weiter gehen. Da vorne geht der Weg bergab zu einem See mit einem Wasserfall. Sage [[Zum See]]. ';
            } else {
                attributes.scene["Item_Magietrank"] += 1;
                attributes.scene["GetränkBeimRaten"] = 1;
                output += '<say-as interpret-as="interjection">prima</say-as> du passt ja wirklich auf. Dafür schenke ich dir noch einen Magietrank. <audio src="https://nordicskills.de/audio/DerZauberwaldKendell/zauber_geschenkt.mp3" /> Gutgemacht Lyria. Dann lass uns weiter gehen. Da vorne geht der Weg Bergab zu einem See mit einem Wasserfall. Sage [[Zum See]]. ';
            }
        }
        output += ' ';
        return output;
    },
    Aura: function(attributes) {
        var output = '';
        attributes.scene["Item_Magietrank"] -= 1;
        output += '<say-as interpret-as="interjection">ist nicht dein Ernst</say-as>. Du hast mir ja gar nicht zugehört. <say-as interpret-as="interjection">oh mann</say-as>. Dafür nimm ich dir jetzt einen Magietrank weg. <audio src="https://nordicskills.de/audio/DerZauberwaldKendell/zauber_weg.mp3" /> Du musst lernen, dass Fehler bestraft werden. <break time="150ms"/> So. Und jetzt streng dich etwas mehr an und sage [[Nochmal versuchen]]. ';
        return output;
    },
    Nochmal_versuchen: function(attributes) {
        var output = '';
        if (attributes.visited.includes("Aura")) {
            attributes.scene["FehlerBeimRaten"] = 1;
            output += 'Du darfst es noch einmal versuchen <break time="100ms"/> aber endtäusche mich nicht. Nenne mir den Namen. War es [[Fauna]] oder [[Aurora]] ? ';
        } else {
            attributes.scene["FehlerBeimRaten"] = 1;
            output += 'Du darfst es noch einmal versuchen aber <break time="100ms"/> endtäusche mich nicht. Nenne mir den Namen. War es [[Aurora]] oder [[Aura]]? ';
        }
        return output;
    },
    Zum_See: function(attributes) {
        var output = '';
        if (attributes.visited.includes("Zum See")) {
            output += 'Da bist du ja wieder. So ein Lebenstrank ist doch wirklich nützlich. <break time="200ms"/> Solange man noch welche hat. <break time="200ms"/> Naja weiter gehts. <break time="200ms"/> Gut gut. Wir müssen auf die andere Seite des Sees. <break time="150ms"/> Das sagte ich dir ja schon. Denn dort ist ein Anstieg zu einem Kleinen Berg und dahinter ist das Tal. <break time="150ms"/> Wie möchtest du gehen [[Links herum]] oder [[Rechts herum]]? ';
        } else {
            attributes.scene["GeschichteTeil2"] = 1;
            output += '<audio src="https://nordicskills.de/audio/DerZauberwaldKendell/zum_see_gehen.mp3" /> <say-as interpret-as="interjection">wow</say-as><break time="10ms"/> wie schön der See ist. <break time="250ms"/> Jetzt haben wir etwas Zeit. Da erzähle ich dir noch etwas über die Elfin und ihrem Bogen. <break time="250ms"/> Hörzu. <break time="150ms"/> Die Elfin Valaria, so war ihr Name, war eine wirklich mächtige Elfin.<break time="350ms"/> Ihre Macht bezog sie hauptsächlich aus dem Elfenbogen Aurora. <break time="150ms"/> Man sagt das ein Drache namens Tarrador <break time="100ms"/> einen Ast aus Eibenholz mit seinem Drachenfeuer verbrannt hat <break time="100ms"/> und nachdem der Ast zu Boden ging <break time="100ms"/> zerfiel er zu Asche und übrig blieb das Bogenholz. <break time="150ms"/> Spannend ist die Geschichte wie Valeria zu Aurora kam. Aber das erzähle ich dir später. <audio src="https://nordicskills.de/audio/DerZauberwaldKendell/am_see_angekommen.mp3" /> Schaumal, wir sind am See angekommen. <break time="150ms"/> Lyria wir müssen auf die andere Seite des Sees. Dort ist ein Anstieg zu einem Kleinen Berg und dahinter ist ein Tal. <break time="150ms"/> Dort müssen wir hin. Wie möchtest du gehen [[Links herum]] oder [[Rechts herum]]? ';
        }
        return output;
    },
    Links_herum: function(attributes) {
        var output = '';
        output += '<audio src="https://nordicskills.de/audio/DerZauberwaldKendell/zum_see_gehen.mp3" /> Schau mal, hinter dem Wasserfall ist eine Höhle. <break time="100ms"/> oder sowas in der Art. <break time="250ms"/> Jedenfalls schimmert da etwas hinter dem Wasser. <break time="100ms"/> Wollen wir uns das einmal ansehen? Dann sage [[Zum Wasserfall]]. Wenn du aber wasserscheu bist, sage [[Weiter nach links]]. ';
        return output;
    },
    Rechts_herum: function(attributes) {
        var output = '';
        output += '<audio src="https://nordicskills.de/audio/DerZauberwaldKendell/schotterweg.mp3" /><prosody volume="-10dB">Pscht. Bleib stehen und sei ganz leise. </prosody><break time="300ms"/> Da vorne sind zwei Wichtel und ich glaube die streiten. <audio src="https://nordicskills.de/audio/DerZauberwaldKendell/wichtel_streit.mp3" /> Lyria. <break time="150ms"/> Bleib du lieber hier. <break time="250ms"/> Ich fliege hin und schau mal was da los ist. Vor dir haben die Angst und laufen weg. <audio src="https://nordicskills.de/audio/DerZauberwaldKendell/wichtel_streit_ende.mp3" /> Da bin ich wieder. Diese Lütten Zwerge. <say-as interpret-as="interjection">hihi</say-as>. Sie streiten sich wer mehr Angst hat. <break time="150ms"/> Dem einem Wichtel ist am Seeufer sein Gehstock ins Wasser hinein gefallen <break time="100ms"/> und keiner der beiden traut sich ihn herauszuholen. <break time="100ms"/> Ich habe Ihnen versprochen das du es machst. <break time="250ms"/> Und hilfst du den beiden? <break time="250ms"/> Dann sage [[Zum Seeufer]]. Wenn nicht sage [[Weiter nach rechts]]. ';
        return output;
    },
    Weiter_nach_links: function(attributes) {
        var output = '';
        if (attributes.visited.includes("Zum Wasserfall")) {
            output += '<audio src="https://nordicskills.de/audio/DerZauberwaldKendell/nach_wasserfall.mp3" /> Das Amulett steht dir echt gut. Ich bin mir sicher, dass es dir irgendwann noch einmal behilflich sein wird. Verliere es bloß nicht. <break time="100ms"/> ';
            attributes.scene["Item_Drachenamulett"] = 1;
        } else {
            output += '<audio src="https://nordicskills.de/audio/DerZauberwaldKendell/zum_see_gehen.mp3" /> ';
            output += 'Hattest du keine Lust auf eine kleine Dusche? <break time="100ms"/> Geschadet hätte es dir nicht. Naja, so sind die Fliegen wenigstens nicht bei mir. <say-as interpret-as="interjection">hihi</say-as><break time="100ms"/> ';
        }
        output += 'Und wollen wir weiter? dann sage [[Den Berg hinauf]]. ';
        return output;
    },
    Zum_Wasserfall: function(attributes) {
        var output = '';
        attributes.scene["Item_Drachenamulett"] = 1;
        output += 'Nimm mich aber erst unter deine Weste <break time="100ms"/> sonst werde ich ganz naß. <audio src="https://nordicskills.de/audio/DerZauberwaldKendell/vor_wasserfall.mp3" /> <say-as interpret-as="interjection">mist</say-as> jetzt sind meine Flügel doch nass geworden. Warte kurz <audio src="https://nordicskills.de/audio/DerZauberwaldKendell/feen_fluegelschlag.mp3" /> <say-as interpret-as="interjection">alles klar</say-as> Sie sind wieder trocken. <break time="500ms"/> <say-as interpret-as="interjection">na sieh mal einer an</say-as> ich habe doch gesagt da funkelt etwas. <break time="150ms"/> Das ist ein Drachenamulett. <break time="350ms"/> <say-as interpret-as="interjection">wow</say-as> Das ist was ganz Besonderes. <break time="400ms"/> Ich frage mich nur, wer das hier verloren hat. <break time="200ms"/> Hänge es dir um deinen Hals. Sowas bringt Glück. <break time="350ms"/> Dann lass uns zurück und dann weiter nach links. <break time="100ms"/> Sage [[Weiter nach links]]. ';
        return output;
    },
    Weiter_nach_rechts: function(attributes) {
        var output = '';
        if (attributes.visited.includes("Gehstock übergeben")) {
            output += '<audio src="https://nordicskills.de/audio/DerZauberwaldKendell/nach_wasserfall.mp3" /> Ich freue mich immer noch über deine Hilfsbereitschaft. <break time="150ms"/> Du bist eine wirklich gute Elfin und ich habe immer mehr das Gefühl, dass die Entscheidung dich zu bitten, die richtige war. <break time="200ms"/> Als kleine Belohnung darfst du heut Abend deinen vierten Zauber lernen. Aber ersteinmal müssen wir nach da oben. <break time="150ms"/> ';
        } else {
            output += '<say-as interpret-as="interjection">na?</say-as> Lyria? <break time="150ms"/> Die Wichtel haben dir dein Brot gestohlen. <break time="200ms"/> Hast du das nicht bemerkt? Tja. Könnte sein das die sich bei dir bedanken wollten. Für die nicht Hilfe. ';
            attributes.scene["Item_Brot"] -= 3;
            output += '<audio src="https://nordicskills.de/audio/DerZauberwaldKendell/zum_see_gehen.mp3" /> Naja <break time="150ms"/> du hättest den beiden schon helfen können. <break time="300ms"/> Aber wenn du nicht willst. Dann lass uns weiter gehen. ';
        }
        output += 'Dafür sage [[Den Berg hinauf]]. ';
        return output;
    },
    Den_Berg_hinauf: function(attributes) {
        var output = '';
        output += '<audio src="https://nordicskills.de/audio/DerZauberwaldKendell/schotterweg.mp3" /> <say-as interpret-as="interjection">achtung</say-as><audio src="https://nordicskills.de/audio/DerZauberwaldKendell/feen_fluegelschlag_schnell.mp3" /> Da stürzen Geröllsteine den Hang herunter. <audio src="https://nordicskills.de/audio/DerZauberwaldKendell/steinschlag.mp3" /> Komisch. <break time="150ms"/> Wo kommen denn auf einmal diese großen Felsbrocken her? <break time="250ms"/> Ich glaube, da will einer nicht das wir ins Tal gehen. <break time="250ms"/> Dann pass auf und lass uns vorsichtig weiter gehen. <audio src="https://nordicskills.de/audio/DerZauberwaldKendell/schotterweg.mp3" /> <say-as interpret-as="interjection">Puh</say-as><break time="10ms"/> gleich haben wir es geschafft. Aber eines kann ich dir versprechen. Der Anblick wird uns belohnen. <break time="200ms"/> Sage [[Nach oben]]. ';
        return output;
    },
    Nach_oben: function(attributes) {
        var output = '';
        output += '<audio src="https://nordicskills.de/audio/DerZauberwaldKendell/schotterweg.mp3" /> gleich sind wir oben <break time="450ms"/> jetzt schau dir das an. ';
        output += '<audio src="https://nordicskills.de/audio/DerZauberwaldKendell/blick_aufs_tal.mp3" /> <say-as interpret-as="interjection">wow</say-as><break time="100ms"/> Was für ein Anblick. <break time="150ms"/> Das <phoneme alphabet="ipa" ph="taːl">Tal</phoneme> ist traumhaft schön. <break time="500ms"/> Aber vergessen wir nicht warum wir hier sind <break time="250ms"/> also los. Weiter gehts. <break time="200ms"/> Wenn du soweit bist sage [[Ins Tal]]. ';
        return output;
    },
    Ins_Tal: function(attributes) {
        var output = '';
        output += '<audio src="https://nordicskills.de/audio/DerZauberwaldKendell/schotterweg.mp3" /> Auf dem Weg nach unten erzähle ich dir wie Valaria zu ihrem Elfenbogen kam. <audio src="https://nordicskills.de/audio/DerZauberwaldKendell/waldweg_steine_laub_voegel.mp3" /> Valaria war eine junge, noch unerfahrene Elfin, so wie du. <break time="200ms"/> Sie fand eines Tages bei ihren täglichen Übungen im Wald ein Ei. Ein sehr sehr großes und bläuliches Ei. <break time="200ms"/> Ihr war sofort klar, dass es kein normales Ei war. <break time="200ms"/> Sie wollte es gerade einstecken, als plötzlich ein Drache vor ihr auftauchte, sie anfauchte und zu ihr sprach <audio src="https://nordicskills.de/audio/DerZauberwaldKendell/tarrador_will_sein_ein.mp3" /> Du kannst dir sicher vorstellen, dass Valaria fast einen Herzstillstand hatte. <break time="200ms"/> Schließlich trifft man ja nicht jeden Tag auf einen Drachen. <break time="150ms"/> Lyria meine junge Elfin. Ich glaube es ist Zeit für eine Pause. <break time="350ms"/> Schau mal da vorn, dort ist ein netter Platz. Oder was meinst du? <break time="150ms"/> Dann erzähle ich dir die Geschichte weiter. <break time="200ms"/> Sage [[Jetzt nicht]] oder [[Pause machen]]. ';
        return output;
    },
    Zum_Seeufer: function(attributes) {
        var output = '';
        output += '<audio src="https://nordicskills.de/audio/DerZauberwaldKendell/schotterweg.mp3" /> Warte. Ich flieg mal über die Stelle und schau ob ich dort was sehen kann. <audio src="https://nordicskills.de/audio/DerZauberwaldKendell/feen_fluegelschlag.mp3" /> Hier. Komm hier her. <say-as interpret-as="interjection">schau an</say-as> dort schwimmt er auf dem Wasser. <audio src="https://nordicskills.de/audio/DerZauberwaldKendell/zum_see_gehen.mp3" /> Holst du ihn bitte da raus? <audio src="https://nordicskills.de/audio/DerZauberwaldKendell/stock_aus_wasser.mp3" /> Klasse gemacht. <break time="300ms"/> Schau dir diesen kleinen winzigen Gehstock an <break time="100ms"/> und überlege genau was du machst. <break time="150ms"/> Sage [[Gehstock übergeben]] oder [[Zerbrechen]]. ';
        return output;
    },
    Gehstock_übergeben: function(attributes) {
        var output = '';
        if (attributes.visited.includes("zerbrechen")) {
            output += 'Gut das du es dir doch noch überlegt hast. ';
        } else {
            attributes.scene["Item_Glücksmünze"] = 1;
        }
        output += 'Sei vorsichtig die beiden haben Angst vor dir. <audio src="https://nordicskills.de/audio/DerZauberwaldKendell/lyria_gehstock_uebergeben.mp3" /> Schau mal wie die sich freuen. <audio src="https://nordicskills.de/audio/DerZauberwaldKendell/lyria_wichtel_sagen_danke.mp3" /> <say-as interpret-as="interjection">siehste?</say-as> hilfsbereitschaft zahlt sich aus. <break time="100ms"/> Die Goldene Münze, die du bekommen hast, ist eine Glücksmünze. <break time="100ms"/> Verlier die bloß nicht. <break time="250ms"/> Na gut lass uns weiter gehen. Sage [[Weiter nach rechts]] ';
        return output;
    },
    Zerbrechen: function(attributes) {
        var output = '';
        output += 'Lyria. <say-as interpret-as="interjection">ist nicht dein ernst</say-as><break time="100ms"/> Bist du dir da sicher? <break time="200ms"/> Überlege genau und sage dann <break time="150ms"/> [[Ja bin ich]] oder <break time="150ms"/> [[Gehstock übergeben]]. ';
        return output;
    },
    Ja_bin_ich: function(attributes) {
        var output = '';
        output += '<audio src="https://nordicskills.de/audio/DerZauberwaldKendell/stab_zerbrechen.mp3" /> <say-as interpret-as="interjection">oh oh</say-as> <audio src="https://nordicskills.de/audio/DerZauberwaldKendell/bestrafung_wichtelblitz.mp3" /> <say-as interpret-as="interjection">autsch</say-as> das tat weh. So ein Wichtelblitz ist kein Spaß. <break time="150ms"/> Tja das wars. Du bist tot. ';
        if (attributes.scene["Item_Lebenstrank"] > 0) {
            output += 'Wenn du willst kannst du einen Lebenstrank nehmen. Dann sage [[Lebenstrank nehmen]]. ';
        } else {
            output += 'da du keinen Lebenstrank mehr hast, hast du verloren. <audio src="https://nordicskills.de/audio/DerZauberwaldKendell/verlorenundtot.mp3" /> Um noch einmal zu spielen, Sage Neustarten. <break time="50ms"/> Um zu beenden sage beenden oder Stop. ';
        }
        return output;
    },
    Lebenstrank_nehmen: function(attributes) {
        var output = '';
        if (attributes.visited.includes("Ich bleibe hier")) {
            attributes.scene["Item_Lebenstrank"] -= 1;
            output += '<audio src="https://nordicskills.de/audio/DerZauberwaldKendell/zaubertrank_trinken.mp3" /> ';
            output += 'Tja. Wenn man von einem Blitz getroffen wird, tut das schon weh. <break time="200ms"/> Aber du hattest ja Glück und konntest gerettet werden. Da du noch einen Lebenstrank hattest. Jetzt hast du noch ' + attributes.scene["Item_Lebenstrank"] + ' Lebenstrank. Zum Weiterspielen, sage [[Zum Tal]]. ';
        } else {
            attributes.scene["Item_Lebenstrank"] -= 1;
            output += '<audio src="https://nordicskills.de/audio/DerZauberwaldKendell/zaubertrank_trinken.mp3" /> ';
            output += '<say-as interpret-as="interjection">halleluja</say-as>. Durch den Lebenstrank, den du gerade genommen hast, kannst du nach dem du dich erholt hast, weiter gehen. Aber vergesse nicht, du hast nur noch ' + attributes.scene["Item_Lebenstrank"] + ' im Beutel. <break time="250ms"/> Wenn du soweit bist, sage [[Zum See]]. ';
        }
        return output;
    },
    Jetzt_nicht: function(attributes) {
        var output = '';
        attributes.scene["SpielerHungrig"] = 1;
        output += 'Bist du sicher? <break time="100ms"/> Du hast doch schon seit Stunden nichts mehr gegessen. <break time="150ms"/> Wenn dir schwummrig wird und du umkippst, <break time="250ms"/> ich kann dir nicht helfen. <break time="100ms"/> Willst du nicht doch was essen? Dann sage [[Etwas essen]] wenn nicht, sage [[Zum Tal]]. ';
        return output;
    },
    Pause_machen: function(attributes) {
        var output = '';
        if (attributes.visited.includes("Pause machen")) {
            output += '<audio src="https://nordicskills.de/audio/DerZauberwaldKendell/waldweg_steine_laub_voegel.mp3" /> ';
            output += 'Du hattest doch schon eine Pause gemacht. Komm lass uns weiter gehen. Sage [[Zum Tal]] oder [[Etwas essen]]. ';
        } else {
            attributes.scene["GeschichteTeil3"] = 1;
            output += '<audio src="https://nordicskills.de/audio/DerZauberwaldKendell/waldweg_steine_laub_voegel.mp3" /> Setz dich und hör mir weiter zu. <break time="100ms"/> Jetzt kommt der spannende Teil der Geschichte. <break time="200ms"/> Valaria schaute das Ei an. Dann schaute sie den Drachen an <break time="100ms"/> und sagte mit sanfter Stimme <audio src="https://nordicskills.de/audio/DerZauberwaldKendell/valaria_spricht_zu_tarrador.mp3" /> Sie legte das Ei sanft zu Boden, ging drei Schritte zurück. Der Drache holte tief Luft und spuckte einen mächtigen Feuerstoß über Valarias Kopf. <audio src="https://nordicskills.de/audio/DerZauberwaldKendell/tarrador_spuckt_feuer.mp3" /> Dann sagte der Drache <audio src="https://nordicskills.de/audio/DerZauberwaldKendell/tarrador_danke.mp3" /> und flog mit seinem Ei, in seinen Klauen davon. <break time="300ms"/> Als Valaria sich umdrehte und den verbrannten Baum sah, viel ein Ast vor ihre Füße und zerbrach. <break time="200ms"/> Ein Windstoß wehte die Asche davon und übrig blieb Aurora <break time="100ms"/> der Elfenbogen. <break time="100ms"/> Bevor der Drache davonflog, ließ er noch eine Sehne von einem Einhorn fallen und verschwand. <break time="300ms"/> Valaria spannte den Bogen mit der Einhornsehne und nahm den Bogen an sich. <break time="100ms"/><say-as interpret-as="interjection">seufz</say-as>. Ich finde das ist die beste Stelle in der Geschichte. <break time="200ms"/> So. Jetzt ist es Zeit was zu Essen. Oder was meinst du? <break time="150ms"/> Sage [[Etwas essen]] oder [[Zum Tal]]. ';
        }
        return output;
    },
    Etwas_essen: function(attributes) {
        var output = '';
        output += '<audio src="https://nordicskills.de/audio/DerZauberwaldKendell/schotterweg.mp3" /> ';
        if (attributes.scene["Item_Brot"] > 0) {
            output += 'Du hast noch ' + attributes.scene["Item_Brot"] + ' Stück Brot. Dann Esse jetzt was, damit du mir nicht vom Fleisch fällst. <audio src="https://nordicskills.de/audio/DerZauberwaldKendell/lyria_essen_trinken.mp3" /> Lyria? schmeckt es dir? <audio src="https://nordicskills.de/audio/DerZauberwaldKendell/lyria_sagt_danke_wieso.mp3" /> man hörts. Du schmatzt. <say-as interpret-as="interjection">bon appetit</say-as>. Und wenn du soweit bist, <break time="100ms"/> ';
            attributes.scene["Item_Brot"] -= 1;
            attributes.scene["SpielerHungrig"] = 0;
        } else {
            output += 'du kannst nichts Essen, du hast kein Brot mehr. Deine Beeren hast du auch alle gegessen. <break time="100ms"/> Tja, dann gehts jetzt hungrig weiter. ';
            attributes.scene["SpielerHungrig"] = 1;
        }
        output += 'Sage [[Zum Tal]]. ';
        output += ' ';
        return output;
    },
    Zum_Tal: function(attributes) {
        var output = '';
        if (attributes.visited.includes("Etwas essen") && attributes.scene["SpielerHungrig"] > 0) {
            output += '<audio src="https://nordicskills.de/audio/DerZauberwaldKendell/magenknurren.mp3" /> Wenn das dein Magen war, dann tut es mir leid. Aber wer sich sein Brot von Wichteln stehlen lässt, hat selber schuld. <audio src="https://nordicskills.de/audio/DerZauberwaldKendell/leichtesgewitter.mp3" /> Hör mal. Da kommt ein Unwetter auf uns zu. wir sollten uns einen Platz zum Schutz und für die Nacht suchen. Außerdem ist es Zeit für deine nächste Lesestunde. Ich hoffe du kannst mit leeren Magen lernen und schlafen? <audio src="https://nordicskills.de/audio/DerZauberwaldKendell/leichtesgewitter.mp3" /> Da vorne ist ein Unterschlupf, dort sollten wir einen sicheren und trocknen Platz für die Nacht finden. Wenn du dort übernachten willst, sage [[Zum Unterschlupf]] oder [[Der gefällt mir nicht]]. ';
        } else {
            if (attributes.visited.includes("Pause machen") && attributes.scene["SpielerHungrig"] > 0) {
                output += '<audio src="https://nordicskills.de/audio/DerZauberwaldKendell/magenknurren.mp3" /> Wenn das dein Magen war, dann solltest du etwas Essen. Mit leeren Magen lässt es sich nicht so Reisen. Außerdem solltest du umkippen kann ich dich nicht aufheben und tragen. Sage [[Etwas essen]]. ';
            } else {
                if (attributes.visited.includes("Etwas essen") && attributes.scene["SpielerHungrig"] == 0) {
                    output += 'So gestärkt lässt es sich doch gleich leichter Wandern oder? <audio src="https://nordicskills.de/audio/DerZauberwaldKendell/lyria_sagt_ja_und_fragt.mp3" /> Doch sicherlich. Ich esse Blüttenblätter, Nektar und liebe den Morgentau. <break time="300ms"/> <audio src="https://nordicskills.de/audio/DerZauberwaldKendell/leichtesgewitter.mp3" /> Hör mal. Da kommt ein Unwetter auf uns zu. Wir sollten uns einen Platz zum Schutz und für die Nacht suchen. Außerdem ist es Zeit für deine nächste Lesestunde. Da vorne ist ein Unterschlupf, dort sollten wir einen sicheren und trocknen Platz für die Nacht finden. Wenn du dort übernachten willst, sage [[Zum Unterschlupf]] oder du sagst [[Der gefällt mir nicht]]. ';
                } else {
                    output += '<audio src="https://nordicskills.de/audio/DerZauberwaldKendell/leichtesgewitter.mp3" /> ';
                    output += 'Hör mal. Da kommt ein Unwetter auf uns zu. Wir sollten uns einen Platz zum Schutz und für die Nacht suchen. Außerdem ist es Zeit für deine nächste Lesestunde. Da vorne ist ein Unterschlupf, dort sollten wir einen sicheren und trocknen Platz für die Nacht finden. Wenn du dort übernachten willst, sage [[Zum Unterschlupf]] oder du sagst [[Der gefällt mir nicht]]. ';
                }
            }
        }
        return output;
    },
    Zum_Unterschlupf: function(attributes) {
        var output = '';
        output += '<audio src="https://nordicskills.de/audio/DerZauberwaldKendell/mittleresgewitter.mp3" /> <say-as interpret-as="interjection">oh mann</say-as><break time="10ms"/> Was für ein blödes Wetter. ';
        if (attributes.visited.includes("Zur Eiche gehen")) {
            output += '<break time="150ms"/> Gut das du mitgekommen bist. Als Tote Elfin hilfst du mir nicht. Warte mal ich muss mir meine Flügel trocknen. <audio src="https://nordicskills.de/audio/DerZauberwaldKendell/feen_fluegelschlag.mp3" /> So. Jetzt fühl ich mich besser. Gut Gut. Jetzt wird es Zeit das du dir deine Schriftrollen schnapst und die nächste Rolle liest. Sage [[Schriftrolle vier lesen]]. ';
        } else {
            output += 'Warte mal ich muss mir meine Flügel trocknen. <audio src="https://nordicskills.de/audio/DerZauberwaldKendell/feen_fluegelschlag.mp3" /> So. Jetzt fühl ich mich besser. Gut Gut. Jetzt wird es Zeit das du dir deine Schriftrollen schnapst und die nächste Rolle liest. Sage [[Schriftrolle vier lesen]] ';
        }
        return output;
    },
    Der_gefällt_mir_nicht: function(attributes) {
        var output = '';
        output += '<audio src="https://nordicskills.de/audio/DerZauberwaldKendell/leichtesgewitter.mp3" /> Die Vögel hören schon auf zu zwitschern. <break time="100ms"/> Da vorn ist eine Alternative. <break time="150ms"/> Ich würde sie nicht wählen. Aber du fällst ja hier die Entscheidung <break time="100ms"/> und somit trägst du auch die Verantwortung. <audio src="https://nordicskills.de/audio/DerZauberwaldKendell/mittleresgewitter.mp3" /> Aber langsam musst du dich entscheiden, denn das Wetter wird immer schlimmer. Wenn du dich entschieden hast, sage [[Zur Eiche gehen]] oder [[Zum Unterschlupf]]. ';
        return output;
    },
    Zur_Eiche_gehen: function(attributes) {
        var output = '';
        output += '<audio src="https://nordicskills.de/audio/DerZauberwaldKendell/leichtesgewitter.mp3" /> <say-as interpret-as="interjection">ey</say-as><break time="10ms"/> hast du ein Glück. Da hat jemand sein Vorratsbeutel liegen gelassen. <break time="100ms"/> Das sind ein Apfel und Zwei Stück Brot drin. Dann lass es dir schmecken. <audio src="https://nordicskills.de/audio/DerZauberwaldKendell/lyria_essen_trinken.mp3" /> Lyria? schmekt es dir? <audio src="https://nordicskills.de/audio/DerZauberwaldKendell/lyria_sagt_danke_wieso_gewitter.mp3" /> man hörts. ';
        attributes.scene["Item_Brot"] += 1;
        attributes.scene["SpielerHungrig"] = 0;
        output += '<audio src="https://nordicskills.de/audio/DerZauberwaldKendell/schweresgewitter.mp3" /> <say-as interpret-as="interjection">gott im himmel</say-as>, das wird mir hier zu gefährlich. Ich will hier weg. <break time="100ms"/> Wenn du mitkommen willst sage [[Zum Unterschlupf]] oder [[Ich bleibe hier]]. ';
        return output;
    },
    Ich_bleibe_hier: function(attributes) {
        var output = '';
        output += '<audio src="https://nordicskills.de/audio/DerZauberwaldKendell/blitzeinschlag_eiche.mp3" /> Tja das wars. Du bist tot. Wie kann man sich bei Gewitter auch unter einer Eiche stellen. Kennst du nicht das Sprichwort <break time="100ms"/> Eichen sollst du weichen buchen sollst du suchen? ';
        if (attributes.scene["Item_Lebenstrank"] > 0) {
            output += 'Wenn du willst kannst du einen Lebenstrank nehmen. Dann sage [[Lebenstrank nehmen]]. <break time="100ms"/> Um noch einmal zu spielen, Sage Neustarten. <break time="50ms"/> Um zu beenden sage beenden oder Stop. ';
        } else {
            output += 'Da du keinen Lebenstrank mehr hast, hast du verloren. <audio src="https://nordicskills.de/audio/DerZauberwaldKendell/verlorenundtot.mp3" /> Um noch einmal zu spielen, Sage Neustarten. <break time="50ms"/> Um zu beenden sage beenden oder Stop. ';
        }
        return output;
    },
    Schriftrolle_vier_lesen: function(attributes) {
        var output = '';
        attributes.scene["Funem"] = 1;
        output += 'Dann fang an zu lesen und sei wie immer vorsichtig. Pass genau auf beim lernen. <audio src="https://nordicskills.de/audio/DerZauberwaldKendell/schriftrolle.mp3" /> <audio src="https://nordicskills.de/audio/DerZauberwaldKendell/zauber_spruch.mp3" />Gut gemacht Lyria, du hast deinen vierten Zauber gelernt, dies ist ein Seilzauber, er heißt Funem. <break time="100ms"/> Mit ihm kannst du ein Seil herbeizaubern <break time="100ms"/> um dich an Bäume, Wände oder anderes auf oder abzuseilen. <break time="100ms"/> Das ist praktisch, wenn du keine Leiter hast. So Lyria, für heute reicht es. Morgen werden wir das Tal erkunden und versuchen die Höhle zu finden. <break time="100ms"/> Jetzt lass uns schlafen gehen. Wenn du soweit bist sage [[Gute nacht]]. ';
        return output;
    },
    Gute_nacht: function(attributes) {
        var output = '';
        output += '<audio src="https://nordicskills.de/audio/DerZauberwaldKendell/lagerfeuer_nachts.mp3" /> Schlaf gut Lyria. Ich bin gespannt was uns Morgen alles so erwartet. <audio src="https://nordicskills.de/audio/DerZauberwaldKendell/baldur_schlaeft.mp3" /> Guten Morgen Lyria. <say-as interpret-as="interjection">donner und doria</say-as>. Hast du gesägt heute Nacht. <break time="150ms"/> Ich dachte schon der Wald fleht um gnade. <break time="150ms"/> <say-as interpret-as="interjection">war nur ein scherz</say-as>. <break time="200ms"/> Aber wenn du wach bist, sage [[Aufstehen]]. ';
        return output;
    },
    Wir_können_weiter: function(attributes) {
        var output = '';
        if (attributes.scene["Verletzt_Kobobande"] == 1) {
            output += 'Tja. Da hast du glückgehabt, dass deine Verletzungen so schnell geheilt sind. <break time="250ms"/> Aber jetzt stehst du erneut vor einer Entscheidung. <break time="250ms"/> Denn wenn wir gleich am Ende des Weges sind, musst du dich wieder entscheiden. <break time="100ms"/> Dort kannst du nach links oder nach rechts gehen. <break time="100ms"/> Auch diesmal, kann ich dir nicht sagen welchen <phoneme alphabet="ipa" ph="veːk">Weg</phoneme> du nehmen sollst. <break time="100ms"/> links ist der lange, aber ruhigere <phoneme alphabet="ipa" ph="veːk">Weg</phoneme> und rechts ist der kurze, aber ungemütliche <phoneme alphabet="ipa" ph="veːk">Weg</phoneme>. <break time="250ms"/> Welchen <phoneme alphabet="ipa" ph="veːk">Weg</phoneme> möchtest du nehmen? Sage [[Die lange Route]] oder [[Die kurze Route]] ';
            attributes.scene["Verletzt_Kobobande"] = 0;
        } else {
            output += 'Hörzu Lyria. Ich sage dir jetzt wie es weiter geht. <break time="100ms"/> Zuerst müssen wir durch das Tal. <break time="150ms"/> Ich kann nicht genau sagen was dort noch alles auf uns zu kommt. Hier in Kendell ist alles möglich. <break time="250ms"/> Du wirst dich Wundern, dich erschrecken <break time="100ms"/> und weitere Aufgaben erledigen müssen. <break time="250ms"/> Am Ende des Tals, kommen wir dann zu dem Berg, in der die Höhle ist die wir suchen. <break time="200ms"/> Und ja, dort ist dann Aurora der Elfenbogen von Valaria versteckt. <break time="250ms"/> Wenn wir gleich am Ende des Weges sind, musst du dich entscheiden. Denn dort kannst du nach links oder nach rechts gehen. <break time="100ms"/> Und nein, ich kann dir nicht sagen welchen <phoneme alphabet="ipa" ph="veːk">Weg</phoneme> du nehmen sollst. Ich kann dir aber soviel verraten <break time="100ms"/> links ist der lange, aber ruhigere <phoneme alphabet="ipa" ph="veːk">Weg</phoneme> und rechts ist der kurze, aber ungemütliche <phoneme alphabet="ipa" ph="veːk">Weg</phoneme>. <break time="250ms"/> Welchen <phoneme alphabet="ipa" ph="veːk">Weg</phoneme> möchtest du nehmen? Sage [[Die kurze Route]] oder [[Die lange Route]]. ';
        }
        return output;
    },
    Die_kurze_Route: function(attributes) {
        var output = '';
        output += '<audio src="https://nordicskills.de/audio/DerZauberwaldKendell/waldweg_steine_laub_voegel.mp3" /> Irgendwo soll hier eine alte Eiche sein. Die soll so alt sein, dass niemand ihr wahres Alter kennt. <break time="100ms"/> Aus Erzählungen weiß ich, dass sie schon vor den Riesen hier war. <break time="100ms"/> Mann nennt sie den Baum der Weisheit <break time="100ms"/> und wenn du ihre Äste berührst <break time="150ms"/> beziehungsweise berühren darfst, kannst du ihr eine Frage stellen. <audio src="https://nordicskills.de/audio/DerZauberwaldKendell/wald_steine_wind.mp3" /> Die Vögel sind schon wieder verstummt. <break time="150ms"/> Was zum Kuckuck ist denn das? <break time="150ms"/> Schau mal ein riesiger Schatten und es wird plötzlich so dunkel. <audio src="https://nordicskills.de/audio/DerZauberwaldKendell/tarrador_fliegt_wald.mp3" /> <say-as interpret-as="interjection">ich glaub mein schwein pfeift</say-as>. Das war Tarrador der alte Drache. Von dem habe ich dir doch erzählt. Der ist in Richtung der Höhle geflogen. Wir sollten auch weiter in Richtung des Berges gehen. Sage [[Richtung Berg gehen]]. ';
        return output;
    },
    Die_lange_Route: function(attributes) {
        var output = '';
        if (attributes.visited.includes("Gesundheitstrank nehmen")) {
            output += '<audio src="https://nordicskills.de/audio/DerZauberwaldKendell/waldweg_steine_laub_voegel.mp3" /> Das ist doch die Stelle, an der wir auf meine Schwestern gestoßen sind? <break time="200ms"/> Ab jetzt sollten wir besser vorsichtig sein. <break time="200ms"/> Darum sage [[Vorsichtig weiter gehen]]. ';
        } else {
            output += '<audio src="https://nordicskills.de/audio/DerZauberwaldKendell/waldweg_steine_laub_voegel.mp3" /> Mal sehen ob deine Entscheidung, die richtige ist. <break time="200ms"/> <audio src="https://nordicskills.de/audio/DerZauberwaldKendell/waldweg_steine_laub_voegel.mp3" /> Bleib stehen und sei ganz leise. <audio src="https://nordicskills.de/audio/DerZauberwaldKendell/feenlachen.mp3" /> Da vorn sind zwei meiner Schwestern. Ich werde mal zu ihnen fliegen und du versteckst dich besser. <audio src="https://nordicskills.de/audio/DerZauberwaldKendell/feen_fluegelschlag_wald.mp3" /> Sage [[Verstecken]] oder [[Nein mach ich nicht]]. ';
        }
        return output;
    },
    Verstecken: function(attributes) {
        var output = '';
        output += '<audio src="https://nordicskills.de/audio/DerZauberwaldKendell/feen_tanzen.mp3" /><audio src="https://nordicskills.de/audio/DerZauberwaldKendell/feen_fluegelschlag_wald.mp3" /> Da bin ich wieder. <break time="150ms"/> Hast du uns beim <phoneme alphabet="ipa" ph="ˈtanʦn̩">Tanzen</phoneme> gesehen? <audio src="https://nordicskills.de/audio/DerZauberwaldKendell/lyria_ja_das_habe_ich_putzig.mp3" /> Das machen wir immer, wenn wir uns treffen. <break time="150ms"/> Sie machen sich über mich lustig. <break time="100ms"/> Weil ich mit einem Grünschnabel wie dir unterwegs bin. <break time="200ms"/> Ich habe Ihnen gesagt das du eine lernwillige, mutige und hilfsbereite Elfin bist. <break time="150ms"/> Daraufhin haben sie mir gesagt, dass wir beide auf der Hut seien sollen und das sich die Gobobande hier herumtreibt. <break time="1s"/> Die Gobobande kennst du nicht. <break time="250ms"/> Das sind drei Goblins die ihre Streiche spielen und den Besuchern von Kendell auflauern, um sie auszurauben oder zu erschrecken. <break time="200ms"/> Wir müssen beide aufpassen. <break time="150ms"/> Sage [[Vorsichtig weiter gehen]]. ';
        return output;
    },
    Nein_mach_ich_nicht: function(attributes) {
        var output = '';
        output += '<audio src="https://nordicskills.de/audio/DerZauberwaldKendell/feen_fluegelschlag_wald.mp3" /> Das war echt blöde von dir. <break time="100ms"/> Warum hast du dich nicht versteckt? <break time="350ms"/> Jetzt sind meine Schwestern verschreckt abgehauen. <break time="500ms"/> Egal. Lass uns weiter gehen. <break time="200ms"/> Wir müssen jetzt da vorn über die Wiese gehen. Dafür sage [[Über die Wiese gehen]]. ';
        return output;
    },
    Vorsichtig_weiter_gehen: function(attributes) {
        var output = '';
        output += '<audio src="https://nordicskills.de/audio/DerZauberwaldKendell/wald_steine_wind.mp3" /><say-as interpret-as="interjection">halt</say-as>. Bleib stehen und schau hin. Da ist die Gobobande. Wenn du leise bist ziehen sie vorbei. <audio src="https://nordicskills.de/audio/DerZauberwaldKendell/goblins_vorbeiziehen.mp3" /> Gut das meine Schwestern uns gewarnt haben. Mit denen ist nicht zu spaßen. Lass uns noch eine kleine Pause einlegen und dann gehts weiter. <audio src="https://nordicskills.de/audio/DerZauberwaldKendell/kleine_pause.mp3" /> OK. Die Luft scheint frei von Goblins zu sein. Da vorn ist eine kleine Wiese, über die müssen wir gehen. Schau dich noch einmal um und dann sage, [[Über die Wiese gehen]]. ';
        return output;
    },
    Über_die_Wiese_gehen: function(attributes) {
        var output = '';
        if (attributes.visited.includes("Gesundheitstrank nehmen")) {
            output += '<audio src="https://nordicskills.de/audio/DerZauberwaldKendell/auf_grass_gehen.mp3" /> Ich hätte natürlich gerne gewusst, ob meine Schwestern etwas zu erzählen hatten, aber naja <break time="100ms"/> hauptsache du hast aus deinem Fehler gelernt. <break time="100ms"/> Jetzt passiert dir dieser Fehler sicherlich nicht noch einmal. <break time="450ms"/> Nun gut. Lass uns schnell die freie Wiese verlassen. Dafür sage [[Von der Wiese verschwinden]]. ';
        } else {
            if (attributes.visited.includes("Vorsichtig weiter gehen")) {
                output += '<audio src="https://nordicskills.de/audio/DerZauberwaldKendell/auf_grass_gehen.mp3" /><say-as interpret-as="interjection">halleluja</say-as>. Ich bin so froh das wir auf meine Schwestern gestoßen sind und Sie uns gewarnt haben. Ich habe Sachen gehört <break time="500ms"/> Naja. Die Gobobande sind ziemliche Fieslinge. Wenn du glück hast, machen Sie nur ihren Spaß mit dir. Aber <break time="150ms"/> und das habe ich nur gehört <break time="250ms"/> sie sollen sogar einem die Ohren abschneiden. <break time="150ms"/> Nun gut. Lass uns schnell die freie Wiese verlassen. Dafür sage [[Von der Wiese verschwinden]]. ';
            } else {
                output += '<audio src="https://nordicskills.de/audio/DerZauberwaldKendell/auf_grass_gehen.mp3" /><say-as interpret-as="interjection">halt</say-as>. Bleib stehen und schau hin. Da ist irgendetwas. <say-as interpret-as="interjection">oh nein</say-as><break time="10ms"/> Das ist nicht gut, da sind Goblins. <say-as interpret-as="interjection">oh mann</say-as><break time="10ms"/>Die werden uns die Beine langziehen und vorher noch ausrauben. <audio src="https://nordicskills.de/audio/DerZauberwaldKendell/kobolde_greifen_an.mp3" /> <break time="250ms"/> Lyria? <break time="150ms"/> Lyria <break time="100ms"/> lebst du noch? <break time="100ms"/> Wenn ja, dann sage was. Sage [[Ich lebe noch]]. ';
            }
        }
        return output;
    },
    Ich_lebe_noch: function(attributes) {
        var output = '';
        if (attributes.scene["Item_Gesundheitstrank"] > 0) {
            output += 'Tja. Hättest du dich versteckt und nicht meine Schwestern erschreckt. Hätten Sie uns vielleicht gewarnt <break time="300ms"/> und du wirst nicht verletzt worden. <break time="500ms"/> Es ist so wie es ist. <break time="300ms"/> Wenn du willst, kannst du jetzt einen Gesundheitstrank nehmen. <break time="200ms"/> Dann sage [[Gesundheitstrank nehmen]]. <break time="100ms"/> Um noch einmal zu spielen, Sage Neustarten. <break time="50ms"/> Um zu beenden sage beenden oder Stop. ';
        } else {
            output += 'Da du keinen Gesundheitstrank mehr hast und deine Verletzungen so schwer sind, dass deine Wunden nicht heilen, hast du verloren. <audio src="https://nordicskills.de/audio/DerZauberwaldKendell/verlorenundtot.mp3" /> Um noch einmal zu spielen, Sage Neustarten. <break time="50ms"/> Um zu beenden sage beenden oder Stop. ';
        }
        return output;
    },
    Von_der_Wiese_verschwinden: function(attributes) {
        var output = '';
        output += '<audio src="https://nordicskills.de/audio/DerZauberwaldKendell/auf_grass_gehen.mp3" /> Schön. Das haben wir hinter uns. <say-as interpret-as="interjection">ach ja</say-as>. Das habe ich fast vergessen. Gleich kommen wir an eine uralte Eiche vorbei. Diese Eiche ist so alt, dass keiner weiß wie alt sie ist. Nur eines kann ich dir über ihr Alter sagen, sie war schon vor den Riesen hier. Man sagt sie weiß alles, das liegt sicher an ihrem Alter. <audio src="https://nordicskills.de/audio/DerZauberwaldKendell/wald_steine_wind.mp3" /> Da vorn bei den Kieseln ist sie. <audio src="https://nordicskills.de/audio/DerZauberwaldKendell/auf_kiesel_gehen.mp3" /> Lyria. Hörzu <break time="150ms"/> Die Eiche ist der Baum der Weisheit. <break time="150ms"/> Ja ja du glaubst mir sicher nicht, es stimmt aber <break time="100ms"/> und wenn du willst, kannst du zu Ihr gehen und Ihr deine Hand nach oben strecken. <break time="100ms"/> Wenn sie dich berührt, kann Sie direkt in dein Herz sehen und dir dann eine, <break time="100ms"/> aber wirklich nur eine Frage beantworten. <break time="350ms"/> Wenn du Sie etwas fragen willst, dann sage [[Zum Baum der Weisheit]] <break time="100ms"/> oder [[Will ich nicht]]. ';
        return output;
    },
    Will_ich_nicht: function(attributes) {
        var output = '';
        output += 'Achso, habe ich ja total vergessen. <break time="250ms"/> Du bist ja schon allwissend <break time="150ms"/> und eine alte Weise Elfin. <break time="300ms"/> Wie konnte ich das vergessen und an dir zweifeln. <break time="250ms"/> Dann lass uns bloß weiter große Elfin. <break time="250ms"/> Sage [[Dann lass uns weiter]]. ';
        return output;
    },
    Zum_Baum_der_Weisheit: function(attributes) {
        var output = '';
        output += '<audio src="https://nordicskills.de/audio/DerZauberwaldKendell/auf_kiesel_gehen.mp3" /> Gut. Pass auf und hör mir genau zu. <break time="300ms"/> Du nimmst deine rechte Hand, streckst sie aus. Dann reichst du deine Hand in Richtung der Äste nach oben. <break time="200ms"/> Jetzt wartest du. <break time="1s"/> <audio src="https://nordicskills.de/audio/DerZauberwaldKendell/die_eiche_mandura_bewegt_sich.mp3" /> <say-as interpret-as="interjection">siehste?</say-as>. Es hat geklappt. <audio src="https://nordicskills.de/audio/DerZauberwaldKendell/lyria_die_eiche_mandura.mp3" /> Jetzt kannst du ihm, naja ihr <break time="100ms"/> ist egal. Dem Baum, deine Frage stellen. <break time="200ms"/> Frage [[Werde ich eine große Elfin]] oder [[Finde ich Aurora]] oder [[Gewinne ich das Spiel]]. ';
        return output;
    },
    Werde_ich_eine_große_Elfin: function(attributes) {
        var output = '';
        if (attributes.scene["Mandura"] == 1) {
            output += '<audio src="https://nordicskills.de/audio/DerZauberwaldKendell/mandura_ist_wuettend.mp3" /> ';
            output += 'Lyria? Hast du mir nicht zugehöhrt? Ich sagte eine. Nicht zwei oder drei. Eine. <break time="300ms"/> Jetzt lass uns gehen. Sage [[Dann lass uns weiter]]. ';
        } else {
            attributes.scene["Mandura"] = 1;
            output += '<audio src="https://nordicskills.de/audio/DerZauberwaldKendell/lyria_mandura_antwort_zwei.mp3" /> <audio src="https://nordicskills.de/audio/DerZauberwaldKendell/lyria_mandura_sie_moege_gehen.mp3" />Du hast deine Antwort bekommen, dann lass uns gehen. Sage [[Dann lass uns weiter]]. ';
        }
        return output;
    },
    Finde_ich_Aurora: function(attributes) {
        var output = '';
        if (attributes.scene["Mandura"] == 1) {
            output += '<audio src="https://nordicskills.de/audio/DerZauberwaldKendell/mandura_ist_wuettend.mp3" /> Lyria? Hast du mir nicht zugehöhrt? Ich sagte eine. Nicht zwei oder drei. Eine. <break time="300ms"/> Jetzt lass uns gehen. Sage [[Dann lass uns weiter]]. ';
        } else {
            attributes.scene["Mandura"] = 1;
            output += '<audio src="https://nordicskills.de/audio/DerZauberwaldKendell/lyria_mandura_antwort_eins.mp3" /><audio src="https://nordicskills.de/audio/DerZauberwaldKendell/lyria_mandura_sie_moege_gehen.mp3" />Du hast deine Antwort bekommen, dann lass uns gehen. Sage [[Dann lass uns weiter]]. ';
        }
        return output;
    },
    Gewinne_ich_das_Spiel: function(attributes) {
        var output = '';
        if (attributes.scene["Mandura"] == 1) {
            output += '<audio src="https://nordicskills.de/audio/DerZauberwaldKendell/mandura_ist_wuettend.mp3" /> ';
            output += 'Lyria? Hast du mir nicht zugehöhrt? Ich sagte eine. Nicht zwei oder drei. Eine. <break time="300ms"/> Jetzt lass uns gehen. Sage [[Dann lass uns weiter]]. ';
        } else {
            attributes.scene["Mandura"] = 1;
            output += '<audio src="https://nordicskills.de/audio/DerZauberwaldKendell/lyria_mandura_antwort_drei.mp3" /><audio src="https://nordicskills.de/audio/DerZauberwaldKendell/lyria_mandura_sie_moege_gehen.mp3" />Du hast deine Antwort bekommen, dann lass uns gehen. Sage [[Dann lass uns weiter]]. ';
        }
        return output;
    },
    Dann_lass_uns_weiter: function(attributes) {
        var output = '';
        output += '<audio src="https://nordicskills.de/audio/DerZauberwaldKendell/wald_steine_wind.mp3" /> ';
        if (attributes.visited.includes("Will ich nicht")) {
            output += 'Wissen ist keine Schande. <break time="300ms"/> Es ist ja nicht so, dass ich neugierig bin, aber das war ja deine Entscheidung. ';
        } else {
            output += '<say-as interpret-as="interjection">wow</say-as><break time="100ms"/>dass du dich das getraut hast. <break time="150ms"/> Ich weiß nicht was dir der Baum der Weisheit gesagt hat, aber ich hoffe es hilft dir weiter. Ich selber habe mich noch nie getraut ihn zu fragen. ';
        }
        output += '<audio src="https://nordicskills.de/audio/DerZauberwaldKendell/wald_steine_wind.mp3" />Gut, jetzt dauert es nicht mehr lange <break time="100ms"/> dann müssten wir den Berg sehen können. <audio src="https://nordicskills.de/audio/DerZauberwaldKendell/bach_im_wald.mp3" /> <say-as interpret-as="interjection">oh oh</say-as>. Da ist ein Fluss und die Brücke ist eingestürzt. Das war bestimmt die Gobobande. <say-as interpret-as="interjection">mist</say-as>. Das ist nicht besonders schlimm. Denn du hast doch den Seilzauber Fuhnem gelernt. Benutz den einmal. <break time="150ms"/> Da dein Latein ja nicht so gut ist, darfst du den Zauber auch so sprechen. Dafür sage [[Seil]]. ';
        output += ' ';
        return output;
    },
    Seil: function(attributes) {
        var output = '';
        output += '<audio src="https://nordicskills.de/audio/DerZauberwaldKendell/zauber_fehlgeschlagen.mp3" /> <say-as interpret-as="interjection">nanu?</say-as>. Da ging wohl was schief. Das kommt, wenn du den Zauber falsch gelernt oder falsch angewendet hast. <break time="150ms"/> Der Funem Zauber ist ganz einfach, wenn du ihn sprichst und dabei Brücke sagst, wichtig ist das du dabei ein Ziel wie den Baum da drüben anvisierst. Versuch es einmal. <break time="250ms"/> Konzentrier dich <break time="250ms"/> und sage dann [[Seil Brücke]]. ';
        return output;
    },
    Seil_Brücke: function(attributes) {
        var output = '';
        output += '<audio src="https://nordicskills.de/audio/DerZauberwaldKendell/funem_zauber.mp3" /><say-as interpret-as="interjection">prima</say-as><break time="10ms"/>das hat doch klasse funktioniert. Aber du denkst daran, dass das Zaubern dir Energie kostet? Nicht vergessen. <break time="150ms"/> Du kannst jetzt noch zweimal Zaubern. Dann musst du wieder einen Magietrank nehmen. <break time="200ms"/> Aber jetzt können wir erst einmal über den Fluss rüber und auf die andere Seite. Halt dich schön am Seil fest. Ich kann ja Fliegen. <break time="100ms"/> Dafür sagst du [[Auf die andere Seite]]. ';
        return output;
    },
    Gesundheitstrank_nehmen: function(attributes) {
        var output = '';
        attributes.scene["Item_Gesundheitstrank"] -= 1;
        output += '<audio src="https://nordicskills.de/audio/DerZauberwaldKendell/zaubertrank_trinken.mp3" /> ';
        output += 'Lyria, durch den Gesundheitstrank, den du gerade getrunken hast, kannst du nach dem du dich erholt hast, weiter gehen. Aber vergesse nicht, du hast nur noch ' + attributes.scene["Item_Gesundheitstrank"] + ' im Beutel. <break time="250ms"/> Wenn du soweit bist, sage [[Aufstehen]]. ';
        return output;
    },
    Auf_die_andere_Seite: function(attributes) {
        var output = '';
        output += '<audio src="https://nordicskills.de/audio/DerZauberwaldKendell/bach_im_wald.mp3" /> Pass auf und halt dich fest. <audio src="https://nordicskills.de/audio/DerZauberwaldKendell/bach_im_wald.mp3" /> <say-as interpret-as="interjection">oje</say-as>. Das war knapp. Aber jetzt sind wir richtig. Wir sind auf der anderen Seite des Flusses. <break time="400ms"/> Wenn wir am Berg angekommen sind richtest du unser Nachtlager her und bereitest dich auf Morgen vor. Naja lass uns weiter. Wir müssen dem Fluss entlang. Dafür sage [[Dem Fluss folgen]]. ';
        return output;
    },
    Richtung_Berg_gehen: function(attributes) {
        var output = '';
        output += '<audio src="https://nordicskills.de/audio/DerZauberwaldKendell/wald_steine_wind.mp3" /> Ich dachte immer Tarrador sei verschwunden, sowie Valaria. Aber er ist am Leben und hast du gesehen wie groß er ist? <audio src="https://nordicskills.de/audio/DerZauberwaldKendell/lyria_sagt_ja_das_habe_ich.mp3" /> Ich bin gespannt, ob wir in noch einmal zu sehen bekommen. <audio src="https://nordicskills.de/audio/DerZauberwaldKendell/bach_im_wald.mp3" /> Oh. <break time="100ms"/> Wir sind auf der anderen Seite des Flusses. <break time="200ms"/> Dann schein wir richtig zu gehen. Beziehungsweise zu fliegen. <say-as interpret-as="interjection">hihi</say-as>. Naja, lass uns weiter. Sage [[Dem Fluss folgen]]. ';
        return output;
    },
    Dem_Fluss_folgen: function(attributes) {
        var output = '';
        if (attributes.visited.includes("Richtung Berg gehen")) {
            output += '<audio src="https://nordicskills.de/audio/DerZauberwaldKendell/wald_steine_wind.mp3" /> Weißt du was Lyria? Ich glaube es wird Zeit für eine Pause. <break time="150ms"/> Danach werden wir den Weg zum Berg folgen. Wenn wir dort angekommen sind, richtest du unser Nachtlager her und bereitest dich auf Morgen vor. Aber erst einmal die Pause. <break time="100ms"/> Sage [[Eine Pause einlegen]]. ';
        } else {
            output += '<audio src="https://nordicskills.de/audio/DerZauberwaldKendell/wald_steine_wind.mp3" /> Die Vögel sind schon wieder verstummt. <break time="150ms"/> Was zum Kuckuck ist denn das? <break time="150ms"/> Schau mal ein riesiger Schatten und es wird plötzlich so dunkel. <audio src="https://nordicskills.de/audio/DerZauberwaldKendell/tarrador_fliegt_wald.mp3" /> <say-as interpret-as="interjection">ich glaub mein schwein pfeift</say-as>. Das war Tarrador der alte Drache. Von dem habe ich dir doch erzählt. Der ist in Richtung der Höhle geflogen. Wir sollten gleich dem Weg zum Berg folgen, aber vorher machen wir noch eine Pause. Sage [[Eine Pause einlegen]]. ';
        }
        return output;
    },
    Zum_Berg_gehen: function(attributes) {
        var output = '';
        output += 'Ich setz mich mal wieder auf deine Schulter. <audio src="https://nordicskills.de/audio/DerZauberwaldKendell/feen_fluegelschlag.mp3" /> Das geflattere strengt mich an. <audio src="https://nordicskills.de/audio/DerZauberwaldKendell/waldweg_steine_laub_voegel.mp3" /> Siehst du den Berg da vorn? <break time="150ms"/> Da müssen wir hin. <break time="100ms"/> Ich erkläre dir während wir dahin gehen, wie das mit dem Rezept ist. <break time="100ms"/> Ich meine das für deine Gesundheitstränke. <break time="200ms"/> Also pass auf. Du nimmst die Tollkirschen. Nicht alle, sonst stirbst du. <break time="150ms"/> Nur acht Stück davon. Wickelst sie in ein Tuch <break time="100ms"/> und quetscht den Saft heraus. <break time="200ms"/> Danach weichst du vier von den trocknen Röhrlingen in deinem Wasser ein. <break time="350ms"/> Jetzt musst du deine Kräuter noch klein malen. Dafür benutz du einen Stein. <break time="250ms"/> Dann vermengst du alles und schüttelst es kräftig durch. Wenn du fertig bist, füllst du alles in eine der leeren Phiolen ab. Natürlich nur die Flüssigkeit. <say-as interpret-as="interjection">hihi</say-as><break time="10ms"/> und schon hast du zwei neue Lebenstränke. <break time="250ms"/> Ganz einfach nicht wahr? <break time="400ms"/> Das hast du dir doch sicher alles gemerkt <break time="100ms"/> oder? <audio src="https://nordicskills.de/audio/DerZauberwaldKendell/waldweg_steine_laub_voegel.mp3" /> <say-as interpret-as="interjection">juhu</say-as><break time="10ms"/> Jetzt habe ich so viel geredet, dass wir schon da sind. <break time="250ms"/><say-as interpret-as="interjection">wow</say-as><break time="10ms"/>schau dir mal die Höhle an. <break time="250ms"/> Der Eingang ist aber ziemlich groß. Da passt ja ein halber Riese rein. <break time="150ms"/>Nun gut. Hier ist der perfekte Platz, um heute Nacht zu schlafen. <break time="150ms"/> Dafür sage [[Hier übernachten]]. ';
        return output;
    },
    Eine_Pause_einlegen: function(attributes) {
        var output = '';
        output += '<audio src="https://nordicskills.de/audio/DerZauberwaldKendell/waldweg_steine_laub_voegel.mp3" /> Dieser Platz sieht doch gut aus. Hier können wir uns auf den Baumstumpf setzen und eine Pause machen. Du solltest was Essen und Trinken. <audio src="https://nordicskills.de/audio/DerZauberwaldKendell/lyria_essen_trinken.mp3" /><break time="100ms"/> Noch was anderes. <break time="100ms"/> Schau mal um dich, <break time="150ms"/> dort vorn sind ein paar Pilze. <break time="100ms"/> Nimm aber nur die Röhrlinge und links von dir findest du einige Kräuter. <break time="200ms"/> Davon nimmst du aber nur den Jungfrauenschuh. <break time="100ms"/> Dort am Strauch, findest du einige Tollkirschen und weiter hinten am Bach kannst du deine Flasche mit Wasser auffüllen. <break time="500ms"/> Aus diesen Zutaten kannst du nachher am Lager, einen Lebenstrank herstellen. <break time="150ms"/> Wie das geht, zeige ich dir dann. <break time="250ms"/> Außerdem musst du dir deine Schriftrollen nehmen und die fünfte Rolle lesen. <break time="100ms"/> Was du zuerst machen willst, musst du entscheiden. <break time="100ms"/> Dann fang an und sage [[Gegenstände sammeln]] oder [[Schriftrolle fünf lesen]]. ';
        return output;
    },
    Schriftrolle_fünf_lesen: function(attributes) {
        var output = '';
        attributes.scene["MagieStufe2"] = 1;
        attributes.scene["Ignis"] = 2;
        output += 'Dann fang an zu lesen und sei wie immer vorsichtig. Pass genau auf beim Lernen. <audio src="https://nordicskills.de/audio/DerZauberwaldKendell/schriftrolle.mp3" /> <audio src="https://nordicskills.de/audio/DerZauberwaldKendell/zauber_spruch.mp3" />Gut gemacht Lyria. <break time="150ms"/> Du hast deinen fünften Zauber gelernt. <break time="100ms"/> Dies ist ein Stärkungszauber <break time="100ms"/> er verbessert alle deine Zauber. So kannst du zum Beispiel jetzt mit dem Ignis Zauber, dass ist der Feuerzauber, einen Feuerball auf Gegner werfen. <break time="100ms"/> Dies ist besonders hilfreich bei großen Gegnern. <break time="100ms"/> ';
        if (attributes.visited.includes("Gegenstände sammeln")) {
            output += 'Du bist wirklich fleißig. Gelesen hast du, die Sachen für dein Rezept hast du auch beisammen, dann gehts jetzt weiter zum Berg. Dafür sage [[Zum Berg gehen]]. ';
        } else {
            output += 'Du solltest jetzt unbedingt noch die Gegenstände für deinen Lebenstrank sammeln, bevor wir weiter gehen. Dafür sage [[Gegenstände sammeln]]. ';
        }
        output += ' ';
        output += ' ';
        output += ' ';
        return output;
    },
    Hier_übernachten: function(attributes) {
        var output = '';
        output += '<audio src="https://nordicskills.de/audio/DerZauberwaldKendell/lagerfeuer.mp3" /> Klasse. Der Platz gefällt mir. <break time="100ms"/> Dann richte noch deinen Schlafplatz her und vergesse nicht die Zutaten zusammen zu mischen. <break time="250ms"/> Die Zutaten müssen noch frisch sein. <break time="200ms"/> Sonst wird aus deinem Lebenstrank ein Schrumpftrank. <break time="400ms"/> Obwohl? <break time="200ms"/> Dann wärst du so klein wie ich <break time="100ms"/> und du könntest auf meinen Rücken sitzen. <say-as interpret-as="interjection">nee</say-as>. Das lassen wir lieber. <break time="200ms"/> So. Was möchtest du machen? [[Zutaten mischen]] oder [[Schlafplatz herrichten]]. ';
        return output;
    },
    Gegenstände_sammeln: function(attributes) {
        var output = '';
        attributes.scene["Zutaten"] = 1;
        output += 'OK Lyria. Dann schau mal da vorne. Da ist der Bach, geh dahin und fülle deine Flasche auf. <audio src="https://nordicskills.de/audio/DerZauberwaldKendell/wasser_in_flasche_fuellen.mp3" /> ok das reicht. <break time="200ms"/> Jetzt sammel die Pilze und Kräuter. <audio src="https://nordicskills.de/audio/DerZauberwaldKendell/wald_steine_wind.mp3" /> Ich glaube du hast genug. <break time="100ms"/> Dann fehlen noch die Tollkirschen. Die sind da am Strauch beim Fluss. <audio src="https://nordicskills.de/audio/DerZauberwaldKendell/bach_im_wald.mp3" /> Super. Jetzt hast du alles zusammen. ';
        if (attributes.visited.includes("Seite fünf lesen")) {
            output += 'Du bist wirklich fleißig. Gelesen hast du, die Sachen für dein Rezept hast du auch beisammen, dann gehts jetzt weiter zum Berg. Dafür sage [[Zum Berg gehen]] ';
        } else {
            output += 'Dann solltest du jetzt noch die fünfte Schriftrolle lesen. <break time="100ms"/> Dafür musst du [[Schriftrolle fünf lesen]] sagen. ';
        }
        return output;
    },
    Zutaten_mischen: function(attributes) {
        var output = '';
        attributes.scene["Zutatenmischen"] = 1;
        attributes.scene["Item_Lebenstrank"] += 1;
        output += 'Gut. Schauen wir mal ob du alles dabeihast. Die Röhrlinge sind da. <break time="200ms"/> Die Jungfrauenschuhe auch. <break time="150ms"/> Die Tollkirschen? Achja <break time="100ms"/> da sind sie. <break time="200ms"/> Und das Wasser? <audio src="https://nordicskills.de/audio/DerZauberwaldKendell/wasser_in_der_flasche.mp3" /> Gut das reicht. <break time="200ms"/> Dann fang an die Zutaten zu verarbeiten und danach gut zu vermischen. <audio src="https://nordicskills.de/audio/DerZauberwaldKendell/rezept_herstellen.mp3" /><say-as interpret-as="interjection">wow</say-as><break time="10ms"/>Du hast gewirbelt, als wenn du die Meisterköchin persönlich bist. So schnell habe ich noch niemanden einen Zaubertrank herstellen gesehen. <break time="200ms"/>OK. Da du damit fertig bist, gehts jetzt weiter. ';
        if (attributes.visited.includes("Schlafplatz herrichten")) {
            output += 'Das Feuer ist an und unser Schlafplatz ist fertig <break time="150ms"/> dann kannst du jetzt mit dem lernen anfangen. Nimm deine Schriftrollen, setzt dich ans Feuer und fang an zu lernen. Sage [[Schriftrolle sechs lernen]]. ';
        } else {
            output += 'Damit wir es nachher warm und trocken haben, muss das Lager noch hergerichtet werden, dafür sagst du [[Schlafplatz herrichten]]. ';
        }
        return output;
    },
    Schlafplatz_herrichten: function(attributes) {
        var output = '';
        output += '<audio src="https://nordicskills.de/audio/DerZauberwaldKendell/holzsammeln_wald.mp3" /> Ich glaube das reicht jetzt. Wir wollen ja nur ein Lagerfeuer für diese Nacht. <break time="200ms"/> Morgen geht es auf die Suche nach Raknur. <break time="100ms"/> Dann mach mal das Feuer an. Nicht vergessen dafür kannst du deinem Feuerzauber benutzen. Du bist ja schließlich eine Elfin. <break time="150ms"/> Sage [[Ignis Lagerfeuer]]. ';
        return output;
    },
    Schriftrolle_sechs_lernen: function(attributes) {
        var output = '';
        attributes.scene["Silentium"] = 1;
        output += 'Dann fang an zu lesen und sei wie immer vorsichtig. Pass genau auf beim Lernen. So wie schon beim letzten Mal. <audio src="https://nordicskills.de/audio/DerZauberwaldKendell/schriftrolle.mp3" /> <audio src="https://nordicskills.de/audio/DerZauberwaldKendell/zauber_spruch.mp3" /> Sehr gut Lyria, du hast deinen sechsten und vorerst letzten Zauber gelernt. <break time="150ms"/> Dies ist ein Zauber der die Bewegung von Menschen, Tieren und Dingen anhält. Sagen wir, der Zauber verlangsamt oder stoppt die Bewegungen. <break time="100ms"/> Er heißt Silentium und ist einer der fünf mächtigsten Zauber. <break time="100ms"/> Besonders effektiv um dich bei einem Angriff aus dem Staub zu machen. Oder wenn andere Gefahren drohen, kannst du durch diesen Zauber Zeit gewinnen und Situationen zu verändern. <break time="250ms"/> Aber eines sei dir gewiss, so hilfreich es sein kann so gefährlich ist er auch. Daher ist er mit Bedacht einzusetzen. <break time="200ms"/> So Lyria, für heute reicht es. Morgen werden wir die Höhle betreten. <break time="100ms"/> Jetzt lass uns schlafen gehen. Wenn du soweit bist sage [[Schlafen gehen]]. ';
        return output;
    },
    Schlafen_gehen: function(attributes) {
        var output = '';
        output += '<audio src="https://nordicskills.de/audio/DerZauberwaldKendell/hoehlengeraeusch.mp3" /> Hast du das gehört? <break time="250ms"/> Ich glaub das kam aus der Höhle. Nur gut das ich Fliegen kann. <break time="100ms"/> Wenn das ein Monster ist, bin ich auf den Bäumen, wenn es kommt. Sei mir bitte nicht böse <break time="150ms"/> wenn ich dich nicht retten kann. <break time="250ms"/> Jetzt wünsche ich dir eine gute Nacht. <audio src="https://nordicskills.de/audio/DerZauberwaldKendell/baldur_schlaeft.mp3" /> Lyria. Lyria? sieht so aus, dass du gut geschlafen hast. <break time="200ms"/> <say-as interpret-as="interjection">manometer</say-as><break time="10ms"/> ich habe mich nicht getraut zu schlafen. Ich hatte Angst das Tier aus der Höhle kommt und frisst dich auf. <break time="1s"/> Nagut. Wenn du alles zusammengepackt hast und fertig mit dem Frühstück bist, dann lass uns los. Die Höhle wartet. <break time="200ms"/> Wenn du soweit bist, sage [[Höhle betreten]]. ';
        return output;
    },
    Ignis_Lagerfeuer: function(attributes) {
        var output = '';
        output += '<audio src="https://nordicskills.de/audio/DerZauberwaldKendell/funem_lagerfeuerGR.mp3" /><say-as interpret-as="interjection">vorsicht</say-as>.<say-as interpret-as="interjection">wow</say-as> Du willst doch nicht den ganzen Wald abfackeln? <break time="250ms"/> Versuchs nochmal, aber diesmal schön sanft und mit Gefühl. <audio src="https://nordicskills.de/audio/DerZauberwaldKendell/funem_lagerfeuerKL.mp3" /><say-as interpret-as="interjection">siehste</say-as>, du kannst es doch. <audio src="https://nordicskills.de/audio/DerZauberwaldKendell/lagerfeuer.mp3" /> ';
        attributes.scene["Item_Magietrank"] -= 1;
        output += 'Du hast dreimal gezaubert, daher ist wieder Zeit für einen Magietrank. <break time="150ms"/> Nimm dir eine Phiole und trink. <break time="150ms"/> <audio src="https://nordicskills.de/audio/DerZauberwaldKendell/zaubertrank_trinken.mp3" /> Lyria, später kannst du Getränke mischen die viel stärker sind. <break time="100ms"/> Dann brauchst du erst nach fünf und noch später, erst nach dem zehnten mal, einen Trank nehmen. <break time="400ms"/> So das Feuer brennt, ';
        if (attributes.scene["Zutatenmischen"] == 1) {
            output += 'dann kannst du jetzt sagen [[Schriftrolle sechs lernen]]. ';
        } else {
            output += 'du musst deinen Lebenstrank noch herstellen. Sage [[Zutaten mischen]]. ';
        }
        return output;
    },
    Höhle_betreten: function(attributes) {
        var output = '';
        output += '<audio src="https://nordicskills.de/audio/DerZauberwaldKendell/wald_steine_wind.mp3" /> So. hier ist jetzt deine letzte Chance aufzugeben. Ich kann es verstehen, wenn du nicht da rein willst, aber wenn du dich jetzt entscheidest weiterzugehen, dann gibt es keine Rückkehr. Jedenfalls bis du deine Aufgabe erledigt hast. Entscheide dich und sage [[Ich gehe rein]] oder [[Ich gebe auf]]. ';
        return output;
    },
    Ich_gehe_rein: function(attributes) {
        var output = '';
        output += '<audio src="https://nordicskills.de/audio/DerZauberwaldKendell/hoehlebetreten.mp3" /> Lyria? Ich bin ziemlich stolz auf dich. Wenn ich darüber nachdenke das du das alles für mich machst. Und was du schon alles gelernt hast. Ich bin gespannt was noch passiert und ob wir die Truhe finden werden. <audio src="https://nordicskills.de/audio/DerZauberwaldKendell/drachenhauch.mp3" /><say-as interpret-as="interjection">ähm</say-as><break time="10ms"/>was war das? <break time="250ms"/><say-as interpret-as="interjection">oh oh</say-as><break time="10ms"/>aber egal. Da müssen wir jetzt durch. <audio src="https://nordicskills.de/audio/DerZauberwaldKendell/hoehle_weiter_laufen.mp3" /> So wohin willst du? <break time="150ms"/> nach links oder rechts? Sage dafür [[Den linken Weg]] oder [[Den rechten Weg]]. ';
        return output;
    },
    Ich_gebe_auf: function(attributes) {
        var output = '';
        output += '<audio src="https://nordicskills.de/audio/DerZauberwaldKendell/lagerfeuer.mp3" /> Setz dich ans Lagerfeuer und warte bis dich einer abholt. Ich fliege zurück. <audio src="https://nordicskills.de/audio/DerZauberwaldKendell/negatives_spiel_ende.mp3" />Da du aufgegeben hast, hast du verloren. <break time="100ms"/> Um noch einmal zu spielen, Sage Neustarten. <break time="50ms"/> Um zu beenden sage beenden oder Stop. ';
        return output;
    },
    Den_linken_Weg: function(attributes) {
        var output = '';
        output += '<audio src="https://nordicskills.de/audio/DerZauberwaldKendell/hoehle_weiter_laufen.mp3" /> Ich glaub das ist der richtige. <audio src="https://nordicskills.de/audio/DerZauberwaldKendell/hoehle_weiter_laufen.mp3" /> Schau mal, da vorn macht der Weg eine kleine Biegung nach links. <break time="400ms"/> Kann es sein, dass es dort immer dunkler wird? <break time="250ms"/> Mach doch mal deine Lampe an. Dafür hast du sie doch mitgenommen. <break time="200ms"/> Sage [[Lampe anzünden]] oder [[Nein ich brauch kein Licht]]. ';
        return output;
    },
    Den_rechten_Weg: function(attributes) {
        var output = '';
        output += '<audio src="https://nordicskills.de/audio/DerZauberwaldKendell/hoehle_weiter_laufen.mp3" /> Bist du sicher das dies die richtige Richtung ist? <break time="150ms"/> Egal. Wir gehen weiter. <audio src="https://nordicskills.de/audio/DerZauberwaldKendell/hoehle_weiter_laufen.mp3" /> Merkst du was? hier zieht es. <audio src="https://nordicskills.de/audio/DerZauberwaldKendell/hoehle_wind_leise.mp3" /><say-as interpret-as="interjection">hui</say-as>. Wenn das so weiter geht kannst du auch gleich fliegen. Aber ohne Flügel. <audio src="https://nordicskills.de/audio/DerZauberwaldKendell/hoehle_wind_lauter.mp3" /><say-as interpret-as="interjection">tja</say-as>. Weiter kommen wir nicht. Da vorne ist ein tiefer Schacht. Also zurück und in die andere Richtung. Sage [[Den linken Weg]]. ';
        return output;
    },
    Lampe_anzünden: function(attributes) {
        var output = '';
        output += '<audio src="https://nordicskills.de/audio/DerZauberwaldKendell/gaslampe.mp3" /><say-as interpret-as="interjection">ah</say-as><break time="10ms"/> jetzt können wir auch was sehen. <break time="200ms"/> Ich hasse es in Spinnweben zu fliegen. <audio src="https://nordicskills.de/audio/DerZauberwaldKendell/hoehle_weiter_laufen.mp3" /> Ich frage mich die ganze Zeit <break time="150ms"/> was, beziehungsweise wer, diese Höhle gemacht hat. Diese Gänge sind riesig. <break time="400ms"/> Und wieder eine Abzweigung. <break time="200ms"/> Naja nicht wirklich. <break time="150ms"/> Schau. Nach links gehts gar nicht weiter. Dort ist die Höhle eingestürzt. <break time="200ms"/> Dann gehts ja nur weiter rein. Sage [[Tiefer rein gehen]]. ';
        return output;
    },
    Nein_ich_brauch_kein_Licht: function(attributes) {
        var output = '';
        output += '<audio src="https://nordicskills.de/audio/DerZauberwaldKendell/hoehle_weiter_laufen.mp3" /><audio src="https://nordicskills.de/audio/DerZauberwaldKendell/kopfschlag.mp3" /> Lyria? War das dein Kopf? <audio src="https://nordicskills.de/audio/DerZauberwaldKendell/lyria_sagt_autsch.mp3" /> Na dann. Ich glaube es ist besser wir zünden die Lampe doch an. Sage dafür, [[Lampe anzünden]]. ';
        return output;
    },
    Tiefer_rein_gehen: function(attributes) {
        var output = '';
        output += '<audio src="https://nordicskills.de/audio/DerZauberwaldKendell/hoehle_gehen.mp3" /> Das scheint die Haupthöhle zu sein. <say-as interpret-as="interjection">manometer</say-as>. Ist die groß. <say-as interpret-as="interjection">ähm</say-as>. Was ist das da? <audio src="https://nordicskills.de/audio/DerZauberwaldKendell/drachenhauch.mp3" /> Das ist Tarrador <break time="250ms"/> <say-as interpret-as="interjection">was zur Hölle</say-as> macht der hier? <audio src="https://nordicskills.de/audio/DerZauberwaldKendell/tarrador_begruesst_lyria.mp3" /> Ok Lyria, dann hör mir bitte zu. <break time="150ms"/> Tarrador kam eines Tages zu mir. Bessergesagt <break time="100ms"/> er hat nach mir geschickt und ich bin natürlich zu ihm geflogen. <break time="150ms"/> Er sagte mir, dass es Zeit wird eine neue Elfin auszubilden. <break time="100ms"/> Da die Zeit reif ist und etwas Böses auf Kendell zukommt. <break time="150ms"/> Er erklärte mir das wir eine Elfin mit reinem Herzen, einem scharfen Verstand und Ausdauer im Kampf gegen das Böse brauchen. <break time="200ms"/> Darum habe ich mich auf die Suche nach einer Elfin gemacht. <break time="100ms"/> Und gefunden habe ich dich. <break time="250ms"/> Aber bevor ich dir den Rest erzähle, setz dich da hin. <break time="150ms"/> Da, auf dem Felsen. Sage [[Ich setze mich dahin]]. ';
        return output;
    },
    Ich_setze_mich_dahin: function(attributes) {
        var output = '';
        output += '<audio src="https://nordicskills.de/audio/DerZauberwaldKendell/tarrador_sagt_komm_naeher_lyria.mp3" /> Die Aufgaben die du auf den Weg hierher gelöst hast, waren alles Prüfungen. ';
        if (attributes.scene["Item_Drachenfeder"] == 1) {
            output += 'Der kleine Drache. War eine List. Eine kleine Täuschung von mir. Aber die Drachenfeder ist echt und sie wird dir noch von Nutzen sein. ';
        } else {}
        if (attributes.scene["Item_Glücksmünze"] == 1) {
            output += 'Die beiden Wichtel zum beispiel. Deren Münze, die du noch brauchen wirst. <break time="200ms"/> Sie haben uns geholfen. ';
        } else {}
        if (attributes.scene["Item_Drachenamulett"] == 1) {
            output += 'Der Wasserfall und die Höhle dahinter, waren kein Zufall. Aber das Drachenamulett Echt <break time="150ms"/> und es ist ein Zeichen, dass dich und Tarrador verbindet. ';
        } else {}
        if (attributes.scene["Mandura"] == 1) {
            output += 'Mandura die alte Eiche, die direkt in dein Herz geschaut hat. Sie hat Tarrador verraten, dass du der Richtige bist. ';
        } else {
            output += 'Mandura die alte Eiche. Sie hatte auf dich gewartet. <break time="150ms"/> Auch Sie hatte eine Aufgabe für dich. Aber es gibt bestimmt noch die Möglichkeit Sie zu sprechen. ';
        }
        output += 'Alle Aufgaben und Rätsel die du lösen musstest. <break time="200ms"/> Sie waren eine List, um zu sehen ob du der Richtige bist. <break time="150ms"/> Der richtige als der, nein unser, neuer Magier von Kendell. <break time="400ms"/> Jetzt bist Du hier und hast bewiesen das du bereit bist. <break time="200ms"/> Bereit für die letzte Aufgabe. <break time="300ms"/> Eine letzte Prüfung. <break time="200ms"/> Höre Tarrador genau zu. <audio src="https://nordicskills.de/audio/DerZauberwaldKendell/tarrador_gibt_lyria_aufgabe.mp3" /> So Lyria. Wenn du soweit bist dann sage [[Zum Abgrund gehen]]. ';
        return output;
    },
    Zum_Abgrund_gehen: function(attributes) {
        var output = '';
        if (attributes.visited.includes("Erneut versuchen")) {
            output += '<audio src="https://nordicskills.de/audio/DerZauberwaldKendell/klinge.mp3" /> OK. Dann versuch es nochmal. Denke daran nur ein Weg ist der richtige. Jetzt zeige uns was du gelernt hast. <break time="150ms"/> Wähle deinen Zauber und Sage [[Momentum Holzkiste]], [[Patentibus Holzkiste]], [[Ignis Truhe]], [[Silentium Streitaxt]] oder [[Seil Streitaxt]]. ';
        } else {
            attributes.scene["Richtig"] = 0;
            output += '<audio src="https://nordicskills.de/audio/DerZauberwaldKendell/hoehle_weiter_laufen.mp3" /> Bleib stehen. Da geht es steil runter. <break time="200ms"/> Du hast verschiedene Zauber gelernt. <break time="200ms"/> Der Momentum Zauber ist ein Bewegungszauber. <break time="250ms"/> Dann Patentibus, zum Öffnen von Schlössern. <break time="250ms"/> Ignis der Feuerzauber. <break time="250ms"/> Fuhnem der Seilzauber. <break time="250ms"/> Dann nicht zu vergessen, Silentium um Gegestände anzuhalten. <break time="150ms"/> Du siehst dort hinter dem tiefen Graben die Truhe <break time="100ms"/> und du siehst auch die Streitaxt die dort vor der Truhe Haarscharf hin und her pendelt. <audio src="https://nordicskills.de/audio/DerZauberwaldKendell/klinge.mp3" /> Deine Aufgabe ist es, die Truhe zu öffnen, den Stab Raknur dort heraus zu holen, dann lebendig und in einem Stück wieder hierher zu kommen. <break time="200ms"/> Hierfür darfst du alle Zauber benutzen. Aber nur ein <break time="150ms"/> und das merke dir <break time="200ms"/> nur ein Weg <break time="150ms"/> ist der richtige. <break time="350ms"/> Jetzt zeige uns was du gelernt hast. <break time="150ms"/> Wähle deinen Zauber und Sage [[Ignis Truhe]], [[Momentum Holzkiste]], [[Patentibus Holzkiste]], [[Silentium Streitaxt]] oder [[Seil Streitaxt]]. ';
            attributes.scene["DerAufgabenVersuch"] = 1;
        }
        return output;
    },
    Silentium_Streitaxt: function(attributes) {
        var output = '';
        if (attributes.scene["DerAufgabenVersuch"] == 1) {
            attributes.scene["Richtig"] = 1;
            output += '<audio src="https://nordicskills.de/audio/DerZauberwaldKendell/klinge_still.mp3" /> <say-as interpret-as="interjection">bravo</say-as>. Gut das war richtig und dein erster Schritt. Denke daran <break time="150ms"/> der Zauber hält nicht lange. <break time="150ms"/> Sobald er um ist, schwinkt das Beil wieder weiter. <break time="100ms"/> Und jetzt? <break time="250ms"/> Was machst du als nächstes? <break time="200ms"/> Sage [[Momentum Holzkiste]], [[Patentibus Holzkiste]], [[Ignis Truhe]] oder [[Seil Streitaxt]]. ';
        } else {
            if (attributes.scene["DerAufgabenVersuch"] == 2) {
                attributes.scene["Richtig"] = 1;
                output += 'Auf ein neues. Dein zweiter Verusch. Gut das war richtig und dein erster Schritt. Denke daran <break time="150ms"/> der Zauber hält nicht lange. <break time="150ms"/> Sobald er um ist, schwinkt das Beil wieder weiter. <break time="100ms"/> Und jetzt? <break time="250ms"/> Was machst du als nächstes? <break time="200ms"/> Sage [[Momentum Holzkiste]], [[Patentibus Holzkiste]], [[Ignis Truhe]] oder [[Seil Streitaxt]]. ';
            } else {
                if (attributes.scene["DerAufgabenVersuch"] == 3) {
                    attributes.scene["Richtig"] = 1;
                    output += 'Auf ein neues. Dein dritter und letzter Verusch. Gut das war richtig und dein erster Schritt. Denke daran <break time="150ms"/> der Zauber hält nicht lange. <break time="150ms"/> Sobald er um ist, schwinkt das Beil wieder weiter. <break time="100ms"/> Und jetzt? <break time="250ms"/> Was machst du als nächstes? <break time="200ms"/> Sage [[Momentum Holzkiste]], [[Patentibus Holzkiste]], [[Ignis Truhe]] oder [[Seil Streitaxt]]. ';
                }
            }
        }
        return output;
    },
    Seil_Streitaxt: function(attributes) {
        var output = '';
        if (attributes.scene["Richtig"] == 0) {
            output += '<audio src="https://nordicskills.de/audio/DerZauberwaldKendell/funem_zauber.mp3" /><audio src="https://nordicskills.de/audio/DerZauberwaldKendell/klinge_trifft.mp3" />Das Seil wurde von der Streitaxt zerschnitten. Das war ein Fehler. Du kannst deine Aufgabe nicht mehr erledigen. Daher hast du verloren. ';
            if (attributes.scene["Item_Lebenstrank"] > 0) {
                output += 'Wenn du willst kannst du einen Lebenstrank nehmen. Dann sage [[Erneut versuchen]]. <break time="100ms"/> Ansonsten sage Beenden oder Neustarten um nochmal zu spielen. ';
            } else {
                output += 'Da du keinen Lebenstrank mehr hast und deine verletzung so schwer sind, dass deine Wunden nicht heilen, hast du verloren. <audio src="https://nordicskills.de/audio/DerZauberwaldKendell/verlorenundtot.mp3" /> Sage Beenden oder Neustarten um nochmal zu spielen. ';
            }
        }
        if (attributes.scene["Richtig"] == 1) {
            attributes.scene["Richtig"] = 2;
            output += '<audio src="https://nordicskills.de/audio/DerZauberwaldKendell/funem_zauber.mp3" /> <say-as interpret-as="interjection">halleluja</say-as>. Genau so. Die Streitaxt ist mit dem Seil festgebunden. Jetzt kann sie nicht mehr schwingen und auch keinen mehr verletzen. Tja das war der zweite Schritt. Und jetzt? <break time="250ms"/> Was machst du als nächstes? <break time="200ms"/> Sage [[Momentum Holzkiste]], [[Patentibus Holzkiste]] oder [[Ignis Truhe]]. ';
        }
        return output;
    },
    Momentum_Holzkiste: function(attributes) {
        var output = '';
        if (attributes.scene["Richtig"] == 0 || attributes.scene["Richtig"] == 1) {
            output += '<audio src="https://nordicskills.de/audio/DerZauberwaldKendell/teleport_magier.mp3" /><say-as interpret-as="interjection">vorsicht</say-as><break time="10ms"/><audio src="https://nordicskills.de/audio/DerZauberwaldKendell/streitaxt_trifft.mp3" /> Schau mal Tarrador <break time="150ms"/> Das wars. Sie kann uns nicht mehr helfen. <break time="250ms"/> Das war ein Fehler. Du kannst deine Aufgabe nicht mehr erledigen. Daher hast du verloren. ';
            if (attributes.scene["Item_Lebenstrank"] > 0) {
                output += 'Wenn du willst kannst du einen Lebenstrank nehmen. Dann sage [[Erneut versuchen]]. <break time="100ms"/> Ansonsten sage Beenden oder Neustarten um nochmal zu spielen. ';
            } else {
                output += 'Da du keinen Lebenstrank mehr hast und deine verletzung so schwer sind, dass deine Wunden nicht heilen, hast du verloren. <audio src="https://nordicskills.de/audio/DerZauberwaldKendell/verlorenundtot.mp3" /> Sage Beenden oder Neustarten um nochmal zu spielen. ';
            }
        }
        if (attributes.scene["Richtig"] == 2) {
            attributes.scene["Richtig"] = 3;
            output += '<audio src="https://nordicskills.de/audio/DerZauberwaldKendell/teleport_magier.mp3" />Gut gemacht. Jetzt stehst du vor der Truhe. Die Streitaxt kann dir nichts anhaben. Nun musst du sie nur noch öffnen. <break time="150ms"/> ';
            attributes.scene["Item_Magietrank"] -= 1;
            output += 'Aber bevor du wieder Zaubern kannst, musst du deine Energie wieder auffüllen. <break time="150ms"/> Dafür musst du einen Magietrank nehmen. <break time="100ms"/> Nimm dir eine Phiole und trink. <break time="150ms"/> <audio src="https://nordicskills.de/audio/DerZauberwaldKendell/zaubertrank_trinken.mp3" /> Ok. Und wie geht es weiter? Sage  [[Patentibus Holzkiste]] oder [[Ignis Truhe]]. ';
        }
        return output;
    },
    Patentibus_Holzkiste: function(attributes) {
        var output = '';
        if (attributes.scene["Richtig"] == 3) {
            output += '<audio src="https://nordicskills.de/audio/DerZauberwaldKendell/zauber_spruch.mp3" /><audio src="https://nordicskills.de/audio/DerZauberwaldKendell/truhe_schloss_auf.mp3" /><say-as interpret-as="interjection">hurra</say-as>. Schau hin. <audio src="https://nordicskills.de/audio/DerZauberwaldKendell/racnur_in_truhe.mp3" /> Lyria <break time="150ms"/> du hast es geschafft. <break time="200ms"/> Die Truhe ist auf und du kannst dir jetzt Aurora nehmen. <audio src="https://nordicskills.de/audio/DerZauberwaldKendell/racnur_aus_truhe.mp3" /> Hörzu. Du nimmst ihn in die Hand <break time="100ms"/> und kreist den Bogen rechts herum über deinem Kopf. <break time="200ms"/> So geht die Macht des Bogens in dir über. <break time="100ms"/> erschrecke nicht, es passiert dir nichts. <break time="100ms"/> Denn Aurora, hat dich bereits auserwählt. <break time="400ms"/> Dann fang an. Aber rechts herum. <audio src="https://nordicskills.de/audio/DerZauberwaldKendell/raknur_macht_uebernahme.mp3" /> <say-as interpret-as="interjection">wow</say-as> Das sah beeindruckend aus. <break time="400ms"/> Und jetzt komm zu uns zurück. Dafür musst du nur [[Momentum Felsen]] sagen. ';
        } else {
            output += '<audio src="https://nordicskills.de/audio/DerZauberwaldKendell/zauber_spruch.mp3" /><audio src="https://nordicskills.de/audio/DerZauberwaldKendell/truhe_schloss_auf.mp3" />Du hast die Truhe geöffnet. Aber wie willst du denn von hier an den Stab kommen? <break time="300ms"/> Schau hin <break time="10ms"/> die Truhe schließt sich wieder. <audio src="https://nordicskills.de/audio/DerZauberwaldKendell/truhe_schliesst.mp3" /> Jetzt sage [[Momentum Holzkiste]], [[Ignis Truhe]], [[Silentium Streitaxt]] oder [[Seil Streitaxt]]. ';
        }
        output += ' ';
        return output;
    },
    Momentum_Felsen: function(attributes) {
        var output = '';
        output += '<audio src="https://nordicskills.de/audio/DerZauberwaldKendell/teleport_magier.mp3" /> Da bist du wieder. <break time="150ms"/> Komm zu Tarrador, er will mit dir reden. <break time="200ms"/> <audio src="https://nordicskills.de/audio/DerZauberwaldKendell/tarrador_lyria_aurora.mp3" /> Weißt du was Lyria? Ich habe gewusst das du es schaffst. <break time="150ms"/> Ich bin so froh, dass wir dich gefunden haben. <break time="200ms"/> Du hast alle Aufgaben gemeistert und bewiesen, dass du eine wirklich große Elfin geworden bist. <break time="250ms"/> Aurora hat eine würdige Besitzerin gefunden. Aber leider haben wir keine Zeit das zu feiern. Denn Kendell wartet auf dich <break time="150ms"/> und Tarrador hat es ja gerade gesagt, du hast viele neue Aufgaben zu erledigen. <break time="150ms"/> Aber erst einmal lass uns hier raus. Tarrador geht vor und wartet auf uns vorm Höhleneingang. <break time="100ms"/> Dann lass uns los und sage [[Zum Ausgang]]. ';
        return output;
    },
    Ignis_Truhe: function(attributes) {
        var output = '';
        output += '<audio src="https://nordicskills.de/audio/DerZauberwaldKendell/funem_lagerfeuerKL.mp3" /> Tja das wars die Truhe brennt vor sich hin. <audio src="https://nordicskills.de/audio/DerZauberwaldKendell/truhe_brennt.mp3" /> Das war ein Fehler. Du kannst deine Aufgabe nicht mehr erledigen. Daher hast du verloren. ';
        if (attributes.scene["Item_Lebenstrank"] > 0) {
            output += 'Wenn du willst kannst du einen Lebenstrank nehmen. Dann sage [[Erneut versuchen]]. <break time="100ms"/> Ansonsten sage Beenden oder Neustarten um nochmal zu spielen. ';
        } else {
            output += 'Da du keinen Lebenstrank mehr hast und der Bogen mit der Truhe verbrannt ist, hast du verloren. <audio src="https://nordicskills.de/audio/DerZauberwaldKendell/verlorenundtot.mp3" /> Um noch einmal zu spielen, Sage Neustarten. <break time="50ms"/> Um zu beenden sage beenden oder Stop. ';
        }
        return output;
    },
    Erneut_versuchen: function(attributes) {
        var output = '';
        attributes.scene["Item_Lebenstrank"] -= 1;
        attributes.scene["Richtig"] = 0;
        output += '<audio src="https://nordicskills.de/audio/DerZauberwaldKendell/zaubertrank_trinken.mp3" /> ';
        output += 'DU hast glück gehabt das du noch einen Lebenstrank hattest. Jetzt hast du noch ' + attributes.scene["Item_Lebenstrank"] + ' Lebenstrank. Zum weiterspielen, sage [[Zum Abgrund gehen]]. ';
        return output;
    },
    Zum_Ausgang: function(attributes) {
        var output = '';
        output += '<audio src="https://nordicskills.de/audio/DerZauberwaldKendell/hoehlebetreten.mp3" /> Lyria ich werde mich gleich von dir verabschieden, aber keine Angst wir werden uns wiedersehen. <break time="200ms"/> Tarrador wird dir gleich noch Kendell von oben Zeigen. <break time="150ms"/> Ich wünsche dir viel Spaß beim Fliegen. Und bis Bald. <audio src="https://nordicskills.de/audio/DerZauberwaldKendell/feen_fluegelschlag.mp3" /> Sage [[Auf wiedersehen]]. ';
        return output;
    },
    Auf_wiedersehen: function(attributes) {
        var output = '';
        output += '<audio src="https://nordicskills.de/audio/DerZauberwaldKendell/wald_steine_wind.mp3" /><audio src="https://nordicskills.de/audio/DerZauberwaldKendell/feen_fluegelschlag.mp3" /> Ach Lyria. <break time="100ms"/> Ich habe ja noch was vergessen. <break time="200ms"/> Und das ist auch für dich. Du, vor dem AMAZON Gerät. <break time="200ms"/> Vielen Dank das du mit mir gespielt hast. Bis zum nächsten Mal. Auf Wiederhören. <break time="500ms"/> <audio src="https://nordicskills.de/audio/DerZauberwaldKendell/terrador_ruecken_lyria.mp3" /> Du willst wissen wie es weitergeht? Welche Abenteuer Lyria noch zu erwarten hat? Tja, dass kannst du in der Fortsetzung von <break time="100ms"/> Der Zauberwald Kendell <break time="100ms"/> erfahren. <break time="300ms"/> Fortsetzung demnächst. <break time="500ms"/> Um noch einmal zu spielen, Sage Neustarten. <break time="50ms"/> Um zu beenden sage beenden oder Stop. ';
        return output;
    },
}