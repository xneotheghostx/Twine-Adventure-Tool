module.exports = {
    Start: function(attributes) {
        var output = '';
        output += 'Hallo Steve, hier ist dein erster Auftrag. <break time="150ms"/> "Der Adel". <break time="100ms"/><audio src="https://nordicskills.de/audio/MeinAuftrag/schnarchen.mp3" /> Du warst gerade dabei ein kleines Nickerchen zu machen, als das klingeln des Telefons dich geweckt hat.<audio src="https://nordicskills.de/audio/MeinAuftrag/telefon.mp3" /> <audio src="https://nordicskills.de/audio/MeinAuftrag/frauthomson.mp3" /> OK Steve, wenn du wach bist sage [[Anfangen]]. ';
        return output;
    },
    Anfangen: function(attributes) {
        var output = '';
        output += 'Der Anruf gerade, kam von einer Alten Dame mit Namen Thomson. <break time="100ms"/> Sie ist etwas schräg und nervig, aber sie hat Geld, deshalb magst du Sie. <break time="150ms"/> Das mit dem Auftrag für dich ist gut, denn du bist mal wieder knapp bei Kasse. <break time="100ms"/> Sie erwartet dich in einer Stunde bei sich zuhause. Steve vergiss deinen Block und Stift nicht und kämm dir vorher noch die Haare. <break time="100ms"/> Hast du alles dabei? Wenn ja und du soweit bist sage [[Auf nach London]]. ';
        return output;
    },
    Auf_nach_London: function(attributes) {
        var output = '';
        output += 'Die Tür fällt hinter dir ins schloss <audio src="https://nordicskills.de/audio/MeinAuftrag/tuerzu.mp3" /><say-as interpret-as="interjection">ist nicht dein ernst.</say-as> Steve, du hast schon wieder nicht abgeschlossen.<break time="100ms"/> Wenn ich dich nicht an alles erinnere <break time="150ms"/><say-as interpret-as="interjection">oh mann</say-as> <audio src="https://nordicskills.de/audio/MeinAuftrag/tuerabschliessen.mp3" /> so ab nach unten auf die Strasse <audio src="https://nordicskills.de/audio/MeinAuftrag/trepperunter.mp3" /> <say-as interpret-as="interjection">Vorsicht.</say-as> <audio src="https://nordicskills.de/audio/MeinAuftrag/fahradklingel.mp3" /> Das war knapp. <break time="100ms"/>Da ich keine lust habe zu laufen, besorge ich uns ein Taxi. <audio src="https://nordicskills.de/audio/MeinAuftrag/taxi_pfeifen.mp3" /> <break time="350ms"/> Das Taxi steht vor dir. Zum losfahren sage [[Einsteigen]]. ';
        return output;
    },
    Einsteigen: function(attributes) {
        var output = '';
        output += 'Nach dem du dem Fahrer dein Ziel gesagt hast, fährt er los. <audio src="https://nordicskills.de/audio/MeinAuftrag/autostarten.mp3" /> Während der Fahrt spricht der Fahrer dich an. Er erklärt dir, das der Enkel von Frau Thomson Tot ist. Es soll nach Polizeiangaben ein Unfall gewesen sein. <break time="100ms"/> Ob das stimmt ? <break time="100ms"/> Vielleicht ist das dein neuer Auftrag. <break time="200ms"/> An der Villa angekommen bezahlst du und steigst aus. <audio src="https://nordicskills.de/audio/MeinAuftrag/autotuerzu.mp3" /> Auf der Quittung vom Taxifahrer steht Handgeschrieben eine Zwanzig. <break time="200ms"/> Wieso Zwanzig? Steve, ich glaube das ist wichtig. <break time="500ms"/> Und vergesse nicht das Frau Thomson auf dich wartet, wir sollten zu ihr gehen.<audio src="https://nordicskills.de/audio/MeinAuftrag/fussweg.mp3" /> Du stehst vor Ihrer Tür. <break time="200ms"/> Wenn du soweit bist sage [[Anklopfen]]. ';
        return output;
    },
    Anklopfen: function(attributes) {
        var output = '';
        output += '<audio src="https://nordicskills.de/audio/MeinAuftrag/anklopfen.mp3" /> Der <phoneme alphabet="ipa" ph="ˈbatlɐ">Butler</phoneme> des Hauses öffnet  die Tür. <break time="200ms"/> Da wir erwartet werden, führt er uns in den Salon. <audio src="https://nordicskills.de/audio/MeinAuftrag/salongbetreten.mp3" /> Mrs. Thomson kommt herein und begrüßt uns. Sie erzählt von Ihrem Neffen, das er Tot ist, das die Polizei von einem Unfall spricht. Sie aber glaubt das es ein Mord war. <break time="200ms"/> Du bekommst den Auftrag es zu beweisen. Sie redet unendlich viel, vieles ist belanglos, bis auf das Notizbuch von Noah. <break time="150ms"/> Sie gibt es dir und du steckst es ein. <break time="300ms"/> Sie möchte das du nach Liverpool fährst, denn dort ist die Wohnung von Noah. Vielleicht findest du dort Beweise für die Vermutungen von Mrs. Thomson. <break time="200ms"/> Nachdem du dir alles von ihr angehört hast, versprichst du Ihr zu helfen und verabschiedest dich. <break time="300ms"/> Steve, wir sollten uns auf den Weg zum Bahnhof machen. Ich habe uns schon ein Taxi gerufen.<audio src="https://nordicskills.de/audio/MeinAuftrag/taxihupe.mp3" /> OK Steve, lass uns gehen, wir müssen nach Liverpool. <break time="200ms"/> Wenn du soweit bist, sage [[Villa verlassen]]. ';
        return output;
    },
    Villa_verlassen: function(attributes) {
        var output = '';
        output += 'Am Bahnhof angekommen <break time="100ms"/> machst du dich auf den Weg zum Fahrkartenautomaten. <audio src="https://nordicskills.de/audio/MeinAuftrag/bahnhof.mp3" /> Wohin willst du fahren? <break time="100ms"/> Da wir ja in der Wohnung von Noah anfangen wollen, sage [[Nach Liverpool]]. ';
        return output;
    },
    Nach_Liverpool: function(attributes) {
        var output = '';
        output += 'Im Zug angekommen <break time="100ms"/>nimmst du dir das Notizbuch von Noah und schaust es dir während der Fahrt genauer an. <audio src="https://nordicskills.de/audio/MeinAuftrag/zugfahrt.mp3" /> <voice name="Hans">Komisch</voice>. <break time="250ms"/> Was meinst du Steve? Dir fällt immer wieder eine Zahl auf, die Seitenzahl, die Hausnummer und die Uhrzeit zu der sich Noah mit seinem Onkel William Thomson in Blackpool treffen wollte. Immer wieder die Neunzehn. <say-as interpret-as="interjection">Echt Komisch</say-as>. Steve, du solltest dir diese Zahl merken oder besser noch aufschreiben. <break time="500ms"/> Wir sind in Liverpool angekommen. Jetzt müssen wir in die Mathew Street, dort ist die Wohnung von Noah. <audio src="https://nordicskills.de/audio/MeinAuftrag/zugverlassen.mp3" /> Steve, da vorne ist das Haus mit der Hausnummer dreiundzwanzig <break time="200ms"/> schon wieder eine Zahl. <break time="400ms"/> In dem Wohnhaus hatte Noah doch seine Wohnung. Lass uns hinein gehen und schauen was wir dort finden. Wenn du soweit bist, sage [[Haus betreten]]. ';
        return output;
    },
    Haus_betreten: function(attributes) {
        var output = '';
        output += 'Steve, hast du die Nachbarin gesehen? <break time="150ms"/> Die weiß doch bestimmt irgend etwas <break time="100ms"/> oder was meinst du ? <break time="100ms"/> Willst du die Nachbarin befragen <break time="10ms"/> oder nach oben in den ersten Stock. Sage [[Nachbarin befragen]] oder [[In Noahs Wohnung]]. ';
        return output;
    },
    Nachbarin_befragen: function(attributes) {
        var output = '';
        output += 'Die Nachbarin knallt dir die Tür vor der Nase zu <audio src="https://nordicskills.de/audio/MeinAuftrag/tuerzuknallen.mp3" /> <say-as interpret-as="interjection">wow</say-as> hat die schlechte Laune. Ich glaube das bringt nichts, lass uns zurück gehen. ';
        if (attributes.visited.includes("In Noahs Wohnung")) {
            output += 'In dem Zimmer von Noah waren wir ja schon. Dann lass uns das Wohnhaus verlassen. Dafür sage [[Wohnhaus verlassen]] ';
        } else {
            output += 'Vergesse nicht noch in Noah´s Wohnung zu gehen. Dafür sage [[In Noahs Wohnung]]. ';
        }
        return output;
    },
    In_Noahs_Wohnung: function(attributes) {
        var output = '';
        output += '<audio src="https://nordicskills.de/audio/MeinAuftrag/treppenstuffen.mp3" /> Steve, du hast den Schlüssel von Frau Thomson bekommen. Schließt du bitte die Tür auf <audio src="https://nordicskills.de/audio/MeinAuftrag/schluesselbund.mp3" /> ';
        output += 'Du durchsuchst die ganze Wohnung <audio src="https://nordicskills.de/audio/MeinAuftrag/schubladeauf.mp3" /> alle Schränke, jede Schublade und jeden Winkel darin. Nichts, absolut nichts, bis auf den Zettel an der Pinnwand. Dort steht <break time="200ms"/> Vergesse nicht die Notiz auf Seite Achtzehn. Steve, gib mir mal das Notizbuch von Noah. Ich will wissen was auf der Seite Achtzehn steht. <audio src="https://nordicskills.de/audio/MeinAuftrag/notizblockblaettern.mp3" /> <amazon:effect name="whispered">sechzehn. siebzehn, achtzehn.</amazon:effect> Seite achtzehn, da ist sie. <break time="400ms"/><say-as interpret-as="interjection">Na sieh mal einer an.</say-as>Steve, wir müssen nach Sheffield. Dort ist das Elternhaus von Noah. Sieht so aus, als kommt noch eine menge Arbeit auf uns zu. Ok Steve, lass uns zurück zum Bahnhof <break time="200ms"/> und auf dem Weg dahin, möchte ich was Essen. Ich habe Hunger. Sage [[Wohnhaus verlassen]]. ';
        return output;
    },
    Wohnhaus_verlassen: function(attributes) {
        var output = '';
        output += 'Steve, da vorne ist ein Restaurant und der Bahnhof. Wo willst du jetzt hingehen? Sage [[Nach Sheffield]] oder [[Ins Restaurant]]. ';
        return output;
    },
    Nach_Sheffield: function(attributes) {
        var output = '';
        if (attributes.visited.includes("Ins Restaurant")) {
            output += 'Danke Steve für die Einladung. Nach dem Essen geht es mir schon viel besser. <audio src="https://nordicskills.de/audio/MeinAuftrag/zugfahrt.mp3" /> So Steve, pass auf. Ich habe im Notizbuch auf Seite Achtzehn gelesen, das Noahs Eltern und sein Onkel, schon seit Jahren zerstritten sind. Leider stand im Buch nichts darüber warum. <break time="200ms"/> Aber wir fahren jetzt nach Sheffield um das heraus zubekommen. <break time="250ms"/> In an paar Minuten sind wir dort. <break time="450ms"/> Ich bestelle uns schon mal ein Taxi. <audio src="https://nordicskills.de/audio/MeinAuftrag/bahnhof.mp3" /> So wir sind da <break time="200ms"/> das Taxi wartet. Wenn du soweit bist können wir weiter, dafür sage [[In das Taxi Einsteigen]]. ';
        } else {
            output += 'Danke das ich jetzt mit knurendem Magen weiter gehen darf. <audio src="https://nordicskills.de/audio/MeinAuftrag/magenknurren.mp3" /> <break time="300ms"/> Schau mich nicht so an. <break time="100ms"/> Und ja. Ich habe Schlechtelaune. <audio src="https://nordicskills.de/audio/MeinAuftrag/zugfahrt.mp3" /> So Steve, pass auf. Ich habe im Notizbuch auf Seite Achtzehn gelesen, das Noahs Eltern und sein Onkel, schon seit Jahren zerstritten sind. Leider stand im Buch nichts darüber warum. <break time="200ms"/> Aber wir fahren jetzt nach Sheffield um das heraus zubekommen. <break time="250ms"/> In an paar Minuten sind wir dort. <break time="450ms"/> Ich bestelle uns schon mal ein Taxi. <audio src="https://nordicskills.de/audio/MeinAuftrag/bahnhof.mp3" /> So wir sind da <break time="200ms"/> das Taxi wartet. Wenn du soweit bist können wir weiter, dafür sage [[In das Taxi Einsteigen]]. ';
        }
        return output;
    },
    Ins_Restaurant: function(attributes) {
        var output = '';
        output += '<audio src="https://nordicskills.de/audio/MeinAuftrag/restaurant.mp3" /> Steve, wir fahren gleich zu Noahs Elternhaus. Eines musst du noch wissen bevor wir ankommen. <break time="100ms"/> Noahs Eltern sind vor einem Jahr bei einem Verkehrsunfall gestorben. <break time="100ms"/> Ja ich weis, es klingt komisch, finde ich auch. Aber ich wollte das du es vorher weist, bevor es im Haus sonst peinlich werden könnte.<break time="1s"/> So wenn du fertig bist darfst du bezahlen und lass dir eine Quittung geben. Schließlich muss ich nachher noch die Abrechnung machen. <break time="1s"/> Komm schon, wir müssen zum Bahnhof. Dafür sage [[Nach Sheffield]]. ';
        return output;
    },
    In_das_Taxi_Einsteigen: function(attributes) {
        var output = '';
        if (attributes.visited.includes("Ins Restaurant")) {
            output += '<audio src="https://nordicskills.de/audio/MeinAuftrag/autotuerzu.mp3" /> Steve, bitte vergiss nicht, was ich dir beim Essen gesagt habe.<audio src="https://nordicskills.de/audio/MeinAuftrag/autostarten.mp3" /> Das Personal erwatet uns <break time="50ms"/> denn Frau Thomson hat uns angekündigt. <break time="1s"/> Ok, wir sind da, lass uns zur Tür gehen. <audio src="https://nordicskills.de/audio/MeinAuftrag/fussweg.mp3" /> Wenn du soweit bist sage [[Klingel benutzen]]. ';
        } else {
            output += '<audio src="https://nordicskills.de/audio/MeinAuftrag/autotuerzu.mp3" /> ';
            output += '<audio src="https://nordicskills.de/audio/MeinAuftrag/magenknurren.mp3" /><say-as interpret-as="interjection">oh mann</say-as> habe ich einen hunger.<audio src="https://nordicskills.de/audio/MeinAuftrag/autostarten.mp3" /> Das Personal erwatet uns <break time="50ms"/> denn Frau Thomson hat uns angekündigt. <break time="1s"/> Ok, wir sind da, lass uns zur Tür gehen. <audio src="https://nordicskills.de/audio/MeinAuftrag/fussweg.mp3" /> Wenn du soweit bist sage [[Klingel benutzen]]. ';
        }
        return output;
    },
    Klingel_benutzen: function(attributes) {
        var output = '';
        output += '<audio src="https://nordicskills.de/audio/MeinAuftrag/klingel.mp3" /> Die Hausdame öffnet uns die Tür <break time="20ms"/> und bittet uns herein. <audio src="https://nordicskills.de/audio/MeinAuftrag/salongbetreten.mp3" /> Sie begleitet uns in die Bibliothek und erklärt dir, das Noah nach dem Tödlichem Autounfall seiner Eltern, nach Liverpool gezogen ist. <break time="150ms"/>Er wollte hier nicht mehr wohnen. <break time="250ms"/>Weiterhin erzählt Sie, das der Onkel William und Noah vor Zehn tagen einen Riesenstreit hatten. <audio src="https://nordicskills.de/audio/MeinAuftrag/klingel.mp3" /> Die Hausdame geht zur Tür. <break time="50ms"/>Steve, wenn Sie gleich zurück kommt, frage sie doch nach William <break time="50ms"/> oder Sie soll uns noch was erzählen was uns weiterhilft. <break time="300ms"/> <emphasis level="strong">pscht</emphasis> <break time="100ms"/> Sie kommt wieder. <audio src="https://nordicskills.de/audio/MeinAuftrag/salongbetreten.mp3" /> Steve, lass uns dort hingehen. Sage [[zum Bücherregal]]. ';
        return output;
    },
    zum_Bücherregal: function(attributes) {
        var output = '';
        output += 'Hier können wir besser reden.<break time="150ms"/> Ich hole die Haushälterin einmal hierher. Dann kannst du Sie was fragen. <audio src="https://nordicskills.de/audio/MeinAuftrag/salongbetreten.mp3" /> Und was willst du von ihr wissen? Du kannst sagen [[Wo ist William Thomson]], [[Was ist mit Noah passiert]] oder [[Wer war das an der Tür]]. ';
        return output;
    },
    Was_ist_mit_Noah_passiert: function(attributes) {
        var output = '';
        if (attributes.scene["WasIstMitNoah"] == 1) {
            output += 'Die Hausdame schau dich verwundert an und sagt, <voice name="Marlene">das habe ich doch schon erzählt</voice>. <break time="100ms"/> Wenn du willst kannst du sie noch was anderes fragen. Dann sage [[Wer war das an der Tür]], [[Wo ist William Thomson]] oder [[Haus verlassen]]. ';
        } else {
            attributes.scene["WasIstMitNoah"] = 1;
            output += 'Die Hausdame erklärt dir, das Sie für die Familie Thomsons schon seit sechzehn Jahren arbeitet. Sie sagt, das Sie nicht glaubt, das es ein Unfall war. Weder der Tot der Eltern noch Noahs <break time="100ms"/> und Sie hofft das wir diese schrecklichen Ereignisse aufklären. <break time="1s"/> Steve, schon wieder eine Zahl <break time="250ms"/> diesmal ist es die sechzehn. <break time="100ms"/> Wenn du willst kannst du sie noch was anderes fragen. Dann sage [[Wer war das an der Tür]], [[Wo ist William Thomson]] oder [[Haus verlassen]]. ';
        }
        return output;
    },
    Haus_verlassen: function(attributes) {
        var output = '';
        output += '<say-as interpret-as="interjection">Komm schon.</say-as> Lass und zum Bahnhof und dann nach Blackpool. Wir sollten uns mal mit dem lieben Onkel William unterhalten. Ich bin ja mal gespannt, was das für einer ist. <audio src="https://nordicskills.de/audio/MeinAuftrag/aussen_schritte.mp3" /> So, da ist der Bahnhof. <audio src="https://nordicskills.de/audio/MeinAuftrag/bahnhof.mp3" /> Wenn du soweit bist lass uns einsteigen. Sage [[Nach Blackpool]]. ';
        return output;
    },
    Wer_war_das_an_der_Tür: function(attributes) {
        var output = '';
        if (attributes.scene["WerWarDasAnDerTür"] == 1) {
            output += 'Die Hausdame schaut dich verwundert an und sagt, <voice name="Marlene">das habe ich doch schon gesagt</voice>. <break time="200ms"/> <say-as interpret-as="interjection">wow</say-as> was ist die zickig. Wenn du willst kannst du sie noch was anderes fragen. Dann sage [[Wo ist William Thomson]], [[Was ist mit Noah passiert]] oder [[Haus verlassen]]. ';
        } else {
            attributes.scene["WerWarDasAnDerTür"] = 1;
            output += 'Du fragst die Hausdame wer das gerade an der Tür war. Aber ihre Antwort war <say-as interpret-as="interjection">echt komisch</say-as>. Sie sagte nur einen Satz <break time="1s"/> <voice name="Marlene">niemand</voice><break time="1s"/> <say-as interpret-as="interjection">mal ehrlich</say-as>. Dafür war Sie aber ziemlich lange weck. Oder? <break time="200ms"/> Wenn du willst kannst du sie noch etwas anderes fragen. Dann sage [[Wo ist William Thomson]], [[Was ist mit Noah passiert]] oder [[Haus verlassen]]. ';
        }
        return output;
    },
    Wo_ist_William_Thomson: function(attributes) {
        var output = '';
        if (attributes.scene["WilliamThomson"] == 1) {
            output += 'Die Hausdame schaut dich verwundert an und sagt, <voice name="Marlene">das habe ich doch schon erzählt</voice>. <break time="100ms"/> Wenn du willst kannst du sie noch was anderes fragen. Dann sage [[Was ist mit Noah passiert]], [[Wer war das an der Tür]] oder [[Haus verlassen]]. ';
        } else {
            attributes.scene["WilliamThomson"] = 1;
            output += 'Die Hausdame erklärt dir, das William ein Haus in Blackpool hat, gibt dir die Adresse und warnt uns. <break time="100ms"/> Sie meint das man William nicht trauen kann und das, wenn wir auf ihn treffen, wir uns lieber in acht nehmen sollen. <break time="100ms"/> Wenn du willst kannst du sie noch was anderes fragen. Dann sage [[Was ist mit Noah passiert]], [[Wer war das an der Tür]] oder [[Haus verlassen]]. ';
        }
        return output;
    },
    Nach_Blackpool: function(attributes) {
        var output = '';
        output += 'Steve, wir haben jetzt eine kleine Pause. Ich werde mir das Notizbuch noch mal anschauen und sehen ob ich noch was Interessantes finde.<audio src="https://nordicskills.de/audio/MeinAuftrag/notizblockblaettern.mp3" /> <say-as interpret-as="interjection">hört hört</say-as> <break time="500ms"/> <say-as interpret-as="interjection">oh oh</say-as> <break time="500ms"/> ';
        output += '<say-as interpret-as="interjection">oh nein</say-as> <break time="500ms"/> Steve, Noah schreibt in seinem Notizbuch, das der streit zwischen den Brüdern William und Hennry schon über 20 Jahre geht. Aber auch hier steht kein Grund für den Streit.<audio src="https://nordicskills.de/audio/MeinAuftrag/zugverlassen.mp3" /><say-as interpret-as="interjection">ach</say-as> Wir sind ja schon da. Gut, dann lass uns aussteigen und William Thomson aufsuchen. Sage [[Aussteigen]]. ';
        return output;
    },
    Aussteigen: function(attributes) {
        var output = '';
        output += '<prosody pitch="low">Kurze zeit später</prosody> <audio src="https://nordicskills.de/audio/MeinAuftrag/klpause.mp3" /> Da vorne ist das Haus von William Thomson. <break time="100ms"/> Es sieht nicht gerade einladend aus.<audio src="https://nordicskills.de/audio/MeinAuftrag/aussen_schritte.mp3" /> Bleib mal stehen. Sieh dir das mal an. <break time="250ms"/> Kann es sein, das ihm das Geld ausgegangen ist? <break time="100ms"/> Da ist die Eingangstür. <audio src="https://nordicskills.de/audio/MeinAuftrag/aussen_schritte.mp3" /> Sage [[Zum Eingang]]. ';
        return output;
    },
    Zum_Eingang: function(attributes) {
        var output = '';
        output += '<audio src="https://nordicskills.de/audio/MeinAuftrag/fussweg.mp3" /> Du stehst vor der Tür. Ich hoffe der hat keinen Hund. Ich erinnre dich nur ungern daran, was bei deinem letzten Klingelstreich passiert ist. <break time="250ms"/> Steve, was machen wir jetzt? Sage [[Nach rechts]], [[Die Klingel benutzen]], oder [[Nach links]]. ';
        return output;
    },
    Nach_rechts: function(attributes) {
        var output = '';
        output += 'Hinter dem Haus siehst du ein Gartenhaus. <audio src="https://nordicskills.de/audio/MeinAuftrag/fussweg.mp3" /> Hier sieht es aus als wenn der Gärtner lange nichts mehr getan hat. <break time="100ms"/> Alles vertrocknet und ungepflegt. <break time="100ms"/> Hier macht es keinen sinn sich länger umzuschauen. <break time="250ms"/> Lass uns was anderes versuchen. <break time="100ms"/> Warte mal.<break time="250ms"/> Ich habe vorhin neben dem Eingang noch eine Kellertreppe gesehen. Lass uns zurück gehen und dann die Treppe runter. Sage [[Die Treppe runter]] ';
        return output;
    },
    Nach_links: function(attributes) {
        var output = '';
        output += '<audio src="https://nordicskills.de/audio/MeinAuftrag/fussweg.mp3" /> ';
        if (attributes.scene["NachLinks"] == 1) {
            output += 'Ich glaube nicht das wir Zeit haben, um die Fenster zuputzen. Warum bist du denn schon wieder hier. Die Blumen sind immer noch vertrocknet. Lass uns was anderes versuchen. Sage [[Nach rechts]] oder [[Die Klingel benutzen]]. ';
        } else {
            attributes.scene["NachLinks"] = 1;
            output += 'Du stehst vor einem Fenster und kannst zwischen dem Vorhang durch sehen. Du kannst nichts besonderes sehen, aber dir fällt auf, das auf dem Tisch eine Vase mit einem vertrockneten Blumenstrauß steht. Steve, hier war schon länger keiner mehr. Lass uns was anderes versuchen. Sage [[Nach rechts]] oder [[Die Klingel benutzen]]. ';
        }
        output += ' ';
        output += ' ';
        return output;
    },
    Die_Klingel_benutzen: function(attributes) {
        var output = '';
        output += '<audio src="https://nordicskills.de/audio/MeinAuftrag/fussweg.mp3" /> ';
        if (attributes.scene["Geklingelt"] == 1) {
            output += 'Ich habe doch gerade geklingelt. Da ebend keiner aufgemacht hat, wird es jetzt auch keiner machen. Lass uns zurück gehen und was anderes versuchen. Sage [[Nach links]] oder [[Nach rechts]]. ';
        } else {
            attributes.scene["Geklingelt"] = 1;
            output += 'Steve, schau mal auf den Postkasten. Den hat auch schon lange keiner mehr geleert. <break time="100ms"/> Ich klingel mal. ';
            output += '<audio src="https://nordicskills.de/audio/MeinAuftrag/tuerklingel.mp3" /> <break time="2s"/> <audio src="https://nordicskills.de/audio/MeinAuftrag/tuerklingel.mp3" /> <break time="2s"/> <say-as interpret-as="interjection">Schon klar.</say-as> Hätte mich auch gewundert. So wie das hier aussieht. <break time="100ms"/> Lass uns zurück gehen und was anderes versuchen. Sage [[Nach links]] oder [[Nach rechts]]. ';
        }
        return output;
    },
    Die_Treppe_runter: function(attributes) {
        var output = '';
        output += '<audio src="https://nordicskills.de/audio/MeinAuftrag/fussweg.mp3" /> Die Tür lässt sich nur sehr schwer öffnen. <audio src="https://nordicskills.de/audio/MeinAuftrag/kellertuer.mp3" /> <say-as interpret-as="interjection">vorsicht</say-as>, hier ist es dunkel <break time="10ms"/> und lass bloß die Tür auf. <break time="300ms"/> Was ist? <break time="250ms"/> Willst du weiter gehen? Wenn ja dann sage [[Weiter gehen]]. ';
        return output;
    },
    Weiter_gehen: function(attributes) {
        var output = '';
        output += 'Steve, schau mal am ende des Gangs steht ein kleiner Schrank, lass uns den durchsuchen.<audio src="https://nordicskills.de/audio/MeinAuftrag/kellerschritte.mp3" /> Mach auf <audio src="https://nordicskills.de/audio/MeinAuftrag/schubladeauf.mp3" /> <say-as interpret-as="interjection">Bingo</say-as> <break time="200ms"/> Da haben wir doch was. Eine Schatulle. ';
        output += '<audio src="https://nordicskills.de/audio/MeinAuftrag/kellertuerzu.mp3" /> <say-as interpret-as="interjection">oh oh</say-as>. Die Tür ist zu gefallen. <break time="400ms"/> Ich hoffe du hast dein Schlüsselbund dabei, das mit der Taschenlampe daran. <break time="300ms"/> <audio src="https://nordicskills.de/audio/MeinAuftrag/taschenlampe.mp3" /> <say-as interpret-as="interjection">halleluja</say-as> <break time="300ms"/> du bist der beste. <break time="300ms"/> Aber die Tür ist zu <break time="150ms"/> und der Griff ist kaputt.<break time="250ms"/> Tja und jetzt? <break time="200ms"/> Warte, da vorne ist noch eine zweite Tür. <audio src="https://nordicskills.de/audio/MeinAuftrag/kellerschritte.mp3" /> <break time="250ms"/> Ich hab die Schatulle, lass uns weiter. Sage [[Tür aufmachen]] ';
        return output;
    },
    Tür_aufmachen: function(attributes) {
        var output = '';
        output += '<say-as interpret-as="interjection">abrakadabra</say-as><audio src="https://nordicskills.de/audio/MeinAuftrag/kellertuer.mp3" /><say-as interpret-as="interjection">voila</say-as> und die Tür ist auf. <break time="250ms"/>Ich liebe unverschlossene Türen. <break time="250ms"/> Ok sage [[weiter gehts]]. ';
        return output;
    },
    weiter_gehts: function(attributes) {
        var output = '';
        output += 'Schau mal, hier ist ein langer Kellergang <break time="200ms"/> den müssen wir entlang gehen. <audio src="https://nordicskills.de/audio/MeinAuftrag/kellerschritte_laut.mp3" /> Da, am Ende ist eine Treppe die nach oben führt. Die uns dann hoffentlich hier raus bringt. <break time="300ms"/> Das stinkt hier und mir kribbelt es überall. Wenn wir hier raus sind brauche ich eine Dusche. Steve, <say-as interpret-as="interjection">hop hop</say-as> <break time="300ms"/> Sage [[Die Treppe rauf]]. ';
        return output;
    },
    Die_Treppe_rauf: function(attributes) {
        var output = '';
        output += 'Steve? Siehst du die Luke? <break time="250ms"/> Die musst du aufstoßen.<audio src="https://nordicskills.de/audio/MeinAuftrag/bodenluke1.mp3" /> <say-as interpret-as="interjection">doller</say-as><audio src="https://nordicskills.de/audio/MeinAuftrag/bodenluke2.mp3" /><say-as interpret-as="interjection">noch doller</say-as> <break time="250ms"/> Sage [[geh jetzt auf]]. ';
        return output;
    },
    geh_jetzt_auf: function(attributes) {
        var output = '';
        output += '<audio src="https://nordicskills.de/audio/MeinAuftrag/bodenlukeauf.mp3" /><say-as interpret-as="interjection">Na endlich.</say-as> <break time="150ms"/>Steve, das hast du gut gemacht. Jetzt lass uns hier abhauen. <break time="250ms"/>Ich weis nicht was mit dir ist, aber ich brauche eine Dusche. Ich habe hunger <break time="50ms"/> und ich bin müde. <break time="200ms"/> An der Straße entlang war ein Hotel. Wir haben dort Zeit uns den Inhalt der Schatulle genauer anzusehen. Sage [[Hotel betreten]]. ';
        return output;
    },
    Hotel_betreten: function(attributes) {
        var output = '';
        output += '<audio src="https://nordicskills.de/audio/MeinAuftrag/hotelklingel.mp3" /><break time="150ms"/><audio src="https://nordicskills.de/audio/MeinAuftrag/hotelempfang.mp3" /> Du kannst [[Ja]] oder [[nein]] sagen. ';
        return output;
    },
    Ja: function(attributes) {
        var output = '';
        output += 'Steve, die Zimmer sind im ersten Stock. Der Fahrstuhl ist dort vorne. <break time="200ms"/> Wir treffen uns Morgen früh um acht Uhr. Ich schau mir noch die Schatulle an <break time="50ms"/> und sage dir dann beim Frühstück was ich gefunden habe. <break time="200ms"/> Ich wünsche dir eine gute Nacht. <break time="250ms"/> Sage [[Fahrstuhl betreten]]. ';
        return output;
    },
    nein: function(attributes) {
        var output = '';
        output += '<say-as interpret-as="interjection">ist nicht dein ernst.</say-as> Du spinnst wohl. <break time="200ms"/> Ich habe gesagt ich will duschen, habe hunger und brauche eine Mütze voll Schlaf. <break time="100ms"/> Du gehst jetzt sofort zurück und buchst die Zimmer. Sage [[Ja]]. ';
        return output;
    },
    Fahrstuhl_betreten: function(attributes) {
        var output = '';
        output += '<audio src="https://nordicskills.de/audio/MeinAuftrag/fahrstuhl.mp3" /><audio src="https://nordicskills.de/audio/MeinAuftrag/salongbetreten.mp3" /><audio src="https://nordicskills.de/audio/MeinAuftrag/hotelzimmertuer.mp3" /> Sage [[Zimmer betreten]]. ';
        return output;
    },
    Zimmer_betreten: function(attributes) {
        var output = '';
        if (attributes.scene["IchBleibeHier"] == 1) {
            output += 'Steve, dein Albtraum ist vorbei. Wachwerden Alexa kommt gleich. <audio src="https://nordicskills.de/audio/MeinAuftrag/schnarchen.mp3" /><audio src="https://nordicskills.de/audio/MeinAuftrag/hotelzimklopfen.mp3" /> Steve, <break time="100ms"/> <say-as interpret-as="interjection">komm schon</say-as> ich warte auf dich. Ich will Frühstücken. Sage [[Zimmer verlassen]]. ';
        } else {
            output += 'Du bist in deinem Zimmer.<break time="250ms"/> Duschen und dann zu Bett. Morgen ist auch noch ein Tag. <audio src="https://nordicskills.de/audio/MeinAuftrag/klpause.mp3" /> <emphasis level="strong">Der nächste Morgen.</emphasis> <audio src="https://nordicskills.de/audio/MeinAuftrag/schnarchen.mp3" /><audio src="https://nordicskills.de/audio/MeinAuftrag/hotelzimklopfen.mp3" /> Steve, <break time="100ms"/> <say-as interpret-as="interjection">komm schon</say-as> ich warte auf dich. Ich will Frühstücken. <break time="250ms"/> Sage [[Zimmer verlassen]]. ';
        }
        return output;
    },
    Zimmer_verlassen: function(attributes) {
        var output = '';
        output += '<audio src="https://nordicskills.de/audio/MeinAuftrag/hotelzimmertuer.mp3" /><audio src="https://nordicskills.de/audio/MeinAuftrag/fahrstuhlrunter.mp3" /><audio src="https://nordicskills.de/audio/MeinAuftrag/salongbetreten.mp3" /><audio src="https://nordicskills.de/audio/MeinAuftrag/restaurant.mp3" />Guten Morgen Steve. <break time="200ms"/> Ich habe Neuigkeiten.<break time="250ms"/> Ich habe die Schatulle geöffnet und dort waren Briefe.<break time="250ms"/> Liebesbriefe <break time="250ms"/> von William an Ruth. <break time="250ms"/> <say-as interpret-as="interjection">von wegen</say-as> <say-as interpret-as="interjection">da war nichts</say-as>.<break time="1s"/> <p>Du hast recht.</p> Die Brüder haben die gleiche Frau geliebt. Darum der Streit zwischen den Brüdern. <break time="100ms"/> Jetzt stellt sich nur die Frage, ob Noah das wusste. <break time="200ms"/> Ich gebe dir recht das die Adresse in York, die du im Notizbuch gefunden hast, unser nächstes Ziel sein sollte.<break time="250ms"/> Ok, dann Zahl die Rechnung und lass uns zum Bahnhof. Um die Rechnung zu zahlen musst du in die Lobby. Dafür sage [[Zur Lobby]]. ';
        return output;
    },
    Zur_Lobby: function(attributes) {
        var output = '';
        output += '<audio src="https://nordicskills.de/audio/MeinAuftrag/hotel_lobby.mp3" /> ';
        output += 'Wenn du bezahlt hast lass uns los. Wir haben einen Auftrag der auf uns wartet und gelöst werden möchte. <break time="250ms"/> Sage [[Ich will hier bleiben]] oder [[Hotel verlassen]]. ';
        return output;
    },
    Ich_will_hier_bleiben: function(attributes) {
        var output = '';
        output += 'Steve? <say-as interpret-as="interjection">was zur hölle</say-as> machst du hier. Warum sitzt du noch rum? Wir müssen los. <break time="300ms"/> Mir ist klar, dass du der Boss bist, aber dann entscheide dich jetzt und sage [[Hotel verlassen]] oder [[Ich bleibe hier]]. ';
        return output;
    },
    Hotel_verlassen: function(attributes) {
        var output = '';
        output += 'Steve, wir fahren jetzt nach York. Dort angekommen werden wir in ein Taxi steigen. Dann das Sommerhaus der Thomsons aufsuchen. Ich hoffe für Die Alte, das wir weiter kommen. <audio src="https://nordicskills.de/audio/MeinAuftrag/stevewiederspruch.mp3" /> Sorry <break time="200ms"/><say-as interpret-as="interjection"> Unsere Kundin.</say-as> <audio src="https://nordicskills.de/audio/MeinAuftrag/bahnhof.mp3" /> Wenn du die Fahrkarten hast, lass uns einsteigen. Sage [[In den Zug steigen]]. ';
        return output;
    },
    Ich_bleibe_hier: function(attributes) {
        var output = '';
        if (attributes.scene["IchBleibeHier"] == 1) {
            output += 'Also Steve, diesmal ist es kein Traum. Leider hast du dich entschieden lieber noch eine Nacht im Hotel zu verbringen, darum bist du deinen Auftrag los und hast verloren.<audio src="https://nordicskills.de/audio/MeinAuftrag/verloren.mp3" /> Sage beenden oder um noch einmal zu spielen, sage Neustarten. ';
        } else {
            output += 'Leider hast du dich entschieden lieber noch eine Nacht im Hotel zu verbringen, darum bist du deinen Auftrag los und hast verloren.<audio src="https://nordicskills.de/audio/MeinAuftrag/verloren.mp3" /> ';
            attributes.scene["IchBleibeHier"] = 1;
            output += 'Du hast Glück, dass es gerade ein Traum ist, darum Sage [[Zimmer betreten]]. ';
        }
        return output;
    },
    In_den_Zug_steigen: function(attributes) {
        var output = '';
        output += 'Steve, weist du was ich mir überlege? <break time="250ms"/> Also, <break time="200ms"/>erst sterben die Eltern von Noah <break time="200ms"/> dann Noah selbst.<break time="150ms"/> Angeblich beide bei einem Unfall und jetzt ist der Onkel verschwunden.<say-as interpret-as="interjection">echt komisch</say-as> oder? <break time="150ms"/> Ich bin gespannt was wir in York finden werden.<audio src="https://nordicskills.de/audio/MeinAuftrag/servicekraft.mp3" /> Nein danke.<audio src="https://nordicskills.de/audio/MeinAuftrag/servicekraft1.mp3" /> <say-as interpret-as="interjection">ey was ist das?</say-as> kann der Typ nicht hören? Ich hatte nein gesagt.<break time="200ms"/> Was ist das in dem Becher? <break time="150ms"/><audio src="https://nordicskills.de/audio/MeinAuftrag/kaffeebecher.mp3" /> Ein Schlüssel? <break time="150ms"/> mit einem Anhänger und einer Adresse in York. <audio src="https://nordicskills.de/audio/MeinAuftrag/zugverlassen.mp3" /> <say-as interpret-as="interjection">so ein scheiß</say-as> <break time="50ms"/> der Typ ist nicht mehr zu sehen. Lass uns hier aussteigen und ein Taxi nehmen. Sage [[Hier aussteigen]]. ';
        return output;
    },
    Hier_aussteigen: function(attributes) {
        var output = '';
        output += 'Das Taxi wartet schon. <break time="50ms"/> Ich bin so gespannt was uns hier in York erwartet. Und zu welchem Haus <break time="150ms"/> warte mal <break time="250ms"/> <say-as interpret-as="interjection">ich glaub mein schwein pfeift</say-as> <break time="150ms"/>das ist ja die gleiche Adresse wie das Sommerhaus von der Thomsons. Glaubst du auch was ich denke? Schnell, lass uns los fahren. Sage [[In das Taxi steigen]]. ';
        return output;
    },
    In_das_Taxi_steigen: function(attributes) {
        var output = '';
        output += '<audio src="https://nordicskills.de/audio/MeinAuftrag/autostarten.mp3" /> Steve, Wenn wir gleich ankommen, pass bitte auf dich auf. Denn wir wissen ja nicht was auf uns zukommt und uns dort erwartet. Schließlich möchte ich Morgen auch noch einen Chef haben. <break time="2s"/> <audio src="https://nordicskills.de/audio/MeinAuftrag/autohalten.mp3" /> Gut, wir sind da <break time="150ms"/> lass uns aussteigen. <audio src="https://nordicskills.de/audio/MeinAuftrag/autotuerzu.mp3" /> Sage [[Aus dem Taxi steigen]]. ';
        return output;
    },
    Aus_dem_Taxi_steigen: function(attributes) {
        var output = '';
        output += '<audio src="https://nordicskills.de/audio/MeinAuftrag/fussweg.mp3" />Steve, du kannst klingeln oder den Schlüssel mit der Nummer neunzehn benutzen. <break time="400ms"/> Weist du was? <break time="100ms"/> Ich würde erst Klingeln, nicht das er da im Sessel sitz und du einfach in seine Stube reinspazierst. Sage [[Klingeln]] oder [[Schlüssel benutzen]]. ';
        return output;
    },
    Schlüssel_benutzen: function(attributes) {
        var output = '';
        output += '<audio src="https://nordicskills.de/audio/MeinAuftrag/tueraufschliessen.mp3" /><say-as interpret-as="interjection">Bingo</say-as> <break time="150ms"/>der schlüssel passt. <break time="100ms"/> Gut Steve, du willst ja das wir hier alles durchsuchen, dann fang mal an. Ich glaube geradeaus geht es weiter. Wenn du dir die Schuhe abgeputzt hast, sage [[Zum Eingangsbereich]]. ';
        return output;
    },
    Klingeln: function(attributes) {
        var output = '';
        output += '<audio src="https://nordicskills.de/audio/MeinAuftrag/tuerklingel.mp3" /> <break time="2s"/> <audio src="https://nordicskills.de/audio/MeinAuftrag/tuerklingel.mp3" /> Steve, hier ist keiner. Oder der schläft. <break time="250ms"/> Tja. Dann benutze doch den Schlüssel. Und ja, ich weis, du wolltest ihn ja gleich benutzen. <break time="50ms"/> Ist ja schon gut. Du bist der Boss und hast das sagen. <break time="50ms"/> Dann mach das auch und sage [[Schlüssel benutzen]]. ';
        return output;
    },
    Zum_Eingangsbereich: function(attributes) {
        var output = '';
        if (attributes.scene["LinksWarIchSchon"] == 1) {
            output += 'Die Besenkammer liegt rechts von dir und dann geht da noch eine Treppe nach oben und weiter durch ist ein Flur. Wohin willst du als nächstes? Sage [[Rechts]], [[Die Trappe nach oben]] oder [[Zum Flur]]. ';
        } else {
            if (attributes.scene["RechtsWarIchSchon"] == 1) {
                output += 'Nach links kommst du in einen anderen Raum. Ich glaube das ist die Küche. Dann geht da noch eine Treppe nach oben und weiter durch ist ein Flur. Wohin willst du als nächstes? Sage [[Links]], [[Die Trappe nach oben]] oder [[Zum Flur]]. ';
            } else {
                if (attributes.scene["ObenWarIchSchon"] == 1) {
                    output += 'Die Besenkammer liegt rechts von dir und nach links kommst du in einen anderen Raum. Ich glaube das ist die Küche und weiter durch ist ein Flur. Wohin willst du als erstes? Sage [[Links]], [[Rechts]] oder [[Zum Flur]]. ';
                } else {
                    output += 'Steve, hier im Eingangsbereich findest du einige Türen, einen Flur und eine Besenkammer. Die Besenkammer liegt rechts von dir und nach links kommst du in einen anderen Raum. Ich glaube das ist die Küche. Dann geht da noch eine Treppe nach oben und weiter durch ist ein Flur. Wohin willst du als erstes? Sage [[Links]], [[Rechts]], [[Die Trappe nach oben]] oder [[Zum Flur]]. ';
                }
            }
        }
        return output;
    },
    Links: function(attributes) {
        var output = '';
        attributes.scene["LinksWarIchSchon"] = 1;
        output += '<audio src="https://nordicskills.de/audio/MeinAuftrag/kuehlschrank.mp3" /> Sag ich doch, dass ist die Küche. <break time="400ms"/> <say-as interpret-as="interjection">oh mann</say-as> haben sie dem das Wasser abgestellt? <break time="200ms"/> Oder warum wäscht der nicht ab. <break time="100ms"/> Hier ist nichts anderes, außer ein Kühlschrank und das volle Waschbecken. <break time="100ms"/> Lass uns zurück in den Flur. Dafür sage, [[Zum Eingangsbereich]] oder [[Kühlschrank öffnen]]. ';
        return output;
    },
    Rechts: function(attributes) {
        var output = '';
        attributes.scene["RechzsWarIchSchon"] = 1;
        output += '<audio src="https://nordicskills.de/audio/MeinAuftrag/besenkammer.mp3" /><prosody pitch="x-high">Igitt</prosody><break time="100ms"/> Was für eine Fette Spinne. <break time="50ms"/> Schnell. Mach die Tür zu. <audio src="https://nordicskills.de/audio/MeinAuftrag/besenkammerzu.mp3" /> Das hier ist die Besenkammer. Außer Staub und was zum saubermachen findest du hier nichts. Lass uns weiter suchen. Sage [[Zum Eingangsbereich]]. ';
        return output;
    },
    Die_Trappe_nach_oben: function(attributes) {
        var output = '';
        attributes.scene["ObenWarIchSchon"] = 1;
        output += '<audio src="https://nordicskills.de/audio/MeinAuftrag/treppenstuffen.mp3" /> und? <audio src="https://nordicskills.de/audio/MeinAuftrag/verschltuer.mp3" /> Die Tür zum Dachboden ist abgeschlossen <break time="150ms"/> und du hast keinen Schlüssel. Dann komm wieder runter. Sage [[Zum Eingangsbereich]]. ';
        return output;
    },
    Zum_Flur: function(attributes) {
        var output = '';
        output += 'Hier im Flur siehst du Bilder der Familie.<break time="200ms"/> <say-as interpret-as="interjection">hey</say-as><break time="200ms"/> was macht denn der Kaffeetyp aus dem Zug auf den Bildern der Familie? <break time="150ms"/><prosody rate="x-slow">warte mal</prosody><break time="250ms"/><say-as interpret-as="interjection">natürlich</say-as>. Du hast recht. <break time="100ms"/> Das war kein Zufall.<break time="150ms"/> Er wollte das wir herkommen. <break time="150ms"/> Er wollte das wir das sehen. <break time="400ms"/> ';
        output += '<say-as interpret-as="interjection">na klar</say-as> Du hast recht, das war William der Onkel von Noah. <break time="200ms"/> Warte, ich komme.<break time="350ms"/> Da ist noch eine Tür. <break time="100ms"/>Mach bitte die Tür auf. <break time="250ms"/> Dafür sage [[Tür öffnen]]. ';
        return output;
    },
    Kühlschrank_öffnen: function(attributes) {
        var output = '';
        output += '<audio src="https://nordicskills.de/audio/MeinAuftrag/kuehlschrank_auf.mp3" /> ';
        output += '<say-as interpret-as="interjection">Echt jetzt</say-as>. <break time="150ms"/>Hast du schon wieder Hunger? <break time="150ms"/> Hier im Kühlschrank ist nichts. Auch wenn er Leer ist, vergesse nicht die Tür zu schließen. <audio src="https://nordicskills.de/audio/MeinAuftrag/kuehlschrank_zu.mp3" />  Lass und zurück gehen. Sage [[Zum Eingangsbereich]]. ';
        return output;
    },
    Tür_öffnen: function(attributes) {
        var output = '';
        output += 'Ok Steve, schau dich mal um.<break time="200ms"/> Da steht noch ein Computer. Ich werde mal sehen was ich darauf finde.<break time="100ms"/> <audio src="https://nordicskills.de/audio/MeinAuftrag/computeran.mp3" /><say-as interpret-as="interjection">ähm</say-as><break time="100ms"/> Steve ? <break time="150ms"/> Du bist drann. <break time="150ms"/> Jetzt brauch ich deine Hilfe.<break time="100ms"/> Der Computer ist mit einem Passwort geschützt. Achtstellig. <break time="150ms"/>Aber, warte mal.<break time="200ms"/>Erinnerst du dich noch an all die Zahlen ?<break time="150ms"/> Da war die Quittung vom Taxiefahrer. <break time="250ms"/> Die Seitenzahl im Notizbuch.<break time="250ms"/> Die Hausnummer von Noahs Wohnung. <break time="250ms"/> Die Seite mit der Notiz. Welche Zahl war das? <break time="350ms"/>Wann war der Unfall der Eltern? <break time="250ms"/> Was hatten wir denn noch? <break time="400ms"/> Stimmt. Wie lange arbeitet die Hausdame schon für die Familie? und welche Zahl stand auf dem Schlüsselanhänger? <break time="450ms"/>  Und Steve, schau mal hier, am Monitor <break time="400ms"/> der <phoneme alphabet="ipa" ph="pɔstit">Post-it</phoneme>. <break time="350ms"/>Da steht eine fünfzehn. Wieder eine Zahl. <break time="500ms"/> Das sind acht Zahlen. Richtig? <break time="250ms"/>Und wenn jede Zahl für einen Buchstaben steht. Dann hast du das Passwort.<break time="350ms"/> Du kannst jetzt das Passwort sagen, eine Pause machen oder beenden wenn du nicht weiter kommst. ';
        if (attributes.scene["Passwort"] == 1) {
            output += '[[Passwort]] ';
        }
        return output;
    },
    Passwort: function(attributes) {
        var output = '';
        output += '<say-as interpret-as="interjection">voila</say-as> <break time="350ms"/> und der PC gehört uns. <break time="200ms"/> Klasse gemacht Steve. <break time="250ms"/> Gut ich mach weiter. Ich will sehen was wir hier haben. <break time="1s"/><say-as interpret-as="interjection">Ich glaubs ja nicht</say-as><break time="250ms"/> Ich habe hier Noas Online Tagebuch. <break time="350ms"/> Und du wirst es nicht glauben.<break time="200ms"/>Der letzte Eintag ist von Heute. <break time="300ms"/><say-as interpret-as="interjection">Ja. Heute</say-as> <break time="100ms"/> Habe ich doch gerade gesagt.<break time="500ms"/>Ist mir klar das es nicht sein kann. <break time="200ms"/> Es sei denn. Jemand hat seinen Account gehäckt. <break time="1s"/><say-as interpret-as="interjection">Ist das dein ernst</say-as><break time="500ms"/>. Du glaubst das er noch lebt? <break time="250ms"/> Da kannst du recht haben. Dann lass uns weiter lesen. Sage [[Weiter lesen]]. ';
        return output;
    },
    Weiter_lesen: function(attributes) {
        var output = '';
        output += '<say-as interpret-as="interjection">ich glaub mein schwein pfeift</say-as>. Steve, das solltest du dir anhören. Hier kommt eine Sprachnachricht für uns.<audio src="https://nordicskills.de/audio/MeinAuftrag/mitteilung.mp3" /> <break time="1s"/> Ich habe mir die Nachricht komplett angehört und folgendes war passiert. <break time="100ms"/> Noah hatte die alten Liebesbriefe von seinem Onkel an seine Mutter gefunden. <break time="250ms"/>In dem letzten Brief wurde es sehr unschön. Noah hatte den verdacht, das William etwas mit dem Unfall seiner Eltern zu tun haben könnte und hatte ihn darauf angesprochen. <break time="250ms"/> Darauf eskalierte das Gespräch. Noah floh nach York in das Sommerhaus und auf dem Weg hierher hatte er einen Unfall. Er wurde darauf hin in ein Privates Krankenhaus gebracht. <break time="350ms"/> Vor lauter Angst vor seinem Onkel, täuschte er seinem Tot vor. <break time="250ms"/> Und so kamst du zu deinem Auftrag. Den Rest besprechen wir im Taxi. <break time="400ms"/> Wir fahren jetzt zu Noah ins Krankenhaus. <break time="250ms"/> <say-as interpret-as="interjection">komm schon</say-as>, das Taxi wartet. <break time="250ms"/> Sage dafür, [[Das Haus verlassen]]. ';
        return output;
    },
    Das_Haus_verlassen: function(attributes) {
        var output = '';
        output += '<audio src="https://nordicskills.de/audio/MeinAuftrag/fussweg.mp3" /><audio src="https://nordicskills.de/audio/MeinAuftrag/autotuerzu.mp3" /><audio src="https://nordicskills.de/audio/MeinAuftrag/autostarten.mp3" />Tja Steve, freust du dich? <break time="200ms"/> gleich hast du den Fall gelöst. <break time="100ms"/> Aber wer weiß, was uns noch im Krankenhaus erwartet. <break time="1s"/> <say-as interpret-as="interjection">alles klar</say-as> wir sind da. Dann lass uns aussteigen und zum Eingang gehen. <break time="100ms"/> Sage [[Das Taxi verlassen]]. ';
        return output;
    },
    Das_Taxi_verlassen: function(attributes) {
        var output = '';
        output += 'Ok Steve, wir sind jetzt im Krankenhausflur. Schau mal da vorn steht William und hier vorn, ist die Tür von Noah´s Zimmer. <break time="100ms"/> Vergesse nicht, Frau Thomson müssen wir auch noch anrufen. <break time="350ms"/> Tja. Jetzt musst du dich entscheiden, was wir machen sollen. Wo willst du hin, bzw was willst du? <break time="250ms"/> Sage [[Zu William gehen]], [[Zu Noah gehen]] oder [[Frau Thomson anrufen]]. ';
        return output;
    },
    Zu_William_gehen: function(attributes) {
        var output = '';
        output += 'William steht am Fenster und schaut runter auf die Strasse. <break time="200ms"/> Dort beobachtet er nachdenklich einen Rettungswagen.<audio src="https://nordicskills.de/audio/MeinAuftrag/rettungswagen.mp3" /> Als er dich bemerkt <break time="50ms"/> schaut er dich fragend an. <break time="150ms"/> <voice name="Hans">wenn ich das sehe, frage ich mich wie konnte das alles passieren.</voice> <break time="250ms"/> William erklärt das er damals auch in Noahs Mutter verliebt war. <break time="300ms"/>Er hatte Probleme zu akzeptieren, das Ruth sie für Henry entschied.<break time="300ms"/> So dauerte der Familien streit Jahre an. Bis zum Unfall der Eltern von Noah. <break time="100ms"/> Gerade fingen Noah und William an sich besser zu verstehen. <break time="150ms"/>Da fand Noah die Briefe. Denn Rest kennen wir.<break time="250ms"/> Nach einer weile geht Ihr gemeinsam zu Noah. Dafür sage [[Noah besuchen]]. ';
        return output;
    },
    Zu_Noah_gehen: function(attributes) {
        var output = '';
        output += 'Als wir den Raum betreten <break time="100ms"/>liegt Noah in seinem Bett. Er bittet uns seine Tante anzurufen und Sie zu bitten hierher zukommen. <break time="100ms"/> Du versprichst Noah, das wir es machen und gehen. <break time="250ms"/> <say-as interpret-as="interjection">hals und beinbruch</say-as><break time="200ms"/> Oh, Entschuldigung. <break time="150ms"/> Das war unpassend.<break time="400ms"/> Steve, lass uns gehen und Frau Thomson anrufen. <break time="300ms"/> Sage [[Frau Thomson anrufen]]. ';
        return output;
    },
    Frau_Thomson_anrufen: function(attributes) {
        var output = '';
        output += '<audio src="https://nordicskills.de/audio/MeinAuftrag/anrufbeithomson.mp3" />Hallo Frau Thomson. <break time="100ms"/> Hier ist Alexa. Wir haben eine gute Nachricht für Sie. <break time="150ms"/>Noah lebt, keine Sorge es geht ihm gut. Er liegt im Krankenhaus in York. <break time="100ms"/> Wir kommen nach London zurück und besprechen alles weitere vor Ort mit Ihnen.<break time="50ms"/> Auf  wiederhören. <break time="250ms"/> Steve, das wars. Lass uns nachhause fahren. <break time="50ms"/>Nach dem Gespräch mit Frau Thomson mach ich noch schnell die Rechnung fertig. Dann mach ich Feierabend.<break time="3s"/><say-as interpret-as="interjection">was denn?</say-as> Hat dir der Schluss nicht gefallen? Dann sage [[Das Taxi verlassen]] und versuch etwas anderes. <break time="350ms"/> Du kannst auch sagen Neustarten oder beenden. ';
        return output;
    },
    Noah_besuchen: function(attributes) {
        var output = '';
        output += 'Als wir das Krankenzimmer von Noah betreten sitzt seine Tante, Frau Thomson bereits neben seinem Bett. Sie schaut uns an und lächelt <voice name="Vicki">Herr Brown, Alexa. Danke das sie meinen Neffen gefunden haben. <break time="200ms"/> Und William, danke für deine Hilfe.</voice> <break time="100ms"/> William geht zu Noah und nimmt seine Hand, streichelt seine Schulter und sagt, <voice name="Hans">wir drei bekommen das schon wieder hin.</voice> <break time="200ms"/>Noah und Frau Thomson schauen sich an und nicken.<break time="250ms"/><say-as interpret-as="interjection">seufz</say-as><break time="200ms"/> Steve, lass uns gehn.<break time="200ms"/> Frau Thomson kommt auf dich zu, drückt dir einen Umschlag in die Hand und sagt.<voice name="Vicki">Herr Brown, hier noch ein kleiner Bonus und rufen Sie mich bitte später noch an.</voice> <break time="250ms"/> Um zu gehen, sage [[Raum verlassen]]. ';
        return output;
    },
    Raum_verlassen: function(attributes) {
        var output = '';
        output += '<audio src="https://nordicskills.de/audio/MeinAuftrag/gewonnen.mp3" /><say-as interpret-as="interjection">prost</say-as> und herzlichen Glückwunsch. <break time="200ms"/> Auch an dich. Ja <break time="100ms"/>du <break time="200ms"/> vor der Alexa. <break time="100ms"/> Danke das du mit mir gespielt hast.<break time="100ms"/>Eine bitte habe ich noch. Ich würde mich freuen, wenn du dir gleich noch etwas Zeit nimmst und mein Spiel bewertest.<break time="300ms"/> Vielen Dank. ';
        return output;
    },
}