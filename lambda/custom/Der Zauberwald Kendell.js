module.exports = {
    Start: function(attributes) {
        var output = '';
        output += 'Herzlich Willkommen bei deinem Spiel <break time="150ms"/> Der Zauberwald Kendell. <break time="150ms"/> Ich werde dich bei diesem Fantasyspiel begleiten und mit dir diese verzauberte Welt erleben. <break time="250ms"/> Kendell erwartet dich und bittet um deine Hilfe. <break time="100ms"/> Nur wenn du alle Abenteuer bestehst, alle Aufgaben löst und dich nicht von den Bösen und Mystischen Wesen in Kendell erwischen lässt, kannst du am Ende als Sieger hervorgehen. Um zu gewinnen stelle ich dir fragen, gebe dir Hinweise und manchmal warne ich dich auch vor Missgeschicken und gefahren. <break time="200ms"/> Aber höre mir genau zu. Denn nur dann, kommst du auch an dein Ziel. <break time="250ms"/> Kommst du einmal nicht weiter oder bist dir nicht sicher was du sagen sollst, sage Hilfe. Und jetzt wünsche ich dir viel Spaß im Zauberwald Kendell. <break time="200ms"/><audio src="https://nordicskills.de/audio/DerZauberwaldKendell/trailer.mp3" /><break time="200ms"/> Hallo mein Besucher, ich bin Alexa <break time="200ms"/> und eine Fee. Es freut mich, dass du mir helfen möchtest <break time="200ms"/> denn ich brauche dich für eine Aufgabe. Du musst mir aus dem Wald vor dir etwas Wichtiges besorgen. <break time="200ms"/> Aber bevor wir uns beide auf den Weg machen, musst du mir noch verraten als was für ein Abenteurer du aufbrechen willst. <break time="150ms"/> Du kannst dich als Magier oder als eine Elfin auf den Weg machen. <break time="250ms"/> Und? <break time="200ms"/> für was entscheidest du dich? Ich gebe dir noch etwas Bedenkzeit damit du es dir überlegen kannst. Aber vergesse nicht, deine Entscheidung nimmt Einfluss auf den Spielverlauf. <break time="1s"/> Jetzt liegt es an dir. Entscheide dich. <break time="200ms"/>Als was möchtest du Kendell betreten. <break time="200ms"/> Als [[Magier]] oder [[Elfin]]. ';
        return output;
    },
    Magier: function(attributes) {
        var output = '';
        attributes.scene["twineSuffix"] = "_Magier";
        attributes.scene["twineSwitch"] = true;
        return output;
    },
    Elfin: function(attributes) {
        var output = '';
        attributes.scene["twineSuffix"] = "_Elfin";
        attributes.scene["twineSwitch"] = true;
        return output;
    },
}