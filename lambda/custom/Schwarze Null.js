module.exports = {
    Anfang: function(attributes) {
        var output = '';
        output += 'Wenn du soweit bist und zeigen willst was du drauf hast sage [[los gehts]]. ';
        return output;
    },
    los_gehts: function(attributes) {
        var output = '';
        output += 'Vor dir steht ein Tisch mit vier Bechern, unter jedem Becher ist ein Zettel mit einer Zahl. Diese Zahl wird von deinem Guthaben abgezogen. Welchen Becher möchtest du anheben? Den [[Blauen Becher]], [[Weißen Becher]], [[Grünen Becher]] oder [[Gelben Becher]]? ';
        return output;
    },
    Blauen_Becher: function(attributes) {
        var output = '';
        output += '[[weiter]] ';
        return output;
    },
    Weißen_Becher: function(attributes) {
        var output = '';
        output += '[[weiter]] ';
        return output;
    },
    Grünen_Becher: function(attributes) {
        var output = '';
        output += '[[weiter]] ';
        return output;
    },
    Gelben_Becher: function(attributes) {
        var output = '';
        output += '[[weiter]] ';
        return output;
    },
    weiter: function(attributes) {
        var output = '';
        output += 'Der Losverkäufer vor dir, schüttelt seinen Loseimer und bittet dir seine Lose an, welches Los nimmst du? Das mit der [[Blauen Schleife]], [[Grünen Schleife]] oder der [[Roten Schleife]]? ';
        return output;
    },
    Blauen_Schleife: function(attributes) {
        var output = '';
        output += '[[Casino Betreten]] ';
        return output;
    },
    Grünen_Schleife: function(attributes) {
        var output = '';
        output += '[[Casino Betreten]] ';
        return output;
    },
    Roten_Schleife: function(attributes) {
        var output = '';
        output += '[[Casino Betreten]] ';
        return output;
    },
    Casino_Betreten: function(attributes) {
        var output = '';
        output += 'Roulette auf rot [[Rot]] oder [[Schwarz]] ';
        return output;
    },
    Rot: function(attributes) {
        var output = '';
        output += '[[Casino verlassen]] ';
        return output;
    },
    Schwarz: function(attributes) {
        var output = '';
        output += '[[Casino verlassen]] ';
        return output;
    },
    Casino_verlassen: function(attributes) {
        var output = '';
        output += 'Doppelklick auf diesen Absatz zum Bearbeiten. ';
        return output;
    },
}