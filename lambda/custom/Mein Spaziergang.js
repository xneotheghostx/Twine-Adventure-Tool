module.exports = {
    Beginn: function(attributes) {
        var output = '';
        attributes.scene["Item_Gaslampe"] = 1;
        attributes.scene["Item_Streichhölzer"] = 2;
        output += 'Willkommen bei meinem Spiel Mein <phoneme alphabet="ipa" ph="ʃpaˈʦiːɐ̯ˌɡaŋ">Spaziergang</phoneme>. Ich werde dich bei dem Spiel begleiten, stelle dir fragen, gebe dir Hinweise und wenn du Hilfe brauchst sage Hilfe.<audio src="https://nordicskills.de/audio/MeinSpaziergang/waldmitvoegel.mp3" /> Eigendlich wolltest du ja nur einen Spaziergang durch einen Wald machen.<break time="100ms"/> Aber, sagte ich eigendlich? <break time="100ms"/> Du bist doch immer für ein Abenteuer zu haben. Ein <phoneme alphabet="ipa" ph="ˈaːbn̩tɔɪ̯ɐ">Abenteuer</phoneme>? <break time="100ms"/> Das kannst du haben. Am Ende des Waldes, findest du eine Mauer mit einem kleinen Eingang. <break time="100ms"/> Dieser Eingang sieht nicht gerade einladend aus, aber du bist doch für etwas Abwechslung. Oder ? <break time="100ms"/> Wenn du bereit bist, sage [[Zum Eingang]]. ';
        return output;
    },
    Zum_Eingang: function(attributes) {
        var output = '';
        if (attributes.visited.includes("Ja") && !attributes.scene["Tot"] && !attributes.scene["Bienen"]) {
            output += 'Also, die Tür zwei war wohl nicht der Brüller. <say-as interpret-as="interjection">kopf hoch</say-as>, ist ja nicht weiter schlimm, denn du hast ja noch zwei andere Türen. <break time="150ms"/> Wo willst du jetzt hin? Sage [[Öffne Tür eins]] oder [[Öffne Tür drei]]. ';
        } else {
            if (attributes.visited.includes("Öffne die Tür") && attributes.scene["Tot"] == 1 && !attributes.scene["Schlüsselpasstnicht"] || attributes.scene["Tot"] == 1 && !attributes.scene["Schlüsselpasstnicht"]) {
                attributes.scene["Tot"] = 0;
                output += 'Da bist du ja wieder. <audio src="https://nordicskills.de/audio/MeinSpaziergang/hallelujah.mp3" /> Auferstanden von den Toten. <break time="100ms"/> Gut gut. Dann gehts jetzt weiter, aber pass bei nächstenmal besser auf. <break time="150ms"/> Wo willst du jetzt hin? Sage [[Öffne Tür eins]], [[Öffne Tür zwei]] oder [[Öffne Tür drei]]. ';
            } else {
                if (attributes.visited.includes("Öffne die Tür") && attributes.scene["Tot"] == 0 && !attributes.scene["Schlüsselpasstnicht"]) {
                    output += 'Ich glaube nicht, dass du irgendetwas hinter der Tür eins findest was dir weiter hilft. Aber das ist deine Entscheidung. <break time="150ms"/> Wo willst du jetzt hin? Sage [[Öffne Tür eins]], [[Öffne Tür zwei]] oder [[Öffne Tür drei]]. ';
                } else {
                    if (attributes.visited.includes("Öffne die Tür") && !attributes.scene["Tot"] && !attributes.scene["Schlüsselpasstnicht"]) {
                        output += 'Ok. Der rechte <phoneme alphabet="ipa" ph="veːk">Weg</phoneme> hinter der Tür eins war es nicht. Dort gab es nichts außer Bienenstiche. <break time="100ms"/> <break time="150ms"/> Dann versuch doch die Tür eins nochmal <break time="100ms"/> und nimm dann den anderen <phoneme alphabet="ipa" ph="veːk">Weg</phoneme> oder versuche eine der anderen Türen. <break time="150ms"/> Sage [[Öffne Tür eins]], [[Öffne Tür zwei]] oder [[Öffne Tür drei]]. ';
                    } else {
                        if (attributes.visited.includes("Weiter") && attributes.scene["Schlüsselpasstnicht"] == 1) {
                            output += 'Gut gut. Jetzt ist es einfach für dich. <break time="100ms"/> Denn hinter der Tür eins, waren nichts außer Bienenstiche und der Tot. Hinter der Tür zwei war eine weitere, aber verschloßene Tür. <break time="150ms"/> Tja, da bleibt ja nur noch Tür drei übrig. Wenn du die nehmen willst, sage [[Öffne Tür drei]]. ';
                        } else {
                            output += '<audio src="https://nordicskills.de/audio/MeinSpaziergang/schotterweg.mp3" /> ';
                            output += 'Du gehst hinein. <break time="150ms"/> Dort angekommen betrittst Du einen Raum mit drei Türen. <audio src="https://nordicskills.de/audio/MeinSpaziergang/kellerschritte.mp3" />Auf jeder Tür steht eine Zahl. Welche Tür möchtest du öffnen? Sage [[Öffne Tür eins]], [[Öffne Tür zwei]] oder [[Öffne Tür drei]]. ';
                        }
                    }
                }
            }
        }
        return output;
    },
    Öffne_Tür_eins: function(attributes) {
        var output = '';
        if (attributes.visited.includes("Nach links")) {
            output += 'Hier warst du schon einmal. Der rechte <phoneme alphabet="ipa" ph="veːk">Weg</phoneme> war gefährlich. Oder sagen wir besser tödlich. <break time="100ms"/> Da bleibt dann nur noch der linke <phoneme alphabet="ipa" ph="veːk">Weg</phoneme> übrig. Wenn du den nehmen willst, sage [[Nach rechts]] ';
        } else {
            if (attributes.visited.includes("Nach rechts")) {
                output += 'Hier warst du schon einmal. Der linke <phoneme alphabet="ipa" ph="veːk">Weg</phoneme> war ziemlich gefährlich. Außer Bienenstiche, gab es dort nichts. Da bleibt dann nur noch der linke <phoneme alphabet="ipa" ph="veːk">Weg</phoneme> übrig. Wenn du den nehmen willst, sage [[Nach links]] ';
            } else {
                output += '<audio src="https://nordicskills.de/audio/MeinSpaziergang/kellertuer.mp3" /> ';
                attributes.scene["TürEins"] = 1;
                output += 'Du gehst hinein. Hinter dem Durchgang kommt ein kleiner <phoneme alphabet="ipa" ph="veːk">Weg</phoneme> der dich zu einer Weggabelung führt. <audio src="https://nordicskills.de/audio/MeinSpaziergang/waldweg_steine_laub_voegel.mp3" /> Ab hier musst du dich entscheiden, ob du nach links oder rechts gehen willst. Sage [[Nach rechts]] oder [[Nach links]]. ';
            }
        }
        return output;
    },
    Nach_rechts: function(attributes) {
        var output = '';
        output += 'Du gehst einen kleinen <phoneme alphabet="ipa" ph="veːk">Weg</phoneme> entlang, vorbei an einem Sumpf. <audio src="https://nordicskills.de/audio/MeinSpaziergang/sumpf.mp3" /> Danach kommst du an eine Gartentür. Der Schlüssel steckt und du könntest hinein gehen. <break time="100ms"/> Ich bin mir nicht sicher, aber ich habe das Gefühl du solltest hier nicht rein gehen. <break time="100ms"/> Aber was sage ich <break time="100ms"/> du bist doch kein Feigling? <break time="100ms"/> oder? Dann sage [[Öffne die Tür]]. ';
        return output;
    },
    Nach_links: function(attributes) {
        var output = '';
        output += '<audio src="https://nordicskills.de/audio/MeinSpaziergang/kellerschritte.mp3" /> ';
        output += 'Dieser <phoneme alphabet="ipa" ph="veːk">Weg</phoneme> gefällt mir gar nicht. Hier ist es kalt, dunkel und schmierig. <break time="100ms"/> Und dieser Geruch. <say-as interpret-as="interjection">pfui</say-as><break time="100ms"/> es stinkt. <break time="150ms"/> Aber es war ja deine Entscheidung. Komm schon. Lass uns schnell weiter gehen. <break time="150ms"/> Dafür sage [[Weiter]] oder gehe zurück zum Eingang. Dafür sage [[Zum Eingang]]. ';
        return output;
    },
    Öffne_die_Tür: function(attributes) {
        var output = '';
        attributes.scene["Bienen"] = 1;
        output += '<audio src="https://nordicskills.de/audio/MeinSpaziergang/bienen.mp3" /> Schnell die Tür zu. <audio src="https://nordicskills.de/audio/MeinSpaziergang/gartentuerzu.mp3" /> <say-as interpret-as="interjection">autsch</say-as><break time="100ms"/> Der Garten dahinter ist voller Bienen. <say-as interpret-as="interjection">naja</say-as>Hier geht es nicht weiter. <break time="100ms"/> Gehe zurück und sage [[Zum Eingang]]. ';
        return output;
    },
    Öffne_Tür_zwei: function(attributes) {
        var output = '';
        output += '<audio src="https://nordicskills.de/audio/MeinSpaziergang/tuerverschl.mp3" /> Die Tür ist verschlossen. Du hast einen Schlüssel <audio src="https://nordicskills.de/audio/MeinSpaziergang/schluessel.mp3" /> willst du ihn benutzen ? ';
        output += 'Dann sage [[Ja]]. ';
        return output;
    },
    Öffne_Tür_drei: function(attributes) {
        var output = '';
        output += 'Die Tür geht auf <audio src="https://nordicskills.de/audio/MeinSpaziergang/tuerauf.mp3" /> und du bist in einem kleinen Raum, es ist kalt und feucht, vor dir siehst du eine Holztür, sie steht offen. Du willst hier ja wieder raus <break time="100ms"/> oder? Und das kannst du nur wenn du weiter gehst. <break time="150ms"/> Na dann, sage zur [[Holztür]]. ';
        return output;
    },
    Weiter: function(attributes) {
        var output = '';
        if (attributes.scene["Tot"] == 0) {
            output += '<audio src="https://nordicskills.de/audio/MeinSpaziergang/schrei.mp3" /> ';
            output += 'Wie oft möchtest du denn noch sterben? Ich dachte ich hatte dich gewarnt? ';
            output += 'Wenn du es nochmal versuchen möchtest sage [[Zum Eingang]] oder sage beenden. ';
            attributes.scene["Tot"] = 1;
        } else {
            attributes.scene["Tot"] = 1;
            output += '<audio src="https://nordicskills.de/audio/MeinSpaziergang/schrei.mp3" /> Tja, ich hatte dich gewarnt. Aber nein. Du wusstest es ja besser.<break time="150ms"/> Jetzt liegst du da unten und das wars. Aus und vorbei. Du bist Tod und hast verloren. <audio src="https://nordicskills.de/audio/MeinSpaziergang/verlorenundtot.mp3" /> Nicht immer ist der erste <phoneme alphabet="ipa" ph="veːk">weg</phoneme> der richtige. Wenn du es nochmal versuchen möchtest sage [[Zum Eingang]] oder sage beenden. ';
        }
        return output;
    },
    Ja: function(attributes) {
        var output = '';
        attributes.scene["Schlüsselpasstnicht"] = 1;
        output += '<audio src="https://nordicskills.de/audio/MeinSpaziergang/VerschlTuer.mp3" /> Dein Schlüssel passt zwar in das Schloss, aber die Tür bleibt verschlossen. Also wieder zurück und auf ein Neues. Sage [[Zum Eingang]]. ';
        return output;
    },
    Holztür: function(attributes) {
        var output = '';
        output += 'Vor dir ist ein langer <phoneme alphabet="ipa" ph="veːk">weg</phoneme>, es geht nur geradeaus. Du gehst weiter <break time="1s"/> und weiter <break time="1s"/> und immer weiter, bis du am Ende des Weges vor einer weiteren Holztür stehst. <break time="100ms"/> Du rüttelst an ihr <audio src="https://nordicskills.de/audio/MeinSpaziergang/gartentuero.mp3" /> und sie geht auf. <break time="100ms"/> Du gehst hindurch. <audio src="https://nordicskills.de/audio/MeinSpaziergang/gartentuerzu.mp3" /> Was war das? Die Tür fällt hinter dir ins schloss. Toll, jetzt ist sie zu und du bekommst sie nicht wieder auf. <break time="150ms"/> Ok, ab jetzt geht es nur gerade aus. <audio src="https://nordicskills.de/audio/MeinSpaziergang/kellerschritte.mp3" /> Vor dir ist eine Treppe. Sie führt zu einem Keller und auch hier bleibt dir nichts übrig, als zum Keller zu gehen. Sage, zum [[Kellereingang]]. ';
        return output;
    },
    Kellereingang: function(attributes) {
        var output = '';
        if (attributes.visited.includes("Den linken Weg")) {
            output += 'Tja, da du jetzt weißt, dass du einen Schlüssel brauchst. Weißt du zumindest wonach du suchen musst. <break time="150ms"/> Wohin gehst du jetzt? Sage [[Den rechten Weg]], [[Gerade aus]] oder [[Den linken Weg]]. ';
        } else {
            if (attributes.visited.includes("Treppe nach oben")) {
                output += 'Na, da bist du ja wieder. Hat dich doch einer gefunden? Tja. Dann versuche es doch gleich noch einmal. ';
                output += 'Wohin gehst du jetzt? Sage [[Den rechten Weg]], [[Gerade aus]] oder [[Den linken Weg]]. ';
            } else {
                if (attributes.scene["Item_Kellerschluessel"] == 1) {
                    output += 'Mit dem Schlüssel den du gerade gefunden hast, kannst du sicher irgendeine Tür öffnen. <break time="100ms"/> Vielleicht kommst du ja auch endlich aus diesem Kellerloch heraus. <break time="150ms"/> Nagut, rechts waren wir ja schon. Wohin gehst du jetzt? Sage [[Gerade aus]] oder [[Den linken Weg]]. ';
                } else {
                    output += 'Hier im Keller ist es sehr dunkel, nur gut dass du eine Gaslampe hast. Schau doch mal in deiner Tasche nach Streichhölzern. <audio src="https://nordicskills.de/audio/MeinSpaziergang/streichholz.mp3" /> Die Lampe geht an. <break time="100ms"/> Ohh, was man mit etwas Licht so alles sieht. Vor dir erkennst du drei Wege. Tja, was machst du jetzt? Ab hier solltest du besonders vorsichtig sein. <break time="150ms"/> Achte  besser auf die Wege und Nischen. Vielleicht ist es auch ratsam, dir das eine oder andere zu merken. <break time="100ms"/> Wohin gehst du? Sage [[Den rechten Weg]], [[Gerade aus]] oder [[Den linken Weg]]. ';
                }
            }
        }
        return output;
    },
    Gerade_aus: function(attributes) {
        var output = '';
        output += '<audio src="https://nordicskills.de/audio/MeinSpaziergang/kaputteflasche.mp3" /> Klasse, das war deine Lampe. <break time="100ms"/> Tja, jetzt ist es so dunkel, das du nichts mehr sehen kannst. Aber soweit ich mich erinnere, war vor dir eine Treppe. <break time="150ms"/> Versuche vorsichtig nach vorne zugehen. <audio src="https://nordicskills.de/audio/MeinSpaziergang/kopfgestossen.mp3" /> Das war dein Kopf. Ich sagte doch, vorsichtig. <break time="150ms"/> Ein Stück nach links und du hast die Treppe gefunden. Dann sage [[Treppe nach oben]]. ';
        return output;
    },
    Den_rechten_Weg: function(attributes) {
        var output = '';
        output += '<audio src="https://nordicskills.de/audio/MeinSpaziergang/fliegen.mp3" /> <say-as interpret-as="interjection">iiieh</say-as><break time="50ms"/> hier riecht es aber nicht besonders gut. Ich glaube, du willst nicht wissen was dort in der Ecke liegt. <break time="100ms"/> Es ist besser wenn du schnell weiter gehst. <audio src="https://nordicskills.de/audio/MeinSpaziergang/laufen.mp3" /> Vor dir ist eine Tür. Mach sie schnell auf. Du musst sagen, [[Die Tür öffnen]]. ';
        return output;
    },
    Den_linken_Weg: function(attributes) {
        var output = '';
        output += '<say-as interpret-as="interjection">nanu?</say-as> hier geht es nicht weiter. Aber an der Mauer sind Umrisse einer geheimen Tür oder eines geheimen durchgangs. Und an der Wand, bei der Tür steht. <break time="150ms"/> <emphasis level="strong">Lies die Worte, auf dem was funkelte in der Nacht.</emphasis> Das ist sicher ein Zauber oder Geheimwort. ';
        if (attributes.scene["Item_Kellerschluessel"] == 1) {
            output += 'Du hast doch vorhin einen Schlüssel gefunden. <break time="100ms"/> Was stand da nochmal drauf? <break time="150ms"/> Ich glaube das war <break time="250ms"/> Nimm mich mit. Oder? <break time="200ms"/> Wenn es das war sage [[Nimm mich mit]]. ';
        } else {
            output += 'Du kennst kein Geheimwort und einen Schlüssel hast du auch nicht. <break time="150ms"/> Also musst du zurück und danach suchen. Da war doch noch eine andere Tür. <break time="100ms"/> Du kannst dich ja einmal umsehen. Dafür sagst du [[Kellereingang]]. ';
        }
        output += ' ';
        return output;
    },
    Die_Tür_öffnen: function(attributes) {
        var output = '';
        attributes.scene["Item_Kellerschluessel"] = 1;
        output += 'Die Tür geht auf, und durch das Licht deiner Lampe, siehst du in der Ecke ein Funkeln. Was ist das ? <break time="1s"/> Du hebst den Gegenstand auf <audio src="https://nordicskills.de/audio/MeinSpaziergang/schluesselgefunden.mp3" /> und findest einen Schlüssel. <break time="100ms"/> Auf dem Schlüssel steht eingraviert, "nimm mich mit". <break time="100ms"/> Was das wohl bedeutet? Egal, ab in deinen Beutel mit dem Schlüssel. <break time="150ms"/> In diesem Raum geht es nicht weiter, du musst zurück gehen. Dafür sage [[Kellereingang]]. ';
        return output;
    },
    Treppe_nach_oben: function(attributes) {
        var output = '';
        output += '<audio src="https://nordicskills.de/audio/MeinSpaziergang/pfeil.mp3" /> Das war eine Falle <break time="100ms"/> und der Pfeil steckt in deinem Bein. <say-as interpret-as="interjection">oh nein</say-as> damit kannst du nicht weiter gehen. Super, das wars. Hier findet dich niemand. <break time="200ms"/> Leider hast du verloren. <audio src="https://nordicskills.de/audio/MeinSpaziergang/verlorenundtot.mp3" /> <break time="150ms"/> Wenn du es nochmal versuchen möchtest, sage [[Kellereingang]]. Von dort kannst du es nochmal versuchen. <break time="100ms"/> Zum beenden, sage Beenden oder für einen Neustart, sage Neustarten. ';
        return output;
    },
    Nimm_mich_mit: function(attributes) {
        var output = '';
        output += 'Das Geheimwort war richtig. <audio src="https://nordicskills.de/audio/MeinSpaziergang/portal.mp3" /> Ein Portal öffnet sich vor deinen Augen, du gehst hinein und verschwindest aus diesem tristen und dunklen Keller. <audio src="https://nordicskills.de/audio/MeinSpaziergang/beam.mp3" /> Auf der anderen Seite des Portals angekommen, findest du dich an einem Strand am Meer wieder. <audio src="https://nordicskills.de/audio/MeinSpaziergang/strand.mp3" /> Was zum Kuckuck war denn das ? Verwirrt schaust du umher, und jetzt ? <break time="1s"/> Ja, das erfährst du im 2. Teil deines Abenteuers. Danke fürs Spielen. <break time="500ms"/> Wenn du jetzt den 2. Teil spielen möchtest, dann sage wenn das Spiel beendet ist, Alexa starte Mein Spaziergang Teil2. <break time="200ms"/> Ich wünsche dir viel Spaß beim 2. Teil. <break time="100ms"/> Wir sehen uns dann am Strand wieder. <break time="100ms"/> Aufwiedersehen. ';
        return output;
    },
}